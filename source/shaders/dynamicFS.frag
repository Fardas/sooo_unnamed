/*
custom fragment shader for the dynamic elements
author = Katai Kristof
*/
#version 450
layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec2 fragTexCoords;
layout(location = 2) in vec3 fragNormal;
layout(location = 3) flat in uint textureIndex;
layout(location = 4) flat in uint texture2Index;

out vec4 outColor;

layout (binding = 0) uniform sampler2DArray textureArray;

uniform vec3 eye;

//Directional Light
vec3 dirLightDir = vec3(-1.0f, -1.0f, -0.5f);

//Light
vec4 ambiLight = vec4(0.5f, 0.5f, 0.5f, 1);
vec4 diffLight = vec4(0.6f, 0.6f, 0.6f, 1);
vec4 specLight = vec4(1.0f, 1.0f, 1.0f, 1);

//Material
vec4 ambiMat = vec4(vec3(0.4f), 1.0f);
vec4 diffMat = vec4(vec3(1.0f), 1.0f);
vec4 specMat = vec4(vec3(0.0f), 1.0f);

void main()
{
	vec4 ambi = ambiLight * ambiMat;

	vec3 normal = normalize(fragNormal);
	vec3 toLight = normalize(-dirLightDir);

	float diffInt = clamp(dot(normal, toLight), 0.0f, 1.0f);

	vec4 diff = diffLight * diffMat * diffInt;

	vec3 toEye = normalize(eye - fragPosition);
	vec3 h = normalize(toLight + toEye);
	float specInt = pow(clamp(dot(normal, h), 0.0f, 1.0f), 40);

	vec4 spec = specLight * specMat * specInt;

	vec4 texture1 =
		texture(textureArray, vec3(fragTexCoords.st, textureIndex));

	if(texture2Index != 1024)
	{
		vec4 texture2 =
			texture(textureArray, vec3(fragTexCoords.st, texture2Index));

		if(texture2.w == 0.0f) outColor = (spec + diff + ambi) * texture1;

		else outColor = (spec + diff + ambi) * texture2;
	}

	else outColor = (spec + diff + ambi) * texture1;
}
