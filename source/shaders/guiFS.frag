/*
custom gui fragment shader for the gui elements
author = Katai Kristof
*/
#version 450
layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec2 fragTexCoords;
layout(location = 2) flat in uint textureIndex;
layout(location = 3) in vec4 color;

layout (binding = 0) uniform sampler2DArray textureArray;

out vec4 outColor;

void main()
{
	vec3 textureColor = vec3(fragTexCoords.st, textureIndex);

	outColor =
		texture(textureArray, vec3(fragTexCoords.st, textureIndex));

	outColor.x *= color.x;
	outColor.y *= color.y;
	outColor.z *= color.z;
	outColor.a *= color.a;
}
