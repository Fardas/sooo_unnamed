/*
custom gui vertex shader for the gui elements
author = Katai Kristof
*/
#version 450
layout(location = 0) in vec3 vertPosition;

layout(location = 0) out vec3 fragPosition;
layout(location = 1) out vec2 fragTexCoords;
layout(location = 2) flat out uint textureIndex;
layout(location = 3) out vec4 color;

struct guiDatas
{
    vec4 translate_scale;
    vec4 texCoords;
    vec4 guiColor;
    vec4 texIndex;
};

layout(std430, binding = 0) buffer ssbo
{
	guiDatas elementParameters[];
};

void main()
{
	guiDatas e_p = elementParameters[gl_InstanceID];
	float px = e_p.translate_scale.x;
	float py = e_p.translate_scale.y;
	float sx = e_p.translate_scale.z;
	float sy = e_p.translate_scale.w;

	mat4 translate =
		mat4(1.0f, 0.0f, 0.0f, px*2-(1-sx),
	         0.0f, 1.0f, 0.0f, -py*2+(1-sy),
	         0.0f, 0.0f, 1.0f, 0.0f,
	         0.0f, 0.0f, 0.0f, 1.0f);

	mat4 scale =
		mat4(sx, 0.0f, 0.0f, 0.0f,
	         0.0f, sy, 0.0f, 0.0f,
	         0.0f, 0.0f, 1.0f, 0.0f,
	         0.0f, 0.0f, 0.0f, 1.0f);

	gl_Position = vec4(vertPosition.xy, 0.0, 1.0) * scale * translate;
	fragPosition = gl_Position.xyz;
	fragTexCoords = vec2(
		e_p.texCoords[int(vertPosition.x) + 1],
		1 - e_p.texCoords[2 - int(vertPosition.y)]);
	textureIndex = uint(e_p.texIndex[0]);
	color = e_p.guiColor;
}
