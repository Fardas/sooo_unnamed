"""UNG Drawer class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import pathlib

from os import walk
from OpenGL.GL import *

# External libraries
import numpy

# Packages
#   Data classes
import source.data_classes.game_object as go
import source.data_classes.main_data as md
#   Utilities
import source.utilities.opengl_utilities as ogl_uty
import source.utilities.utilities as uty


class UNGDrawer:
    def __init__(self, guis=None):
        
        # Array/Buffer Objects
        self.gui_vao = None
        self.gui_vbo = None
        self.gui_ibo = None
        self.gui_ssbo = None
        
        # shaders
        self.gui_shader = None
        
        # Textures
        self.texture_dict = {}
        self.gui_tex_arr = []
        
        # Draw data
        self.draw_command = []
        self.element_parameters = []
        
        # UnGUI
        self.guis = guis
    
    # Base functions
    def init(self):
        """Initializes UNGDrawer"""
        
        # Vertex array object
        self.gui_vao = glGenVertexArrays(1)
        glBindVertexArray(self.gui_vao)

        vertex_shader_path = md.MAIN_DIR.joinpath(
            'source', 'shaders', 'guiVS.vert')
        fragment_shader_path = md.MAIN_DIR.joinpath(
            'source', 'shaders', 'guiFS.frag')

        # Compile shaders
        self.gui_shader = ogl_uty.compile_shader(
            vertex_shader_path, fragment_shader_path)
        
        if self.gui_shader == -1 or self.gui_shader == -2:
            return -1
        
        # Vertex data
        gui_vertex = numpy.array([-1, 1, -1, -1, 1, 1,
                                  1, -1, 1, 1, -1, -1], ctypes.c_float)

        # Loading up vector buffer object
        self.gui_vbo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.gui_vbo)
        glBufferData(GL_ARRAY_BUFFER,
                     gui_vertex.itemsize * len(gui_vertex),
                     gui_vertex, GL_STATIC_DRAW)
        
        glEnableVertexAttribArray(0)
        
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                              gui_vertex.itemsize * 2,
                              ctypes.c_void_p(0))
        
        glBindVertexArray(0)

        # Textures
        self.load_textures()
        self.gui_tex_arr = \
            ogl_uty.generate_texture_array(self.texture_dict)

        # Generate transformation data for elements
        self.generate_element_parameters()

        # Convert to ctypes array
        element_parameters_arr = \
            (ElementData * len(self.element_parameters)) \
                (*self.element_parameters)

        # Generate draw commands and instance parameters
        self.generate_draw_data()
        
        # Loading up shader storage buffer object
        self.gui_ssbo = glGenBuffers(1)
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, self.gui_ssbo)
        glBufferData(GL_SHADER_STORAGE_BUFFER,
                     ctypes.sizeof(element_parameters_arr),
                     element_parameters_arr, GL_DYNAMIC_COPY)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, self.gui_ssbo)

        return 0
    
    def reinit(self):
        """Updates all graphic data of guis"""

        # Generate transformation data for elements
        self.generate_element_parameters()
        
        # Generate draw commands and instance parameters
        self.generate_draw_data()
        
        element_parameters_arr = \
            (ElementData * len(self.element_parameters))\
                (*self.element_parameters)

        # Loading up shader storage buffer object
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, self.gui_ssbo)
        glBufferData(GL_SHADER_STORAGE_BUFFER,
                     ctypes.sizeof(element_parameters_arr),
                     element_parameters_arr, GL_DYNAMIC_COPY)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, self.gui_ssbo)
    
    def draw(self):
        """Draws guis"""
        glDisable(GL_DEPTH_TEST)
        
        glUseProgram(self.gui_shader)
        glBindVertexArray(self.gui_vao)
        glBindTexture(GL_TEXTURE_2D_ARRAY, self.gui_tex_arr)
        
        glMultiDrawArraysIndirect(GL_TRIANGLE_STRIP, self.draw_command,
                                  1, 0)
        
        glEnable(GL_DEPTH_TEST)
        
        glBindVertexArray(0)
    
    def update_element(self, gui_name, element_name):
        """Updates the graphic data of only one element"""
        
        if self.guis[gui_name].enabled:
            graphic = self.guis[gui_name].graphic_data[element_name]
            
            texture_path = md.MAIN_DIR.joinpath(
                'resources', 'textures', 'gui_tex', graphic['tex_name'])

            texture_index = list(self.texture_dict).index(texture_path)
            
            temp_list = [[*graphic['position'], *graphic['size']],
                         [*graphic['tex_pos'], *graphic['tex_size']],
                         [*[c / 255.0 for c in graphic['color']]],
                         [texture_index, 0, 0, 0]]
            
            index = \
                list(self.guis[gui_name].graphic_data).\
                index(element_name)
            
            self.element_parameters[index] = ElementData(*temp_list)
        
            element_parameters_arr = \
                (ElementData * len(self.element_parameters))\
                    (*self.element_parameters)
        
            # loading up shader storage buffer object
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, self.gui_ssbo)
            glBufferData(GL_SHADER_STORAGE_BUFFER,
                         ctypes.sizeof(element_parameters_arr),
                         element_parameters_arr, GL_DYNAMIC_COPY)
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, self.gui_ssbo)

    # Generating data for drawing
    def generate_draw_data(self):
        """Generates draw commands for MultiDrawIndirect"""
        
        self.draw_command = \
            numpy.array([[6, len(self.element_parameters), 0, 0]],
                        ctypes.c_uint32)
    
    def generate_element_parameters(self):
        """Generates element parameters, transformation data"""
        
        gui_parameters = []
        for gui_name in self.guis:
            if self.guis[gui_name].enabled:
                for data in self.guis[gui_name].graphic_data.values():
                    gui_parameters.append(data)

        self.element_parameters = []
        
        gui_tex_path = md.MAIN_DIR.joinpath(
            'resources', 'textures', 'gui_tex')
        
        for graphic in gui_parameters:
            tex_path = gui_tex_path.joinpath(graphic['tex_name'])
            texture_id = list(self.texture_dict).index(tex_path)
    
            temp_list = [[*graphic['position'], *graphic['size']],
                         [*graphic['tex_pos'], *graphic['tex_size']],
                         [*[c / 255.0 for c in graphic['color']]],
                         [texture_id, 0, 0, 0]]
    
            self.element_parameters. \
                append(ElementData(*temp_list))
    
    def load_textures(self):
        """Loads gui textures"""
        
        self.texture_dict = {}
        
        textures_path = md.MAIN_DIR.joinpath(
            'resources', 'textures', 'gui_tex')
        
        for (dir_path, dir_names, file_names) in walk(textures_path):
            texture_id = 0
            for file_name in file_names:
                texture_path = pathlib.Path(dir_path, file_name)
                
                texture_name = file_name.split('.')[0]
                
                texture = go.Texture(texture_name, texture_id,
                                     texture_path)
                
                texture.load_data()
                
                self.texture_dict[texture_path] = texture


# Element data structure for sending data to GPU
class ElementData(ctypes.Structure):
    _fields_ = [('trans_scale', uty.Vec4),
                ('tex_coords', uty.Vec4),
                ('color', uty.Vec4),
                ('tex_index', uty.Vec4)]
    
    def __init__(self, trans_scale=None, tex_coords=None, color=None,
                 tex_index=None):
        if trans_scale is None:
            trans_scale = [0.0, 0.0, 0.0, 0.0]
        if tex_coords is None:
            tex_coords = [0.0, 0.0, 0.0, 0.0]
        if color is None:
            color = [1.0, 1.0, 1.0, 0.0]
        if tex_index is None:
            tex_index = [1.0, 1.0, 1.0, 0.0]
        
        t_trans_scale = uty.Vec4(trans_scale)
        t_tex_coords = uty.Vec4(tex_coords)
        t_color = uty.Vec4(color)
        t_tex_index = uty.Vec4(tex_index)
        
        super(ElementData, self).__init__(
            t_trans_scale, t_tex_coords, t_color, t_tex_index)
