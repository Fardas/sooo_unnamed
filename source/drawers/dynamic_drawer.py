"""Dynamic Drawer class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
from OpenGL.GL import *

# External libraries
import numpy

# Packages
#   Data classes
import source.data_classes.game_object as go
import source.data_classes.main_data as md
#   Control
import source.control.scenes as sc
#   Utilities
import source.utilities.opengl_utilities as ogl_uty
import source.utilities.utilities as uty


class DynamicDrawer:
    def __init__(self):
        
        # Array/Buffer Objects
        self.dynamic_vao = None
        self.dynamic_vbo = None
        self.dynamic_ebo = None
        self.dynamic_ibo = None
        self.dynamic_ssbo = None
        
        # Shaders
        self.dynamic_shader = None
        
        # Uniform location
        self.loc_model = 0
        self.loc_view = 0
        self.loc_projection = 0
        self.loc_eye = 0
        
        # Textures
        self.dynamic_tex_arr = None
        
        # Drawing
        self.draw_commands = []
        self.instance_parameters = []
    
    # Base functions
    def init(self):
        """DynamicDrawer class initialization"""
        
        # Check if scene is correct
        wrong_scene = \
            not isinstance(md.GameD.active_scene, sc.GameScene)

        if wrong_scene:
            return -1
        
        uty.DynamicLoader.load_elements()
        
        # Vertex array object
        self.dynamic_vao = glGenVertexArrays(1)
        glBindVertexArray(self.dynamic_vao)
        
        vertex_shader_path = md.MAIN_DIR.joinpath(
            'source', 'shaders', 'dynamicVS.vert')
        fragment_shader_path = md.MAIN_DIR.joinpath(
            'source', 'shaders', 'dynamicFS.frag')
        
        # Compile shaders
        self.dynamic_shader = ogl_uty.compile_shader(
            vertex_shader_path, fragment_shader_path)
        
        if self.dynamic_shader == -1 or self.dynamic_shader == -2:
            return -2
        
        # Generate draw commands and instance parameters
        self.generate_draw_data()

        # Get vertex/index data
        uty.DynamicLoader.generate_arrays()

        vertex_data, index_data = uty.DynamicLoader.get_arrays()
        
        # Stride for vbo attributes
        stride = vertex_data.itemsize * 8
        
        # Loading up vector buffer object
        self.dynamic_vbo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.dynamic_vbo)
        glBufferData(GL_ARRAY_BUFFER,
                     vertex_data.itemsize * len(vertex_data),
                     vertex_data, GL_STATIC_DRAW)
        
        glEnableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        glEnableVertexAttribArray(2)
        
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride,
                              ctypes.c_void_p(0))
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride,
                              ctypes.c_void_p(4 * 3))
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride,
                              ctypes.c_void_p(4 * 5))
        
        # Loading up element buffer object
        self.dynamic_ebo = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.dynamic_ebo)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     index_data.itemsize * len(index_data),
                     index_data, GL_STATIC_DRAW)

        # Generate transformation data for instances
        self.generate_instance_parameters()

        # Convert to ctypes array
        instance_parameters_arr = \
            (uty.Transformations3 * len(self.instance_parameters))\
                (*self.instance_parameters)
        
        # Generate id-s
        id_data = numpy.array(
            [i for i in range(0, len(self.instance_parameters))],
            ctypes.c_uint32)
        
        # Loading up index buffer object (drawID)
        self.dynamic_ibo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.dynamic_ibo)
        glBufferData(GL_ARRAY_BUFFER,
                     id_data.itemsize * len(id_data),
                     id_data, GL_STATIC_DRAW)
        
        glEnableVertexAttribArray(3)
        
        glVertexAttribIPointer(3, 1, GL_UNSIGNED_INT, 0,
                               ctypes.c_void_p(0))
        glVertexAttribDivisor(3, 1)
        
        # Loading up shader storage buffer object
        self.dynamic_ssbo = glGenBuffers(1)
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, self.dynamic_ssbo)
        glBufferData(GL_SHADER_STORAGE_BUFFER,
                     ctypes.sizeof(instance_parameters_arr),
                     instance_parameters_arr,
                     GL_DYNAMIC_COPY)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, self.dynamic_ssbo)

        texture_paths = [
            md.MAIN_DIR.joinpath('resources', 'textures', 'dynamic',
                                 'player', 'player_team_0.png'),
            md.MAIN_DIR.joinpath('resources', 'textures', 'dynamic',
                                 'player', 'player_team_a.png'),
            md.MAIN_DIR.joinpath('resources', 'textures', 'dynamic',
                                 'player', 'player_team_b.png')]
        
        for texture_path in texture_paths:
            
            texture_name = str(texture_path.stem)
            
            texture_id = len(go.Texture.dynamic_textures)
    
            texture = go.Texture(texture_name, texture_id,
                                 texture_path)
    
            texture.load_data()

            go.Texture.dynamic_textures[texture_path] = texture
        
        # Textures
        if go.Texture.dynamic_textures:
            self.dynamic_tex_arr = \
                ogl_uty.generate_texture_array(
                    go.Texture.dynamic_textures)
        
        # Uniform locations
        self.loc_model = \
            glGetUniformLocation(self.dynamic_shader, "model")

        self.loc_view = \
            glGetUniformLocation(self.dynamic_shader, "view")

        self.loc_projection = \
            glGetUniformLocation(self.dynamic_shader, "projection")

        self.loc_eye = \
            glGetUniformLocation(self.dynamic_shader, "eye")
        
        glBindVertexArray(0)
        
        return 0
        
    def reinit(self):
        """Reinitializes components of dynamic drawer
        if some of them are updated"""
        
        glBindVertexArray(self.dynamic_vao)
        
        # Generate draw commands
        self.generate_draw_data()
        
        # Generate transformation data for instances
        self.generate_instance_parameters()

        # Convert to ctypes array
        instance_parameters_arr = \
            (uty.Transformations4 * len(self.instance_parameters))\
                (*self.instance_parameters)
    
        # Generate id-s
        id_data = numpy.array(
            [i for i in range(0, len(self.instance_parameters))],
            ctypes.c_uint32)
    
        # Loading up index buffer object (drawID)
        glBindBuffer(GL_ARRAY_BUFFER, self.dynamic_ibo)
        glBufferData(GL_ARRAY_BUFFER,
                     id_data.itemsize * len(id_data),
                     id_data, GL_STATIC_DRAW)
    
        # Loading up shader storage buffer object
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, self.dynamic_ssbo)
        glBufferData(GL_SHADER_STORAGE_BUFFER,
                     ctypes.sizeof(instance_parameters_arr),
                     instance_parameters_arr, GL_DYNAMIC_COPY)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, self.dynamic_ssbo)
    
    def draw(self):
        """Draw dynamic elements"""
        
        glUseProgram(self.dynamic_shader)
        glBindVertexArray(self.dynamic_vao)
        glBindTexture(GL_TEXTURE_2D_ARRAY, self.dynamic_tex_arr)
        
        glUniformMatrix4fv(self.loc_model, 1, GL_FALSE,
                           md.OpenGLD.model)
        
        glUniformMatrix4fv(self.loc_view, 1, GL_FALSE,
                           md.OpenGLD.view)
        
        glUniformMatrix4fv(self.loc_projection, 1, GL_FALSE,
                           md.OpenGLD.projection)
        
        glUniform3fv(self.loc_eye, 1, md.OpenGLD.eye)
        
        glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT,
                                    self.draw_commands,
                                    len(self.draw_commands), 0)

        glBindVertexArray(0)
    
    # Generating data for drawing
    def generate_draw_data(self):
        """Generates draw commands for MultiDrawIndirect"""
        
        index_offset = 0        # index offset (for firstIndex)
        vertex_offset = 0       # vertex offset (for baseVertex)
        instance_offset = 0     # instance offset (for baseInstance)
    
        draw_commands = []

        # Iterate through object types
        for element in md.GameD.dynamic_elements.values():
    
            # Element count of current object
            element_count = len(element.game_object.model.indices)
    
            # Instance count of current object
            instance_count = len(element.transformations)
    
            # Draw command per object type
            draw_commands.append([
                element_count,
                instance_count,
                index_offset,
                vertex_offset,
                instance_offset
            ])
    
            index_offset += element_count
            vertex_offset += len(element.game_object.model.vertices)//3
            instance_offset += instance_count

        self.draw_commands = numpy.array(draw_commands, ctypes.c_uint32)
    
    def generate_instance_parameters(self):
        """Generates instance parameters, transformation data"""

        player_team_paths = \
            md.MAIN_DIR.joinpath('resources', 'textures', 'dynamic',
                                 'player')
        
        self.instance_parameters = []
        for element in md.GameD.dynamic_elements.values():
            # If the element is a player
            if element.element_name == 'male' or \
                    element.element_name == 'female':
                
                # Iterate trough element instances
                i = 0
                for player_name in element.transformations:
                    # Extend translations with model and texture ids
                    instance_tr = element.transformations[player_name]
                    
                    team = md.GameD.players[player_name].team
                    
                    team_path = player_team_paths.joinpath(
                        'player_' + team + '.png')
                    
                    texture2_id = go.Texture.\
                        dynamic_textures[team_path].texture_id
                    
                    tr = [[instance_tr.translation.x,
                           instance_tr.translation.y,
                           instance_tr.translation.z,
                           element.game_object.model.model_id],
                          [instance_tr.rotation.x,
                           instance_tr.rotation.y,
                           instance_tr.rotation.z,
                           element.game_object.texture.texture_id],
                          [instance_tr.scale.x,
                           instance_tr.scale.y,
                           instance_tr.scale.z,
                           texture2_id]]
                    i += 1
        
                    tr = uty.Transformations4(*tr.copy())
        
                    self.instance_parameters.append(tr)
                
            else:
                # Iterate trough element instances
                for instance_tr in element.transformations.values():
                    # Extend translations with model and texture ids
                    
                    tr = [[instance_tr.translation.x,
                           instance_tr.translation.y,
                           instance_tr.translation.z,
                           element.game_object.model.model_id],
                          [instance_tr.rotation.x,
                           instance_tr.rotation.y,
                           instance_tr.rotation.z,
                           element.game_object.texture.texture_id],
                          [instance_tr.scale.x,
                           instance_tr.scale.y,
                           instance_tr.scale.z,
                           1024]]
        
                    tr = uty.Transformations4(*tr.copy())
        
                    self.instance_parameters.append(tr)
