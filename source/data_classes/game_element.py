"""Game element classes"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import json

from typing import List, Dict

# Packages
#   Data classes
import source.data_classes.game_object as go
import source.data_classes.main_data as md
#   Utilities
import source.utilities.utilities as uty


class DynamicElement:
    def __init__(self, element_name: str, element_id: int,
                 game_object: 'go.GameObject',
                 transformations: 'Dict[str, uty.Transformations3]'):
        self.element_name = element_name
        self.element_id = element_id
        self.game_object = game_object
        self.transformations = transformations
    
    @classmethod
    def preload_elements(cls):
        """Preload dynamic elements without transformations"""
        
        element_id = 0
        if go.GameObject.dynamic_objects:
            for object_name in go.GameObject.dynamic_objects:
                game_object = go.GameObject.dynamic_objects[object_name]
                
                de = cls(element_name=game_object.object_name,
                         element_id=element_id,
                         game_object=game_object,
                         transformations={})
                md.GameD.dynamic_elements[game_object.object_name] = de
                element_id += 1
            
            return 0
        
        return -1
    
    def serialize(self):
        """Encode the necessary data to json to send over network"""
        
        temp_trs = {}
        for tr_name in self.transformations:
            temp_trs[tr_name] = \
                self.transformations[tr_name].serialize()
        
        json_dict = {'en': self.element_name,
                     'tr': temp_trs}
        
        return json_dict
    
    def load_transformations(self, tr_dict):
        """Load transformations from a dict"""
        
        self.transformations = {}
        if tr_dict:
            for tr_name in tr_dict:
                tr = tr_dict[tr_name]
                
                new_tr = [[tr[0], 0, tr[1]],
                          [0.0, tr[2], 0.0],
                          [1.0, 1.0, 1.0]]
                self.transformations[tr_name] = \
                    uty.Transformations3(*new_tr)


class StaticElement:
    def __init__(self, element_name: str, element_id: int,
                 game_object: 'go.GameObject',
                 transformations: 'List[uty.Transformations3]'):
        self.element_name = element_name
        self.element_id = element_id
        self.game_object = game_object
        self.transformations = transformations


class Player:
    MAX_HEALTH = 5
    
    def __init__(self, name: str, team: str, player_model: str,
                 health: int = MAX_HEALTH):
        self.name = name
        self.team = team
        self.player_model = player_model
        self.health = health
    
    def to_json(self):
        """Encode the necessary data to json to send over network"""

        json_bytes = json.dumps(self.__dict__).encode()
        
        return json_bytes
    
    def dec_health(self, amount: int = 1):
        """Decrease health with amount,
        returns True if health is 0 or below"""
        
        self.health -= amount
        
        return self.health <= 0
    
    def revive(self):
        """Gives player full health"""
        
        self.health = Player.MAX_HEALTH
