"""Game object classes"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import ast
import ctypes
import inspect
import pathlib

from enum import Enum
from PIL import Image
from typing import List

# External libraries
import numpy

# Packages
#   Data classes
import source.data_classes.main_data as md


class GameObject:
    static_objects = {}
    dynamic_objects = {}
    
    def __init__(self, object_name: str, object_id: int,
                 model: 'Model', texture: 'Texture', size: List[float],
                 collision_type: 'CollisionType', marker: bool):
        self.object_name = object_name
        self.object_id = object_id
        self.model = model
        self.texture = texture
        self.size = size
        self.collision_type = collision_type
        self.marker = marker
    
    @classmethod
    def load_object(cls, object_path, object_type):
        """Loads object from object file"""
        
        if object_path not in cls.static_objects and \
                object_path not in cls.dynamic_objects:
            
            if not object_path.exists():
                return None, -1
            
            with object_path.open('r') as game_object_file:
                args = inspect.getfullargspec(GameObject).args
                args.remove('self')

                attr_dict = {'object_id': None}
                for line in game_object_file:
                    attr_name, attr_value = line.strip('\n').split(':')
                    
                    try:
                        attr_dict[attr_name] = \
                            ast.literal_eval(attr_value)
                    
                    except (ValueError, SyntaxError):
                        attr_dict[attr_name] = attr_value
                
                if set(list(attr_dict)) == set(args):
                    # New object id
                    if object_type == 'static':
                        attr_dict['object_id'] = \
                            len(cls.static_objects)
                        
                    elif object_type == 'dynamic':
                        attr_dict['object_id'] = \
                            len(cls.dynamic_objects)
                    
                    # Model:
                    model_path = md.MAIN_DIR.joinpath(
                        'resources', 'meshes', attr_dict['model'])
                    
                    model_name = str(model_path.stem)

                    model = None
                    if object_type == 'static':
                        if model_path not in Model.static_models:
                            model_id = len(Model.static_models)
                            
                            model = Model(model_name, model_id,
                                          model_path)
                            
                            success = model.load_data()
                            
                            if success != 0:
                                return None, -2
                            
                            Model.static_models[model_path] = model
                        
                        else:
                            model = Model.static_models[model_path]
                    
                    elif object_type == 'dynamic':
                        if model_path not in Model.dynamic_models:
                            model_id = len(Model.dynamic_models)
                            
                            model = Model(model_name, model_id,
                                          model_path)
                            
                            success = model.load_data()
                            
                            if success != 0:
                                return None, -2
                            
                            Model.dynamic_models[model_path] = model
                        
                        else:
                            model = Model.dynamic_models[model_path]
                    
                    attr_dict['model'] = model
                    
                    # Texture
                    texture_path = md.MAIN_DIR.joinpath(
                        'resources', 'textures', attr_dict['texture'])
                    
                    texture_name = str(texture_path.stem)
                    
                    texture = None
                    if object_type == 'static':
                        if texture_path not in Texture.static_textures:
                            texture_id = len(Texture.static_textures)
                            
                            texture = Texture(texture_name, texture_id,
                                              texture_path)
                            
                            success = texture.load_data()
                            
                            if success != 0:
                                return None, -3
    
                            Texture.static_textures[texture_path] = \
                                texture
    
                        else:
                            texture = \
                                Texture.static_textures[texture_path]

                    elif object_type == 'dynamic':
                        if texture_path not in Texture.dynamic_textures:
                            texture_id = len(Texture.dynamic_textures)
        
                            texture = Texture(texture_name, texture_id,
                                              texture_path)
                            
                            success = texture.load_data()
                            
                            if success != 0:
                                return None, -3
        
                            Texture.dynamic_textures[texture_path] = \
                                texture
    
                        else:
                            texture = \
                                Texture.dynamic_textures[texture_path]
                    
                    attr_dict['texture'] = texture
                    
                    # Collision type
                    attr_dict['collision_type'] = \
                        CollisionType[attr_dict['collision_type']]
                    
                    if object_type == 'static':
                        cls.static_objects[object_path] = \
                            GameObject(**attr_dict)
                        
                        # Object
                        return cls.static_objects[object_path], 0

                    elif object_type == 'dynamic':
                        cls.dynamic_objects[object_path] = \
                            GameObject(**attr_dict)
    
                        # Object
                        return cls.dynamic_objects[object_path], 0
                    
                else:
                    print('Error: missing or wrong attributes in file!')
                    return None, -4
        
        else:
            if object_type == 'static':
                return cls.static_objects[object_path], 0
            
            elif object_type == 'dynamic':
                return cls.dynamic_objects[object_path], 0


class Model:
    static_models = {}
    dynamic_models = {}
    
    def __init__(self, model_name: str, model_id: int,
                 model_path: pathlib.Path):
        self.model_name = model_name
        self.model_id = model_id
        self.model_path = model_path
        self.vertices = []
        self.tex_coords = []
        self.normals = []
        self.indices = []
    
    def load_data(self):
        """Loads mesh data from .obj file"""
        
        vertex_coordinates = []
        texture_coordinates = []
        normal_coordinates = []
        
        vertex_index = []
        texture_index = []
        normal_index = []
        
        if not self.model_path.exists():
            return -1
        
        with self.model_path.open('r') as object_file:
            try:
                for line in object_file:
                    if line.startswith('#'):
                        continue
                    
                    values = line.split()
                    if not values:
                        continue
                    
                    if values[0] == 'v':
                        vertex_coordinates.append(
                            [float(x) for x in values[1:4]])
                    elif values[0] == 'vt':
                        texture_coordinates.append(
                            [float(x) for x in values[1:3]])
                    elif values[0] == 'vn':
                        normal_coordinates.append(
                            [float(x) for x in values[1:4]])
                    
                    elif values[0] == 'f':
                        temp_face = []
                        temp_text = []
                        temp_norm = []
                        
                        for face in values[1:4]:
                            point = face.split('/')
                            temp_face.append(int(point[0]) - 1)
                            temp_text.append(int(point[1]) - 1)
                            temp_norm.append(int(point[2]) - 1)
                        
                        vertex_index.extend(temp_face)
                        texture_index.extend(temp_text)
                        normal_index.extend(temp_norm)
                    
                    else:
                        continue
                    
            except ValueError:
                return -2
        
        try:
            num = 0
            face_dict = {}
            for i in range(0, len(vertex_index)):
                temp_str = str(vertex_index[i]) + '/' + str(
                    texture_index[i]) + '/' + str(normal_index[i])
                if temp_str not in face_dict:
                    face_dict[temp_str] = num
                    
                    self.vertices.extend(
                        vertex_coordinates[vertex_index[i]])
                    self.tex_coords.extend(
                        texture_coordinates[texture_index[i]])
                    self.normals.extend(
                        normal_coordinates[normal_index[i]])
                    
                    self.indices.append(num)
                    num += 1
                else:
                    self.indices.append(face_dict[temp_str])
        
        except IndexError:
            return -3
        
        return 0
    
    def get_mesh(self):
        """Puts mesh data into one, merged list"""
        
        mesh = []
        for index in range(0, len(self.vertices) // 3):
            for vi in range(index * 3, index * 3 + 3):
                mesh.append(self.vertices[vi])
            for ti in range(index * 2, index * 2 + 2):
                mesh.append(self.tex_coords[ti])
            for ni in range(index * 3, index * 3 + 3):
                mesh.append(self.normals[ni])
            
        return mesh.copy(), self.indices


class Texture:
    static_textures = {}
    dynamic_textures = {}
    
    def __init__(self, texture_name: str, texture_id: int,
                 texture_path: pathlib.Path):
        self.texture_name = texture_name
        self.texture_id = texture_id
        self.texture_path = texture_path
        self.image = None
    
    def load_data(self):
        """Loads image data from image file"""
        
        if not self.texture_path.exists():
            return -1
        
        try:
            image = Image.open(str(self.texture_path))
            image.verify()
            image.close()
        
            image = Image.open(str(self.texture_path))
            image = image.transpose(Image.FLIP_TOP_BOTTOM)
            image = image.convert('RGBA')
            self.image = numpy.array(image, ctypes.c_uint8)
            image.close()
        
        except IOError:
            return -2
        
        return 0


class CollisionType(Enum):
    NO_COLLISION = 0
    CIRCLE = 1
    RECTANGLE = 2


def reset():
    """Reset game_object classes to default"""

    GameObject.static_objects = {}
    GameObject.dynamic_objects = {}

    Model.static_models = {}
    Model.dynamic_models = {}

    Texture.static_textures = {}
    Texture.dynamic_textures = {}
