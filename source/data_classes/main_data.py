"""Main data classes"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
from typing import Dict

# Packages
#   Data classes
import source.data_classes.game_element as ge

MAIN_DIR = None

SPEED_CONST = 15
BULLET_SPEED_CONST = 40
BULLET_TIME = 0.25


# Game data
class GameD:
    # Scenes
    active_scene = None
    
    # Map
    map_name = None
    game_mode = None
    
    # Game elements
    #   Static
    static_elements: 'Dict[str, ge.StaticElement]' = {}
    
    #   Dynamic
    dynamic_elements: 'Dict[str, ge.DynamicElement]' = {}
    
    #   Players
    players = {}
    active_player = None
    
    # Scores:
    team_score = {'team_a': 0, 'team_b': 0}
    player_score = tuple()
    
    # Match time
    current_match_time = [0, 0]
    
    # Events:
    event_list = []
    
    # Settings
    client_settings = {}


# OpenGL related data
class OpenGLD:
    # GLFW window
    window = None
    window_width = 1280
    window_height = 720
    aspect_ratio = [16, 9]
    window_focus = True
    
    # OpenGL time
    delta_time = 0.0
    update_delta_time = 0.0
    last_frame = 0.0
    FPS_LIMIT = 1 / 60

    # Camera
    camera = None
    fov = 45.0
    eye = None

    # Transformation Matrices
    model = None
    view = None
    projection = None


# User input related data
class UInputD:
    # Mouse
    cursor_enabled = True
    first_mouse_interaction = True
    last_x, last_y = 0.0, 0.0
    
    # Player movement
    enable_control = True
    
    move_north = 0
    move_south = 0
    move_east = 0
    move_west = 0
    
    # Player rotation
    player_rotation = 0.0
    
    # Joystick
    joystick_connected = False
    joystick_enabled = False
    
    u_joystick = None
    key_catch = False
