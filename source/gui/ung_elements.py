"""UNG element types"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import re

from typing import List

# Packages
#   Data classes
import source.data_classes.main_data as md


class UNGElement:
    def __init__(self, name: 'str' = 'Element',
                 position: 'List[float]' = None,
                 size: 'List[float]' = None,
                 selectable: 'Bool' = False,
                 tex_pos: 'List[float]' = None,
                 tex_size: 'List[float]' = None,
                 color: 'List[int]' = None,
                 tex_name: 'str' = 'frame.png',
                 visible: 'bool' = True,
                 text: 'UNGText' = None):
        
        self.TYPE = 'UNGElement'
        self.name = name
        self.selectable = selectable
        self.tex_name = tex_name
        self.visible = visible
        self.text = text
        self.sub_elements = {}
        
        if position is None:
            self.position = [0, 0]
        else:
            self.position = position
    
        if size is None:
            self.size = [0, 0]
        else:
            self.size = size
    
        if tex_pos is None:
            self.tex_pos = [0.0, 0.0]
        else:
            self.tex_pos = tex_pos
    
        if tex_size is None:
            self.tex_size = [1.0, 1.0]
        else:
            self.tex_size = tex_size
    
        if color is None:
            self.color = [255, 255, 255, 255]
        else:
            self.color = color
    
    def __str__(self):
        return 'Name: ' + self.name + \
               '\n  Type: ' + self.TYPE + \
               '\n  Position: ' + str(self.position) + ', ' + \
               ';\n  Size: ' + str(self.size) + \
               ';\n  Selectable: ' + str(self.selectable) + \
               ';\n  Texture position: ' + str(self.tex_pos) + \
               ';\n  Texture size: ' + str(self.tex_size) + \
               ';\n  Color: ' + str(self.color) + \
               ';\n  Texture name: ' + str(self.tex_name) + \
               ';\n  Visible: ' + str(self.visible) + \
               ';\n  Sub elements: ' + str(self.sub_elements) + \
               ';\n  Text: ' + str(self.text)


class UNGFrame(UNGElement):
    def __init__(self, name: 'str' = 'Frame',
                 position: 'List[float]' = None,
                 size: 'List[float]' = None,
                 selectable: 'Bool' = False,
                 tex_pos: 'List[float]' = None,
                 tex_size: 'List[float]' = None,
                 color: 'List[int]' = None,
                 tex_name: 'str' = 'frame.png',
                 visible: 'bool' = True,
                 text: 'UNGText' = None):
        
        super().__init__(name=name, position=position,
                         size=size, selectable=selectable,
                         tex_pos=tex_pos, tex_size=tex_size,
                         color=color, tex_name=tex_name,
                         visible=visible, text=text)
        self.TYPE = 'UNGFrame'
    
    def __str__(self):
        return super().__str__()


class UNGButton(UNGElement):
    def __init__(self, name: 'str' = 'Button',
                 position: 'List[float]' = None,
                 size: 'List[float]' = None,
                 selectable: 'Bool' = True,
                 tex_pos: 'List[float]' = None,
                 tex_size: 'List[float]' = None,
                 color: 'List[int]' = None,
                 tex_name: 'str' = 'frame.png',
                 visible: 'bool' = True,
                 text: 'UNGText' = None,
                 value=None,
                 enabled: 'bool' = True):
        
        super().__init__(name=name, position=position,
                         size=size, selectable=selectable,
                         tex_pos=tex_pos, tex_size=tex_size,
                         color=color, tex_name=tex_name,
                         visible=visible, text=text)
        self.TYPE = 'UNGButton'
        self.enabled = enabled
        self.value = value
    
    def __str__(self):
        return super().__str__() + \
               ';\n  Enabled: ' + str(self.enabled) + \
               ';\n  Value: ' + str(self.value)


class UNGTextfield(UNGElement):
    def __init__(self, name: 'str' = 'Textfield',
                 position: 'List[float]' = None,
                 size: 'List[float]' = None,
                 selectable: 'Bool' = False,
                 tex_pos: 'List[float]' = None,
                 tex_size: 'List[float]' = None,
                 color: 'List[int]' = None,
                 tex_name: 'str' = 'frame.png',
                 visible: 'bool' = True,
                 text: 'UNGText' = None):
        
        super().__init__(name=name, position=position,
                         size=size, selectable=selectable,
                         tex_pos=tex_pos, tex_size=tex_size,
                         color=color, tex_name=tex_name,
                         visible=visible, text=text)
        self.TYPE = 'UNGTextfield'
    
    def __str__(self):
        return super().__str__()


class UNGInputTextfield(UNGTextfield):
    def __init__(self, name: 'str' = 'InputTextfield',
                 position: 'List[float]' = None,
                 size: 'List[float]' = None,
                 selectable: 'Bool' = True,
                 tex_pos: 'List[float]' = None,
                 tex_size: 'List[float]' = None,
                 color: 'List[int]' = None,
                 tex_name: 'str' = 'frame.png',
                 visible: 'bool' = True,
                 text: 'UNGText' = None,
                 enabled: 'bool' = True,
                 whitelist: 'str' = '.'):
        
        super().__init__(name=name, position=position,
                         size=size, selectable=selectable,
                         tex_pos=tex_pos, tex_size=tex_size,
                         color=color, tex_name=tex_name,
                         visible=visible, text=text)
        self.TYPE = 'UNGInputTextfield'
        self.enabled = enabled
        self.text.value = self.text.value.replace('/br/', ' ')
        self.whitelist = whitelist
    
    def __str__(self):
        return super().__str__() + \
               ';\n  Enabled: ' + str(self.enabled) + \
               ';\n  Whitelist: ' + str(self.whitelist)


class UNGCharacter(UNGElement):
    def __init__(self, name: 'str' = 'Character',
                 position: 'List[float]' = None,
                 size: 'List[float]' = None,
                 selectable: 'Bool' = False,
                 color: 'List[int]' = None,
                 tex_name: 'str' = 'share_tech_mono.png',
                 visible: 'bool' = True,
                 value: 'str' = 'c',
                 container: 'UNGElement' = None):
        
        char_data = character_table[value]
        tex_size = [(char_data[1] + 0.8) / 16.0,
                    (char_data[0] + 1.0) / 16.0]
        tex_pos = [(char_data[1] + 0.2) / 16.0,
                   char_data[0] / 16.0]
        
        super().__init__(name=name, position=position,
                         size=size, selectable=selectable,
                         tex_pos=tex_pos, tex_size=tex_size,
                         color=color, tex_name=tex_name,
                         visible=visible)

        if color is None:
            self.color = [0, 0, 0, 255]
        else:
            self.color = color
        
        self.TYPE = 'UNGCharacter'
        self.value = value
        self.container = container
    
    def __str__(self):
        return super().__str__() + \
               ';\n  Value:' + str(self.value) + \
               ';\n  Container:' + str(self.container)


class UNGText:
    def __init__(self, container: 'UNGElement' = None,
                 value: 'str' = '',
                 font_size: 'float' = 0.05,
                 color: 'List[int]' = None,
                 align_h: 'str' = 'left',
                 align_v: 'str' = 'top'):
        
        self.container = container
        self.value = value
        self.font_size = font_size
        self.align_h = align_h
        self.align_v = align_v
        self.characters = None
        
        if color is None:
            self.color = [0, 0, 0, 255]
        else:
            self.color = color
    
    def __str__(self):
        return 'Text: ' + self.value + \
               '\n  Font size: ' + str(self.font_size) + \
               '\n  Align horizontally: ' + self.align_h + \
               '\n  Align vertically: ' + self.align_v + \
               '\n  Characters: ' + str(self.characters)
    
    def gen_characters(self):
        cont_pos = self.container.position
        cont_size = self.container.size
        
        char_size = [self.font_size * md.OpenGLD.aspect_ratio[1] /
                     md.OpenGLD.aspect_ratio[0] * 0.6, self.font_size]
        max_columns = int(cont_size[0] // char_size[0])
        max_rows = int(cont_size[1] // char_size[1])
        
        text_lines = []
        for line in self.value.split('/br/'):
            i = 0
            while i < len(line):
                if re.match('[^ ] ', line[i:]):
                    line = line[0:i + 1] + '/wb/' + line[i + 1:]
                    i += 4
                i += 1
            
            words = line.split('/wb/')
            
            i = 0
            while i < len(words):
                if max_columns < len(words[i]):
                    words.insert(i + 1, words[i][max_columns:])
                    words[i] = words[i][0:max_columns]
                i += 1
            
            temp_line = ''
            for word in words:
                if len(temp_line) + len(word) <= max_columns:
                    temp_line += word
                
                else:
                    text_lines.append(temp_line)
                    temp_line = word
            
            text_lines.append(temp_line)
        
        char_dict = {}
        row = 0
        while row < max_rows and row < len(text_lines):
            line = text_lines[row]
            
            char_pos = [0.5, 0.5]
            column = 0
            for ch in line:
                # horizontal alignment
                if self.align_h == 'left':
                    char_pos[0] = \
                        cont_pos[0] + char_size[0] * column
                
                elif self.align_h == 'right':
                    char_pos[0] = \
                        cont_pos[0] + cont_size[0] - char_size[0] * \
                        (len(line) - column)
                
                elif self.align_h == 'center':
                    char_pos[0] = \
                        cont_pos[0] + char_size[0] * column + \
                        (cont_size[0] - char_size[0] * len(line)) / 2
                
                # vertical alignment
                if self.align_v == 'top':
                    char_pos[1] = \
                        cont_pos[1] + char_size[1] * row
                
                elif self.align_v == 'bottom':
                    y_offset = min(len(text_lines), max_rows)
                    char_pos[1] = \
                        cont_pos[1] + cont_size[1] - char_size[1] * \
                        (y_offset - row)
                
                elif self.align_v == 'center':
                    cut_height = min(len(text_lines), max_rows)
                    char_pos[1] = \
                        cont_pos[1] + char_size[1] * row + \
                        (cont_size[1] - char_size[1] * cut_height) / 2
                
                char_name = self.container.name + '_' + ch + '_' + \
                            str(row) + '_' + str(column)
                char_dict[char_name] = \
                    UNGCharacter(name=char_name,
                                 position=char_pos.copy(),
                                 size=char_size, color=self.color,
                                 visible=True, value=ch,
                                 container=self.container)
                column += 1
            row += 1
        
        self.characters = char_dict


# Character positions in character table [row number, column number]
character_table = {
    'A': [0, 0], 'Á': [0, 1], 'B': [0, 2], 'C': [0, 3],
    'D': [0, 4], 'E': [0, 5], 'É': [0, 6], 'F': [0, 7],
    'G': [0, 8], 'H': [0, 9], 'I': [0, 10], 'Í': [0, 11],
    'J': [0, 12], 'K': [0, 13], 'L': [0, 14], 'M': [0, 15],
    'N': [1, 0], 'O': [1, 1], 'Ó': [1, 2], 'Ö': [1, 3],
    'Ő': [1, 4], 'P': [1, 5], 'Q': [1, 6], 'R': [1, 7],
    'S': [1, 8], 'T': [1, 9], 'U': [1, 10], 'Ú': [1, 11],
    'Ü': [1, 12], 'Ű': [1, 13], 'V': [1, 14], 'W': [1, 15],
    'X': [2, 0], 'Y': [2, 1], 'Z': [2, 2], 'a': [2, 3],
    'á': [2, 4], 'b': [2, 5], 'c': [2, 6], 'd': [2, 7],
    'e': [2, 8], 'é': [2, 9], 'f': [2, 10], 'g': [2, 11],
    'h': [2, 12], 'i': [2, 13], 'í': [2, 14], 'j': [2, 15],
    'k': [3, 0], 'l': [3, 1], 'm': [3, 2], 'n': [3, 3],
    'o': [3, 4], 'ó': [3, 5], 'ö': [3, 6], 'ő': [3, 7],
    'p': [3, 8], 'q': [3, 9], 'r': [3, 10], 's': [3, 11],
    't': [3, 12], 'u': [3, 13], 'ú': [3, 14], 'ü': [3, 15],
    'ű': [4, 0], 'v': [4, 1], 'w': [4, 2], 'x': [4, 3],
    'y': [4, 4], 'z': [4, 5], '1': [4, 6], '2': [4, 7],
    '3': [4, 8], '4': [4, 9], '5': [4, 10], '6': [4, 11],
    '7': [4, 12], '8': [4, 13], '9': [4, 14], '0': [4, 15],
    '_': [5, 0], ' ': [5, 1], '\'': [5, 2], '"': [5, 3],
    '?': [5, 4], '!': [5, 5], '(': [5, 6], ')': [5, 7],
    '[': [5, 8], ']': [5, 9], '{': [5, 10], '}': [5, 11],
    '#': [5, 12], '%': [5, 13], '@': [5, 14], '/': [5, 15],
    '\\': [6, 0], '<': [6, 1], '>': [6, 2], '-': [6, 3],
    '+': [6, 4], '&': [6, 5], '=': [6, 6], '®': [6, 7],
    '©': [6, 8], '$': [6, 9],
    ':': [6, 14], ';': [6, 15],
    ',': [7, 0], '.': [7, 1], '*': [7, 2], '~': [7, 3],
    'cur': [15, 14]
}
