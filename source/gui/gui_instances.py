"""GUI instances"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import ast
import json

# External libraries
import glfw

# Packages
#   Data classes
import source.data_classes.game_element as ge
import source.data_classes.main_data as md
#   Control
import source.control.scenes as sc
import source.control.unnamed_game as ug
#   GUI
import source.gui.ung_elements as ue
import source.gui.unnamed_gui as ung


class MainMenuGUI(ung.UNG):
    def __init__(self, name='main_menu', enabled=True):
        super().__init__(name=name, enabled=enabled)

        # Start game
        self.gui_events['start_game'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.start_game,
                 'args': []}}

        # Joystick settings
        self.gui_events['joystick_settings'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.joystick_settings,
                 'args': []}}
        
        # Quit game
        self.gui_events['exit'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.exit_game,
                 'args': []}}
    
    @staticmethod
    def start_game(args=None):
        md.GameD.active_scene.guis['main_menu'].\
            change_enabled_state(False)
        
        md.GameD.active_scene.guis['game_select'].\
            change_enabled_state(True)
        
        md.GameD.active_scene.active_gui_name = 'game_select'

        success = \
            md.GameD.active_scene.guis['game_select'].load_settings()
        
        if success != 0:
            return -1
    
    @staticmethod
    def joystick_settings(args=None):
        md.GameD.active_scene.guis['main_menu'].\
            change_enabled_state(False)
        
        md.GameD.active_scene.guis['joystick_settings'].\
            change_enabled_state(True)
        
        md.GameD.active_scene.active_gui_name = 'joystick_settings'

        md.GameD.active_scene.guis['joystick_settings'].load_settings()
    
    @staticmethod
    def exit_game(args=None):
        GameSelectGUI.save_to_file(md.GameD.client_settings)
        ug.UnnamedGame.exit_game()


# Game select/options gui
class GameSelectGUI(ung.UNG):
    def __init__(self, name='game_select', enabled=True):
        super().__init__(name=name, enabled=enabled)

        # Change team
        self.gui_events['player_team'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.change_team,
                 'args': []}}

        # Change player model
        self.gui_events['player_model'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.change_player_model,
                 'args': []}}
        
        # Change team
        self.gui_events['joystick_switch'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.switch_joystick,
                 'args': []}}
        
        # Join game
        self.gui_events['join_game'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.join_game,
                 'args': ['server_address', 'server_port',
                          'player_name', 'player_team',
                          'player_model']}}

        # Main menu
        self.gui_events['main_menu'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.main_menu,
                 'args': []}}
    
    def join_game(self, args):
        """Get data for connection and connect to server"""

        self.save_settings()
        
        if len(args) != 5:
            return -1
        
        error_textfield = self.search_in_elements('error_textfield')
        
        if error_textfield is not None:
            attr_dict = {}
            for element_name in args:
                element = self.search_in_elements(element_name)

                if element is not None:
                    object_value = element.text.value
                    attr_dict[element_name] = object_value
                
                else:
                    return -1
            
            try:
                attr_dict['server_port'] = \
                    ast.literal_eval(attr_dict['server_port'])
            
            except ValueError:
                return -1
            
            game_scene = sc.GameScene(**attr_dict)
    
            connected = game_scene.connect()
            
            if connected != 0:
                if connected == -1:
                    error_textfield.text.value = 'TIMEOUT ERROR'
                
                elif connected == -2:
                    error_textfield.text.value = 'CONNECTION ERROR'
                
                elif connected == -3:
                    error_textfield.text.value = 'SERVER IS FULL'
    
                error_textfield.text.gen_characters()
                self.reveal_element(error_textfield)
    
            else:
                success = ug.UnnamedGame.change_scene(game_scene)
                
                if success == -2:
                    self.load_settings()
                    
                    error_textfield = \
                        self.search_in_elements('error_textfield')
                    
                    error_textfield.text.value = \
                        'MAP ' + md.GameD.map_name + ' NOT FOUND OR' + \
                        ' CORRUPTED'
    
                    error_textfield.text.gen_characters()
                    self.reveal_element(error_textfield)
                
                if success != 0:
                    return success
            
            return 0
        
        return -1
        
    def change_team(self, args=None):
        """Iterate through teams, and choose one"""

        team_button = self.search_in_elements('player_team')

        if team_button is not None:
            if team_button.text.value == 'team_0':
                team_button.text.value = 'team_a'
            
            elif team_button.text.value == 'team_a':
                team_button.text.value = 'team_b'
            
            else:
                team_button.text.value = 'team_0'
            
            team_button.text.gen_characters()
            self.gen_graphic_data()
            md.GameD.active_scene.ung_drawer.reinit()
            
            return 0
            
        return -1
    
    def change_player_model(self, args=None):
        """Iterate through player models"""

        team_button = self.search_in_elements('player_model')
        
        if team_button is not None:
            if team_button.text.value == 'male':
                team_button.text.value = 'female'
            
            else:
                team_button.text.value = 'male'
            
            team_button.text.gen_characters()
            self.gen_graphic_data()
            md.GameD.active_scene.ung_drawer.reinit()
            
            return 0
            
        return -1

    def switch_joystick(self, args=None):
        """Switch joystick on/off"""

        joystick_switch = self.search_in_elements('joystick_switch')
    
        if joystick_switch is not None:
            if joystick_switch.text.value == 'True':
                joystick_switch.text.value = 'False'
        
            elif joystick_switch.text.value == 'False':
                joystick_switch.text.value = 'True'

            joystick_switch.text.gen_characters()
            self.gen_graphic_data()
            md.GameD.active_scene.ung_drawer.reinit()
        
            return 0
    
        return -1
    
    def main_menu(self, args=None):
        """Change back to main menu"""

        self.save_settings()
        
        error_textfield = self.search_in_elements('error_textfield')
        
        if error_textfield is not None:
            if error_textfield.visible:
                self.hide_element(error_textfield)
            
            md.GameD.active_scene.guis['game_select'].\
                change_enabled_state(False)
            
            md.GameD.active_scene.guis['main_menu'].\
                change_enabled_state(True)
            
            md.GameD.active_scene.active_gui_name = 'main_menu'
            
            return 0
        
        return -1
    
    # Save/Load client config file
    def load_settings(self, args=None):
        """Load client settings"""

        options_frame = self.search_in_elements('options_frame')
        
        if options_frame is not None:
            for element in options_frame.sub_elements.values():
                try:
                    if element.name == 'server_address':
                        element.text.value = \
                            str(md.GameD.client_settings[element.name])
        
                    elif element.name == 'server_port':
                        element.text.value = \
                            str(md.GameD.client_settings[element.name])
        
                    elif element.name == 'player_name':
                        element.text.value = \
                            str(md.GameD.client_settings[element.name])
        
                    elif element.name == 'player_team':
                        element.text.value = \
                            str(md.GameD.client_settings[element.name])
        
                    elif element.name == 'player_model':
                        element.text.value = \
                            str(md.GameD.client_settings[element.name])
        
                    elif element.name == 'joystick_switch':
                        element.text.value = \
                            str(md.GameD.client_settings[element.name])
                
                except KeyError:
                    return -2
                
                element.text.gen_characters()
        
            self.gen_graphic_data()
            md.GameD.active_scene.ung_drawer.reinit()
        
            return 0
    
        return -1
    
    def save_settings(self, args=None):
        """Save settings into file"""
    
        options_frame = self.search_in_elements('options_frame')
    
        ref_dict = {'server_address': '127.0.0.1',
                    'server_port': 5200,
                    'player_name': 'Unnamed',
                    'player_team': 'team_0',
                    'player_model': 'male',
                    'joystick_switch': True,
                    'fullscreen': False}
    
        enum_strings = {'player_team': ['team_0', 'team_a', 'team_b'],
                        'player_model': ['male', 'female']}
    
        if options_frame is not None:
            attr_dict = {}
            for element_name in ref_dict:
                element = self.search_in_elements(element_name)
            
                if element is not None:
                    object_value = element.text.value
                    attr_dict[element_name] = object_value
        
            try:
                attr_dict['server_port'] = \
                    ast.literal_eval(attr_dict['server_port'])
            
                attr_dict['joystick_switch'] = \
                    ast.literal_eval(attr_dict['joystick_switch'])
        
            except (ValueError, SyntaxError):
                pass
            
            attr_dict['fullscreen'] = \
                md.GameD.client_settings['fullscreen']
            
            # Check if file format is correct
            if self.check_if_correct(attr_dict, ref_dict, enum_strings):
                md.GameD.client_settings = attr_dict
                if md.UInputD.joystick_connected:
                    md.UInputD.joystick_enabled = \
                        attr_dict['joystick_switch']
                else:
                    md.UInputD.joystick_enabled = False
                self.save_to_file(attr_dict)
        
            else:
                # Reset file if it isn't correct
                self.reset_config_file()
    
    @classmethod
    def load_from_file(cls):
        """Loads client configuration from file"""
    
        config_path = md.MAIN_DIR.joinpath('client.config')
    
        if not config_path.exists():
            cls.reset_config_file()
    
        client_config = config_path.open('r')
        json_string = client_config.read()
        client_config.close()
        
        ref_dict = {'server_address': '127.0.0.1',
                    'server_port': 5200,
                    'player_name': 'Unnamed',
                    'player_team': 'team_0',
                    'player_model': 'male',
                    'joystick_switch': True,
                    'fullscreen': False}
        
        enum_strings = {'player_team': ['team_0', 'team_a', 'team_b'],
                        'player_model': ['male', 'female']}
        
        try:
            attr_dict = json.loads(json_string)
        
        except json.JSONDecodeError:
            cls.reset_config_file()
        
        else:
            # Check if file format is correct
            if cls.check_if_correct(attr_dict, ref_dict, enum_strings):
                if attr_dict['fullscreen']:
                    mode = \
                        glfw.get_video_mode(glfw.get_primary_monitor())
                    glfw.set_window_monitor(md.OpenGLD.window,
                                            glfw.get_primary_monitor(),
                                            0, 0,
                                            mode.size.width,
                                            mode.size.height,
                                            glfw.DONT_CARE)
        
                else:
                    md.OpenGLD.window_width = 1280
                    md.OpenGLD.window_height = 720
                    glfw.set_window_monitor(md.OpenGLD.window,
                                            None, 40, 40,
                                            md.OpenGLD.window_width,
                                            md.OpenGLD.window_height,
                                            glfw.DONT_CARE)
                return attr_dict
        
            else:
                # Reset file if it isn't correct
                cls.reset_config_file()
        
            return ref_dict

    @staticmethod
    def check_if_correct(attr_dict, ref_dict, enum_strings):
        """Check if config data is correct"""
    
        if set(list(attr_dict)) == set(list(ref_dict)):
            for attr_name in attr_dict:
                attr = attr_dict[attr_name]
                ref_attr = ref_dict[attr_name]
                if type(attr) == type(ref_attr):
                    if attr_name in enum_strings:
                        if attr in enum_strings[attr_name]:
                            pass
                        
                        else:
                            return False
                else:
                    return False
            
            return True

        return False
    
    @staticmethod
    def save_to_file(attr_dict):
        """Save settings into file"""

        config_path = md.MAIN_DIR.joinpath('client.config')
        
        with config_path.open('w') as client_config:
            
            json_string = json.dumps(attr_dict)
            
            client_config.write(json_string)
            client_config.flush()
    
    @staticmethod
    def reset_config_file():
        """Resets config file to defaults"""
    
        config_path = md.MAIN_DIR.joinpath('client.config')
    
        with config_path.open('w') as client_config:
            attr_dict = {
                'server_address': '127.0.0.1',
                'server_port': 5200,
                'player_name': 'Unnamed',
                'player_team': 'team_0',
                'player_model': 'male',
                'joystick_switch': True,
                'fullscreen': False}
        
            json_string = json.dumps(attr_dict)

            client_config.write(json_string)
            client_config.flush()


# Joystick settings menu gui
class JoystickSettingsGUI(ung.UNG):
    def __init__(self, name='joystick_settings', enabled=True):
        super().__init__(name=name, enabled=enabled)
        
        # Change axis
        self.gui_events['axis_m_x'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.catch_axis,
                 'args': ['axis_m_x']}}
        
        self.gui_events['axis_m_y'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.catch_axis,
                 'args': ['axis_m_y']}}
        
        self.gui_events['axis_l_x'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.catch_axis,
                 'args': ['axis_l_x']}}

        self.gui_events['axis_l_y'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.catch_axis,
                 'args': ['axis_l_y']}}
        
        # Change axis inversion
        self.gui_events['axis_m_inv_x'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.switch_inversion,
                 'args': ['axis_m_inv_x']}}

        self.gui_events['axis_m_inv_y'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.switch_inversion,
                 'args': ['axis_m_inv_y']}}

        self.gui_events['axis_l_inv_x'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.switch_inversion,
                 'args': ['axis_l_inv_x']}}

        self.gui_events['axis_l_inv_y'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.switch_inversion,
                 'args': ['axis_l_inv_y']}}

        # Change button
        self.gui_events['fire_button'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.catch_button,
                 'args': ['fire_button']}}
        
        self.gui_events['select_button'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.catch_button,
                 'args': ['select_button']}}

        # Save settings
        self.gui_events['save_settings'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.save_settings,
                 'args': []}}
        
        # Main menu
        self.gui_events['main_menu'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.main_menu,
                 'args': []}}
    
    def catch_axis(self, args):
        """Catches input from joystick"""
        
        if len(args) != 1:
            return -1
        
        element = self.search_in_elements(args[0])
        js_id = md.UInputD.u_joystick.js_id
        index = -1
        
        if element is not None:
            md.UInputD.key_catch = True
            while md.UInputD.key_catch:
                arr, count = glfw.get_joystick_axes(js_id)
                for i in range(0, count):
                    if not -0.1 < arr[i] < 0.1:
                        index = i
                        md.UInputD.key_catch = False
                        
                glfw.poll_events()
            
            if index != -1:
                element.text.value = str(index)
    
                element.text.gen_characters()
                self.gen_graphic_data()
                md.GameD.active_scene.ung_drawer.reinit()
            
            return 0
        
        return -1

    def switch_inversion(self, args):
        """Switches from inverted to not inverted
        and the other way around"""
        
        if len(args) != 1:
            return -1
        
        axis_inv = self.search_in_elements(args[0])
        
        if axis_inv is not None:
            if axis_inv.text.value == '+':
                axis_inv.text.value = '-'
            
            elif axis_inv.text.value == '-':
                axis_inv.text.value = '+'
            
            axis_inv.text.gen_characters()
            self.gen_graphic_data()
            md.GameD.active_scene.ung_drawer.reinit()
            
            return 0
        
        return -1

    # Save/Load joystick config file
    def load_settings(self, args=None):
        """Load control settings"""

        joystick_frame = self.search_in_elements('joystick_frame')
        
        if joystick_frame is not None:
            for element in joystick_frame.sub_elements.values():
                try:
                    if element.name == 'axis_m_x':
                        element.text.value = \
                            str(md.UInputD.u_joystick.move_st.axis_x)
                    
                    elif element.name == 'axis_m_inv_x':
                        if not md.UInputD.u_joystick.move_st.inv_x:
                            element.text.value = '+'
                        
                        else:
                            element.text.value = '-'
                    
                    elif element.name == 'axis_m_y':
                        element.text.value = \
                            str(md.UInputD.u_joystick.move_st.axis_y)
                    
                    elif element.name == 'axis_m_inv_y':
                        if not md.UInputD.u_joystick.move_st.inv_y:
                            element.text.value = '+'
                        
                        else:
                            element.text.value = '-'
                    
                    elif element.name == 'axis_l_x':
                        element.text.value = \
                            str(md.UInputD.u_joystick.look_st.axis_x)
                    
                    elif element.name == 'axis_l_inv_x':
                        if not md.UInputD.u_joystick.look_st.inv_x:
                            element.text.value = '+'
                        
                        else:
                            element.text.value = '-'
                    
                    elif element.name == 'axis_l_y':
                        element.text.value = \
                            str(md.UInputD.u_joystick.look_st.axis_y)
                    
                    elif element.name == 'axis_l_inv_y':
                        if not md.UInputD.u_joystick.look_st.inv_y:
                            element.text.value = '+'
                        
                        else:
                            element.text.value = '-'
                    
                    elif element.name == 'fire_button':
                        element.text.value = \
                            str(md.UInputD.u_joystick.fire_button)
                    
                    elif element.name == 'select_button':
                        element.text.value = \
                            str(md.UInputD.u_joystick.select_button)
                    
                    element.text.gen_characters()
                
                except AttributeError:
                    return -2
            
            self.gen_graphic_data()
            md.GameD.active_scene.ung_drawer.reinit()
            
            return 0
        
        return -1
    
    def save_settings(self, args=None):
        """Saves control settings"""

        joystick_frame = self.search_in_elements('joystick_frame')
        
        if joystick_frame is not None:
            for element in joystick_frame.sub_elements.values():
                try:
                    if element.name == 'axis_m_x':
                        md.UInputD.u_joystick.move_st.axis_x = \
                            int(element.text.value)
                    
                    elif element.name == 'axis_m_inv_x':
                        if element.text.value == '+':
                            md.UInputD.u_joystick.move_st.inv_x = False
                        
                        elif element.text.value == '-':
                            md.UInputD.u_joystick.move_st.inv_x = True
                    
                    elif element.name == 'axis_m_y':
                        md.UInputD.u_joystick.move_st.axis_y = \
                            int(element.text.value)
                    
                    elif element.name == 'axis_m_inv_y':
                        if element.text.value == '+':
                            md.UInputD.u_joystick.move_st.inv_y = False
                        
                        elif element.text.value == '-':
                            md.UInputD.u_joystick.move_st.inv_y = True
                    
                    elif element.name == 'axis_l_x':
                        md.UInputD.u_joystick.look_st.axis_x = \
                            int(element.text.value)
                    
                    elif element.name == 'axis_l_inv_x':
                        if element.text.value == '+':
                            md.UInputD.u_joystick.look_st.inv_x = False
                        
                        elif element.text.value == '-':
                            md.UInputD.u_joystick.look_st.inv_x = True
                    
                    elif element.name == 'axis_l_y':
                        md.UInputD.u_joystick.look_st.axis_y = \
                            int(element.text.value)
                    
                    elif element.name == 'axis_l_inv_y':
                        if element.text.value == '+':
                            md.UInputD.u_joystick.look_st.inv_y = False
                        
                        elif element.text.value == '-':
                            md.UInputD.u_joystick.look_st.inv_y = True
                    
                    elif element.name == 'fire_button':
                        md.UInputD.u_joystick.fire_button = \
                            int(element.text.value)
                    
                    elif element.name == 'select_button':
                        md.UInputD.u_joystick.select_button = \
                            int(element.text.value)
                
                except AttributeError:
                    return -2
        
            md.UInputD.u_joystick.save_to_file()
    
            return 0
        
        return -1
    
    def catch_button(self, args):
        """Catches input from joystick"""
    
        if len(args) != 1:
            return -1
        
        element = self.search_in_elements(args[0])
        js_id = md.UInputD.u_joystick.js_id
        index = -1
        
        if element is not None:
            md.UInputD.key_catch = True
            while md.UInputD.key_catch:
                arr, count = glfw.get_joystick_buttons(js_id)
                for i in range(0, count):
                    if arr[i] > 0:
                        index = i
                        md.UInputD.key_catch = False
            
                glfw.poll_events()
        
            if index != -1:
                element.text.value = str(index)
            
                element.text.gen_characters()
                self.gen_graphic_data()
                md.GameD.active_scene.ung_drawer.reinit()
        
            return 0
        
        return -1
    
    @staticmethod
    def main_menu(args=None):
        """Change back to main menu"""
        
        md.GameD.active_scene.guis['joystick_settings'].\
            change_enabled_state(False)
        
        md.GameD.active_scene.guis['main_menu'].\
            change_enabled_state(True)
        
        md.GameD.active_scene.active_gui_name = 'main_menu'


class DisconnectedGUI(ung.UNG):
    def __init__(self, name='disconnected', enabled=True):
        super().__init__(name=name, enabled=enabled)
        
        # Main menu
        self.gui_events['main_menu'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.main_menu,
                 'args': []}}
    
    @staticmethod
    def main_menu(args=None):
        """Change back to main menu"""
        
        md.GameD.active_scene.guis['disconnected'].\
            change_enabled_state(False)
        
        md.GameD.active_scene.guis['main_menu'].\
            change_enabled_state(True)
        
        md.GameD.active_scene.active_gui_name = 'main_menu'
    
    def change_text(self, error_code):
        """Change error text accordingly"""
        
        error_textfield = self.search_in_elements('error_textfield')
        
        if error_textfield is not None:
            if error_code == -1:
                error_textfield.text.value = \
                    'DISCONNECTED: TIMEOUT'
            else:
                error_textfield.text.value = \
                    'DISCONNECTED: CONNECTION ERROR'
    
            error_textfield.text.gen_characters()
            self.reveal_element(error_textfield)
            
            return 0
            
        return -1


class GameGUI(ung.UNG):
    def __init__(self, name='game', enabled=True):
        super().__init__(name=name, enabled=enabled)
        
        self.player_elements = {}
        
        # Quit game
        self.gui_events['exit'] = {
            glfw.MOUSE_BUTTON_LEFT:
                {'func': self.exit_game,
                 'args': []}}

    @staticmethod
    def exit_game(args=None):
        """Exits game to the main menu"""
        
        md.GameD.active_scene.client.close()
        md.GameD.active_scene.change_control_state(False)
        ug.UnnamedGame.change_scene(sc.MainMenuScene('game_select'))
        
        md.GameD.active_scene.guis['game_select'].load_settings()
        
    def refresh_stats(self):
        """Refreshes stats as scores and health and players"""
        
        # Players
        if len(md.GameD.player_score) != len(self.player_elements):
            players_frame = self.search_in_elements('players_frame')
            
            if players_frame is not None:
                players_frame.sub_elements = {}
                self.player_elements = {}
                for name, score in md.GameD.player_score:
                    team = md.GameD.players[name].team
                    
                    if team == 'team_a':
                        if name == md.GameD.active_player.name:
                            text_color = [209, 57, 57, 255]
                        else:
                            text_color = [189, 37, 37, 255]
                    
                    elif team == 'team_b':
                        if name == md.GameD.active_player.name:
                            text_color = [61, 75, 185, 255]
                        else:
                            text_color = [41, 55, 165, 255]
                        
                    else:
                        if name == md.GameD.active_player.name:
                            text_color = [50, 50, 50, 255]
                        else:
                            text_color = [240, 240, 240, 255]
                    
                    name_el = \
                        ue.UNGTextfield(name=name + '_text',
                                        position=[0.0, 0.0],
                                        size=[0.15, 0.05],
                                        color=[170, 150, 230, 255])
    
                    score_el = \
                        ue.UNGTextfield(name=name + '_score',
                                        position=[0.15, 0.0],
                                        size=[0.025, 0.05],
                                        color=[170, 150, 230, 255])
    
                    score_el.text = ue.UNGText(container=score_el,
                                               value=str(score),
                                               font_size=0.035,
                                               color=text_color.copy(),
                                               align_h='center',
                                               align_v='center')
                    
                    name_el.text = ue.UNGText(container=name_el,
                                              value=name,
                                              font_size=0.035,
                                              color=text_color.copy(),
                                              align_h='left',
                                              align_v='center')
                    
                    players_frame.sub_elements[name_el.name] = name_el
                    
                    players_frame.sub_elements[score_el.name] = score_el
                    
                    name_el.text.gen_characters()
    
                    score_el.text.gen_characters()
                    
                    self.player_elements[name] = [name_el, score_el]
        
            else:
                return -1
            
        i = 0
        for name, score in md.GameD.player_score:
            name_el = self.player_elements[name][0]
            score_el = self.player_elements[name][1]
            
            score_el.text.value = str(score)
            score_el.text.gen_characters()
            name_el.text.gen_characters()

            score_el.position[1] = i / 20
            name_el.position[1] = i / 20
            
            i += 1
        
        # Match time
        time_text = self.search_in_elements('time_text')

        if time_text is not None:
            time_text.text.value = \
                '{0:02d}:{1:02d}'.format(*md.GameD.current_match_time)
    
            time_text.text.gen_characters()
        
        else:
            return -2
            
        # Team score
        score_text = self.search_in_elements('score_text')
        
        if score_text is not None:
            if md.GameD.game_mode != 'dm':
                    if md.GameD.active_player.team == 'team_a':
                        self_team_score = \
                            md.GameD.team_score['team_a']
            
                        other_team_score = \
                            md.GameD.team_score['team_b']
                     
                    else:
                        self_team_score = \
                            md.GameD.team_score['team_b']
            
                        other_team_score = \
                            md.GameD.team_score['team_a']
            
                    score_text.text.value = \
                        str(self_team_score) + ' : ' + \
                        str(other_team_score)
            
                    score_text.text.gen_characters()
            
            else:
                score = 0
                for player_name, player_score in md.GameD.player_score:
                    if player_name == md.GameD.active_player.name:
                        score = player_score
                
                score_text.text.value = str(score)
        
                score_text.text.gen_characters()
            
        else:
            return -3
        
        # Health
        health_bar = self.search_in_elements('health_bar')

        if health_bar is not None:
            health_bar.size[0] = (0.4 * md.GameD.active_player.health /
                                  ge.Player.MAX_HEALTH)
        
        else:
            return -4
        
        self.gen_graphic_data()
        md.GameD.active_scene.ung_drawer.reinit()

    def show_match_end(self, message):
        """Shows match end message"""
        
        match_end_text = self.search_in_elements('match_end_text')
        
        if match_end_text is not None:
            match_end_text.text.value = message
    
            match_end_text.text.gen_characters()
            self.reveal_element(match_end_text)
            
            return 0
        
        return -1
    
    def hide_match_end(self):
        """Hides match end message"""
        
        match_end_text = self.search_in_elements('match_end_text')

        if match_end_text is not None:
            self.hide_element(match_end_text)
            
            return 0
        
        return -1
