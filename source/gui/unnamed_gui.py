"""GUI class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import ast
import xml.etree.ElementTree as elementTree
import inspect
import re

# Packages
#   Data classes
import source.data_classes.main_data as md
#   GUI
from source.gui.ung_elements import UNGFrame, UNGButton, UNGTextfield,\
    UNGInputTextfield, UNGCharacter, UNGText, character_table


class UNG:
    def __init__(self, name='', enabled=True):
        
        # GUI options
        self.name = name
        self.enabled = enabled  # If enabled for drawing/interaction
        
        # GUI element data
        self.root_elements = {}  # Elements that doesn't have parents
        self.all_elements = {}   # All existing element
        self.graphic_data = {}   # Graphic data of elements for drawing
        self.element_areas = {}  # Element's clickable areas on screen
        self.gui_events = {}     # Events assigned to elements
        
        # Element selection
        self.selected_element = None  # Element selected by mouse hover
        
        # Text input character selection
        self.selected_character = {'text': None, 'index': 0}
        
        # Types of UNG elements
        self.element_types = {'UNGFrame': UNGFrame,
                              'UNGButton': UNGButton,
                              'UNGTextfield': UNGTextfield,
                              'UNGInputTextfield': UNGInputTextfield,
                              'UNGCharacter': UNGCharacter}
    
    def init(self):
        """UNG initialization"""
        
        gui_xml_path = md.MAIN_DIR.joinpath(
            'resources', 'gui_xml', 'ung_' + self.name + '.xml')
        
        if gui_xml_path.exists():
            self.read_from_xml(gui_xml_path)
            return 0
        
        else:
            return -1
    
    # Generating data
    def read_from_xml(self, file_path):
        """Reads gui element parameters from xml file"""
        
        gui_tree = elementTree.parse(file_path)
        gui_root = gui_tree.getroot()
        
        # Iterate through the root's branches
        for branch in gui_root:
            element = self.read_from_xml_rec(None, branch)
            self.root_elements[element.name] = element
        
        # Generate graphic data from collected elements
        self.gen_graphic_data()
    
    def read_from_xml_rec(self, parent, gui_root):
        """Recursion part of create_from_xml"""
        
        parameters = {'name': gui_root.attrib['name']}

        try:
            sub = None
            for gui_child in gui_root:
                if gui_child.tag == 'position':
                    t_position = ast.literal_eval(gui_child.text)
                    
                    # Checking if child position is relative to parent
                    if 'relative' in gui_child.attrib and not \
                            ast.literal_eval(
                                gui_child.attrib['relative']):
                        parameters['position'] = t_position
                    else:
                        if parent is not None:
                            parameters['position'] = \
                                [t_position[0] * parent.size[0] +
                                 parent.position[0],
                                 t_position[1] * parent.size[1] +
                                 parent.position[1]]
                        else:
                            parameters['position'] = t_position
                
                elif gui_child.tag == 'size':
                    t_size = ast.literal_eval(gui_child.text)
    
                    # Checking if child's size is relative to parent's
                    if 'relative' in gui_child.attrib and not \
                            ast.literal_eval(
                                gui_child.attrib['relative']):
                        parameters['size'] = t_size
                    else:
                        if parent is not None:
                            parameters['size'] = \
                                [t_size[0] * parent.size[0],
                                 t_size[1] * parent.size[1]]
                        
                        else:
                            parameters['size'] = t_size
                
                # Checking text attributes
                elif gui_child.tag == 'text':
                    if gui_child.text is not None:
                        gui_child.text = \
                            re.sub('\n *', ' ', gui_child.text).strip()
                    else:
                        gui_child.text = ''
                    
                    text_pars = {'value': gui_child.text}
                    
                    if 'font_size' in gui_child.attrib:
                        text_pars['font_size'] = \
                            ast.literal_eval(
                                gui_child.attrib['font_size'])
        
                    if 'align_h' in gui_child.attrib:
                        text_pars['align_h'] = \
                            gui_child.attrib['align_h']
        
                    if 'align_v' in gui_child.attrib:
                        text_pars['align_v'] = \
                            gui_child.attrib['align_v']
                        
                    if 'color' in gui_child.attrib:
                        text_pars['color'] = \
                            ast.literal_eval(gui_child.attrib['color'])
                    
                    parameters['text'] = UNGText(**text_pars)
                
                # Checking if element has sub elements
                elif gui_child.tag == 'SUB':
                    sub = gui_child
                
                # Other parameters without attributes
                else:
                    try:
                        parameters[gui_child.tag] = \
                            ast.literal_eval(gui_child.text)
                    except(ValueError, SyntaxError):
                        parameters[gui_child.tag] = gui_child.text
            
            element = self.create_element(gui_root.tag, parameters)
            
            self.all_elements[element.name] = element
            
            # Iterate through sub elements
            if sub is not None:
                for sub_child in sub:
                    sub_elem = \
                        self.read_from_xml_rec(element, sub_child)
                    
                    element.sub_elements[sub_elem.name] = sub_elem
            
            return element
        
        except (ValueError, SyntaxError):
            return None
    
    def create_element(self, class_name, parameters):
        """Create one element with a class name and it's parameters"""
        
        # Get class reference from class name
        cls = self.element_types[class_name]
        
        # Get arguments of class
        keys = inspect.getfullargspec(cls).args[1:]
        selected_parameters = {}
        
        for key in parameters:
            if key in keys:
                selected_parameters[key] = parameters[key]
        
        element = cls(**selected_parameters)
        
        # If element has text, generate it
        if element.text is not None:
            element.text.container = element
            element.text.gui = self
            element.text.gen_characters()
        
        return element
    
    # Generating data for drawing
    def gen_graphic_data(self):
        """Generates graphic data for drawing"""

        self.graphic_data = {}
        self.element_areas = {}
        
        if self.enabled:
            # Iterate through root elements
            for element_name in self.root_elements:
                element = self.root_elements[element_name]
                
                # If element is visible, start recursive traversal
                if element.visible:
                    self.gen_graphic_data_rec(element)
    
            self.graphic_data['cursor'] = {
                'position': [0.0, 0.0],
                'size': [0.0, 0.0],
                'color': [255, 255, 255, 255],
                'tex_name': 'share_tech_mono.png',
                'tex_pos': [0.0, 0.0],
                'tex_size': [0.0, 0.0]}
    
    def gen_graphic_data_rec(self, self_element):
        """Recursion part of gen_graphic_data"""
        
        # Get graphic data of element
        self.graphic_data[self_element.name] = \
            {'position': self_element.position.copy(),
             'size': self_element.size.copy(),
             'tex_pos': self_element.tex_pos.copy(),
             'tex_size': self_element.tex_size.copy(),
             'color': self_element.color.copy(),
             'tex_name': self_element.tex_name}
        
        if self_element.TYPE != 'UNGCharacter' and \
                self_element.selectable:
            self.element_areas[self_element] = \
                {'position': self_element.position.copy(),
                 'size': self_element.size.copy()}
        
        # Iterate through sub elements
        for element_name in self_element.sub_elements:
            sub_element = self_element.sub_elements[element_name]
            
            # If sub element is visible, continue recursive traversal
            if sub_element.visible:
                self.gen_graphic_data_rec(sub_element)
        
        # If element has text, generate the graphic data of characters
        if self_element.text is not None:
            for character in self_element.text.characters:
                self.gen_graphic_data_rec(
                    self_element.text.characters[character])
    
    def update_graphic_data(self, self_element):
        """Updates graphic data of one element"""
        
        self.graphic_data[self_element.name] = \
            {'position': self_element.position.copy(),
             'size': self_element.size.copy(),
             'tex_pos': self_element.tex_pos.copy(),
             'tex_size': self_element.tex_size.copy(),
             'color': self_element.color.copy(),
             'tex_name': self_element.tex_name}
        
        if self_element.TYPE != 'UNGCharacter':
            self.element_areas[self_element] = \
                {'position': self_element.position.copy(),
                 'size': self_element.size.copy()}
    
    # Search
    def search_in_elements(self, element_name):
        """Returns the element with element_name"""
        
        if element_name in self.all_elements:
            return self.all_elements[element_name]
        
        return None
    
    # Interaction with elements
    def click_detect(self):
        """Click detection"""
        
        self.selected_character = {'text': None, 'index': 0}
        self.update_cursor()
        
        if self.selected_element is not None:
            position = [md.UInputD.last_x / md.OpenGLD.window_width,
                        md.UInputD.last_y / md.OpenGLD.window_height]
            
            if self.selected_element.TYPE == 'UNGInputTextfield':
                self.character_click_detect(position)
                
        return self.selected_element
    
    def mouse_hover_detect(self):
        """Mouse hover detection"""
        
        position = [md.UInputD.last_x / md.OpenGLD.window_width,
                    md.UInputD.last_y / md.OpenGLD.window_height]
        
        # "Unlit" previous selected element
        if self.selected_element is not None:
            self.selected_element.color[0] -= 20
            self.selected_element.color[1] -= 20
            self.selected_element.color[2] -= 20
            
            self.update_graphic_data(self.selected_element)
            md.GameD.active_scene.ung_drawer.update_element(
                self.name, self.selected_element.name)
        
        # Search for selected element
        self.selected_element = None
        for element in list(self.element_areas):
            area = self.element_areas[element]
            if position[0] <= area['position'][0] + area['size'][
                0] \
                    and not position[0] < area['position'][0]:
                if position[1] <= area['position'][1] + \
                        area['size'][1] \
                        and not position[1] < area['position'][1]:
                    self.selected_element = element

        # "Lit" selected element
        if self.selected_element is not None:
            self.selected_element.color[0] += 20
            self.selected_element.color[1] += 20
            self.selected_element.color[2] += 20
            
            self.update_graphic_data(self.selected_element)
            md.GameD.active_scene.ung_drawer.update_element(
                self.name, self.selected_element.name)
    
    def process_gui_event(self, event, element):
        """Processes events attached to elements"""
        
        # Checks if there are any event attached to current element
        if element.name in self.gui_events:
            self.gui_events[element.name][event]['func'](
                self.gui_events[element.name][event]['args'])
    
    def change_enabled_state(self, is_enabled):
        """Enables or disables gui"""
        
        self.enabled = is_enabled
        if self.selected_element is not None:
            self.selected_element.color[0] -= 20
            self.selected_element.color[1] -= 20
            self.selected_element.color[2] -= 20
        self.selected_element = None
        self.selected_character = {'text': None, 'index': 0}
        self.gen_graphic_data()
        md.GameD.active_scene.ung_drawer.reinit()
        
        # Reruns hover detection after state change
        if is_enabled:
            self.mouse_hover_detect()

    def reveal_element(self, element):
        """Reveals an element"""
    
        element.visible = True
        
        self.gen_graphic_data()
        md.GameD.active_scene.ung_drawer.reinit()
    
    def hide_element(self, element):
        """Hides an element"""
        
        element.visible = False
        
        # If hidden element was the selected, clear selected_element
        if self.selected_element == element:
            self.selected_element = None
        
        self.gen_graphic_data()
        md.GameD.active_scene.ung_drawer.reinit()
    
    # Character input
    def character_click_detect(self, position):
        """Character click detection"""
        
        selected_char = None

        for char_name in self.selected_element.text.characters:
            char = self.selected_element.text.characters[char_name]
            if char.visible:
                if position[0] <= char.position[0] + char.size[0] \
                        and not position[0] < char.position[0]:
                    if position[1] <= char.position[1] + char.size[
                        1] \
                            and not position[1] < char.position[1]:
                        selected_char = char

        if selected_char is not None:
            self.selected_character['text'] = self.selected_element.text
            
            index = \
                list(self.selected_element.text.characters).index(
                    selected_char.name)
            
            # Position selection to left or right of the character
            l_dist = position[0] - selected_char.position[0]
    
            if selected_char.size[0] / 2.0 < l_dist:
                index += 1
            
            self.selected_character['index'] = index

        else:
            # If clicked on no characters
            # move cursor to the end of the text
            self.selected_character['text'] = self.selected_element.text
            self.selected_character['index'] = \
                len(self.selected_element.text.characters)

        self.update_cursor()
    
    def input_char(self, char):
        """Inputs one character to cursor position"""
        
        sel_char = self.selected_character
        
        # If there is a selected character
        if sel_char['text'] is not None:
            # If hit character is in the whitelist of textfield
            if re.fullmatch(sel_char['text'].container.whitelist, char):
                text = sel_char['text']
                index = sel_char['index']
                text.value = \
                    text.value[0:index] + char + \
                    text.value[index:]
                
                # Move cursor to the right after character input
                sel_char['index'] += 1
                
                # Update text and draw data
                text.gen_characters()
                self.gen_graphic_data()
                md.GameD.active_scene.ung_drawer.reinit()
                self.update_cursor()
        
    def delete_char(self, delete_mode):
        """Deletes one character with delete mode del or backspace"""
        
        # If there is a selected character
        if self.selected_character['text'] is not None:
            text = self.selected_character['text']
            index = self.selected_character['index']
            
            # Delete
            if delete_mode == 'd':
                if index != len(text.value):
                    text.value = \
                        text.value[0:index] + \
                        text.value[index + 1:]
            
            # Backspace
            elif delete_mode == 'b':
                if index != 0:
                    text.value = \
                        text.value[0:index - 1] + \
                        text.value[index:]
                    index -= 1
            
            self.selected_character['index'] = index

            # Update text and draw data
            text.gen_characters()
            self.gen_graphic_data()
            md.GameD.active_scene.ung_drawer.reinit()
            self.update_cursor()
    
    def move_cursor(self, offset):
        """Moves the cursor with offset"""
        
        # If there is a selected character
        if self.selected_character['text'] is not None:
            index = self.selected_character['index']
            max_index = len(self.selected_character['text'].characters)
            
            if 0 <= offset + index <= max_index:
                self.selected_character['index'] += offset
            
            self.update_cursor()
    
    def update_cursor(self):
        """Updates cursor position in text"""
        
        input_field = self.selected_character['text']
        if input_field is not None:
            keys = list(input_field.characters)
            index = self.selected_character['index']
            
            if len(keys) > 0:
                if index < len(keys):
                    sel_char = input_field.characters[keys[index]]
                    cursor = {'position': sel_char.position,
                              'size': [sel_char.size[0] / 5.0,
                                       sel_char.size[1]],
                              'color': [255, 255, 255, 255],
                              'tex_name': 'share_tech_mono.png'}
                else:
                    sel_char = input_field.characters[keys[-1]]
                    cursor = {'position': [sel_char.position[0] +
                                           sel_char.size[0],
                                           sel_char.position[1]],
                              'size': [sel_char.size[0] / 5.0,
                                       sel_char.size[1]],
                              'color': [255, 255, 255, 255],
                              'tex_name': 'share_tech_mono.png'}
                    
            else:
                cont_pos = input_field.container.position
                cont_size = input_field.container.size
                
                char_size = [input_field.font_size * 9 / 16 * 0.6,
                             input_field.font_size]
                
                cursor = {'position': [0.0, 0.0],
                          'size': [char_size[0] / 5.0,
                                   char_size[1]],
                          'color': [255, 255, 255, 255],
                          'tex_name': 'share_tech_mono.png'}
                
                # Horizontal alignment
                if input_field.align_h == 'left':
                    cursor['position'][0] = \
                        cont_pos[0]
    
                elif input_field.align_h == 'right':
                    cursor['position'][0] = \
                        cont_pos[0] + cont_size[0]
    
                elif input_field.align_h == 'center':
                    cursor['position'][0] = \
                        cont_pos[0] + \
                        (cont_size[0] / 2)
    
                # Vertical alignment
                if input_field.align_v == 'top':
                    cursor['position'][1] = \
                        cont_pos[1]
    
                elif input_field.align_v == 'bottom':
                    cursor['position'][1] = \
                        cont_pos[1] + cont_size[1] - char_size[1]
    
                elif input_field.align_v == 'center':
                    cursor['position'][1] = \
                        cont_pos[1] + \
                        (cont_size[1] - char_size[1]) / 2
            
            cursor_data = character_table['cur']
            
            # Doesn't allow cursor to move outside of container
            if cursor['position'][0] >= input_field.container.position[0] + \
                    input_field.container.size[0]:
                cursor['position'][0] -= cursor['size'][0]
            
            cursor['tex_pos'] = [(cursor_data[1] + 0.2) / 16.0,
                                 cursor_data[0] / 16.0]
            cursor['tex_size'] = [(cursor_data[1] + 0.8) / 16.0,
                                  (cursor_data[0] + 1.0) / 16.0]
        
            self.graphic_data['cursor'] = cursor
        
        else:
            self.graphic_data['cursor']['size'] = [0.0, 0.0]

        md.GameD.active_scene.ung_drawer.update_element(self.name, 'cursor')
