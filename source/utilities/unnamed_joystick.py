"""Joystick handling"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import json
import math

from enum import Enum

# Packages
#   Data classes
import source.data_classes.main_data as md


class UnnamedJoystick:
    # Dead zones
    M_DZONE = 0.05
    L_DZONE = 0.5
    
    def __init__(self, js_id=0, axes_m=None, axes_l=None,
                 move_inv=None, look_inv=None,
                 fire_button=0, select_button=1):
        
        # Set defaults
        if axes_m is None:
            axes_m = [0, 1]
            
        if axes_l is None:
            axes_l = [2, 3]
            
        if move_inv is None:
            move_inv = [False, False]
            
        if look_inv is None:
            look_inv = [False, False]
        
        # Joystick id
        self.js_id = js_id
        
        # Axes
        self.move_st = \
            UnnamedStick(axes_m[0], axes_m[1], move_inv[0], move_inv[1])
        self.look_st = \
            UnnamedStick(axes_l[0], axes_l[1], look_inv[0], look_inv[1])
        
        # Buttons
        self.fire_button = fire_button
        self.select_button = select_button
        
        self.button_state = {'fire_button': 0,
                             'select_button': 0}
    
    def check_move_x(self, value_arr):
        """Checks move stick's x axis"""
        
        x_value = value_arr[self.move_st.axis_x]
        
        west = 0
        east = 0
        if self.move_st.inv_x:
            if x_value > UnnamedJoystick.M_DZONE:
                west = -1
            
            elif x_value < -UnnamedJoystick.M_DZONE:
                east = 1
        
        else:
            if x_value < -UnnamedJoystick.M_DZONE:
                west = -1
    
            elif x_value > UnnamedJoystick.M_DZONE:
                east = 1
        
        return west, east
    
    def check_move_y(self, value_arr):
        """Checks move stick's y axis"""

        y_value = value_arr[self.move_st.axis_y]
        
        north = 0
        south = 0
        if self.move_st.inv_y:
            if y_value > UnnamedJoystick.M_DZONE:
                north = -1
            
            elif y_value < -UnnamedJoystick.M_DZONE:
                south = 1
        
        else:
            if y_value < -UnnamedJoystick.M_DZONE:
                north = -1
    
            elif y_value > UnnamedJoystick.M_DZONE:
                south = 1
        
        return north, south
    
    def get_look_angle(self, value_arr):
        """Returns the look stick's angle"""
        
        x_value = value_arr[self.look_st.axis_x]
        y_value = value_arr[self.look_st.axis_y]
        
        angle = None
        
        if not -UnnamedJoystick.L_DZONE < \
            x_value < UnnamedJoystick.L_DZONE or \
                not -UnnamedJoystick.L_DZONE < \
                y_value < UnnamedJoystick.L_DZONE:
            
            if x_value == 0:
                x_value = 0.001
            
            a_x = x_value
            a_y = y_value
            
            if self.look_st.inv_x:
                a_x = -a_x
            
            if self.look_st.inv_y:
                a_y = -a_y
            
            angle = math.atan2(a_y, a_x)
        
        return angle
    
    @classmethod
    def load_from_file(cls):
        """Loads joystick configuration from file"""
        
        config_path = md.MAIN_DIR.joinpath('joystick.config')
        
        if not config_path.exists():
            cls.reset_config_file()
        
        joystick_config = config_path.open('r')
        json_string = joystick_config.read()
        joystick_config.close()

        ref_dict = {'axes_m': [0, 0],
                    'move_inv': [True, True],
                    'axes_l': [0, 0],
                    'look_inv': [True, True],
                    'fire_button': 0, 'select_button': 0}
        
        try:
            attr_dict = json.loads(json_string)
        
        except json.JSONDecodeError:
            cls.reset_config_file()

        else:
            # Check if file format is correct
            if cls.check_if_correct(attr_dict, ref_dict):
                return UnnamedJoystick(**attr_dict)
                
            else:
                # Reset file if it isn't correct
                cls.reset_config_file()
                
                return UnnamedJoystick()
    
    @staticmethod
    def check_if_correct(attr_dict, ref_dict):
        """Check if config data is correct"""
        
        if set(list(attr_dict)) == set(list(ref_dict)):
            for attr_name in attr_dict:
                attr = attr_dict[attr_name]
                ref_attr = ref_dict[attr_name]
                if type(attr) == type(ref_attr):
                    
                    # Check if it's a list and if same length
                    if type(ref_attr) == list:
                        if len(ref_attr) == len(attr):
                            # Check list elements' type
                            for index in range(0, len(ref_attr)):
                                if type(attr[index]) == \
                                        type(ref_attr[index]):
                                    pass
                                    
                                else:
                                    return False

                        else:
                            return False
                    
                else:
                    return False
            
            return True
            
        return False
    
    def save_to_file(self):
        """Save settings into file"""

        config_path = md.MAIN_DIR.joinpath('joystick.config')

        with config_path.open('w') as joystick_config:
            attr_dict = {
                'axes_m': [self.move_st.axis_x, self.move_st.axis_y],
        
                'move_inv': [self.move_st.inv_x, self.move_st.inv_y],
        
                'axes_l': [self.look_st.axis_x, self.look_st.axis_y],
        
                'look_inv': [self.look_st.inv_x, self.look_st.inv_y],
        
                'fire_button': self.fire_button,
                'select_button': self.select_button}
            
            json_string = json.dumps(attr_dict)
        
            joystick_config.write(json_string)
            joystick_config.flush()
    
    @staticmethod
    def reset_config_file():
        """Resets config file to defaults"""

        config_path = md.MAIN_DIR.joinpath('joystick.config')
        
        with config_path.open('w') as joystick_config:
            attr_dict = {
                'axes_m': [0, 1],
            
                'move_inv': [False, False],
            
                'axes_l': [2, 3],
            
                'look_inv': [False, False],
            
                'fire_button': 0,
                'select_button': 1}
        
            json_string = json.dumps(attr_dict)
        
            joystick_config.write(json_string)
            joystick_config.flush()
    
    def button_event(self, button, new_state):
        """Returns button state event"""
        
        if self.button_state[button] == 0 and new_state == 1:
            self.button_state[button] = 1
            return JoystickEvent.PRESSED
        
        elif self.button_state[button] == 1 and new_state == 0:
            self.button_state[button] = 0
            return JoystickEvent.RELEASED
        
        else:
            return JoystickEvent.NO_INTERACTION


class UnnamedStick:
    def __init__(self, axis_x, axis_y, inv_x, inv_y):
        self.axis_x = axis_x
        self.axis_y = axis_y
        self.inv_x = inv_x
        self.inv_y = inv_y


class JoystickEvent(Enum):
    PRESSED = 0
    RELEASED = 1
    NO_INTERACTION = 2
