"""Utilities"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import json
import pathlib

from os import walk
from OpenGL.GL import *

# External libraries
import numpy

# Packages
#   Data classes
import source.data_classes.game_element as ge
import source.data_classes.game_object as go
import source.data_classes.main_data as md


class DynamicLoader:
    mesh_array = []
    index_array = []
    
    @classmethod
    def load_elements(cls):
        md.GameD.dynamic_elements = {}

        dynamic_object_path = md.MAIN_DIR.joinpath(
            'resources', 'objects', 'dynamic')
        
        element_id = 0
        for (dir_path, dir_names, file_names) \
                in walk(dynamic_object_path):
            for file_name in file_names:
                object_path = pathlib.Path(dir_path, file_name)
                
                object_name = file_name.split('.')[0]
                
                game_object, success = \
                    go.GameObject.load_object(object_path, 'dynamic')
                
                if success != 0:
                    return -1
                
                dynamic_element = \
                    ge.DynamicElement(element_name=object_name,
                                      element_id=element_id,
                                      game_object=game_object,
                                      transformations={})

                md.GameD.dynamic_elements[object_name] = \
                    dynamic_element
    
                element_id += 1
        
        return 0

    @classmethod
    def generate_arrays(cls):
        cls.mesh_array = []
        cls.index_array = []
        for element_name in md.GameD.dynamic_elements:
            element = md.GameD.dynamic_elements[element_name]
            mesh_data, index_data = element.game_object.model.get_mesh()
            cls.mesh_array.extend(mesh_data)
            cls.index_array.extend(index_data)
    
    @classmethod
    def get_arrays(cls):
        
        return numpy.array(cls.mesh_array, ctypes.c_float), \
               numpy.array(cls.index_array, ctypes.c_uint32)


class StaticLoader:
    map_name = None
    mesh_array = None
    index_array = None

    @classmethod
    def load_elements(cls):
        """Load static elements from file"""
        
        go.reset()
        
        md.GameD.static_elements = {}

        map_path = md.MAIN_DIR.joinpath(
            'resources', 'maps', cls.map_name + '.map')
        
        static_object_path = md.MAIN_DIR.joinpath(
            'resources', 'objects', 'static')
        
        if not map_path.exists():
            print('No map file named ' + cls.map_name + '.map found!')
            return -1
        
        with map_path.open('r') as map_file:
            
            try:
                map_dict = json.loads(map_file.read())
            
            except json.decoder.JSONDecodeError:
                return -2
            
            element_id = 0
            for element_path in map_dict:
                object_path = \
                    static_object_path.joinpath(element_path + '.go')
                
                object_name = object_path.stem
                
                if md.GameD.game_mode == 'dm':
                    if object_name.startswith('flag'):
                        continue
                    elif object_name.startswith(
                            'team_a') or \
                            object_name.startswith(
                                'team_b'):
                        continue

                elif md.GameD.game_mode == 'ctf':
                    if object_name.startswith('team_0'):
                        continue

                elif md.GameD.game_mode == 'tdm':
                    if object_name.startswith('flag'):
                        continue
                    elif object_name.startswith('team_0'):
                        continue
                
                game_object, success = \
                    go.GameObject.load_object(object_path, 'static')
                
                if success != 0:
                    return -3

                tr_list = []
                for item in map_dict[element_path]:
                    try:
                        trs = Transformations3(translation=item[0],
                                               rotation=item[1],
                                               scale=item[2])
                    except IndexError:
                        return -5
                        
                    tr_list.append(trs)
                
                static_element = \
                    ge.StaticElement(element_name=object_name,
                                     element_id=element_id,
                                     game_object=game_object,
                                     transformations=tr_list)

                md.GameD.static_elements[object_name] = \
                    static_element
                
                element_id += 1
                
            if md.GameD.game_mode == 'dm':
                if 'team_0_marker' not in \
                        md.GameD.static_elements:
                    return -4
            
            elif md.GameD.game_mode == 'tdm':
                if 'team_a_marker' not in \
                       md.GameD.static_elements:
                    return -4
                
                elif 'team_b_marker' not in \
                        md.GameD.static_elements:
                    return -4
            
            elif md.GameD.game_mode == 'ctf':
                if 'team_a_marker' not in \
                       md.GameD.static_elements:
                    return -4

                elif 'team_b_marker' not in \
                         md.GameD.static_elements:
                    return -4
                
                elif 'flag_a_marker' not in \
                       md.GameD.static_elements:
                    return -4

                elif 'flag_b_marker' not in \
                         md.GameD.static_elements:
                    return -4
                
            return 0
    
    @classmethod
    def generate_arrays(cls):
        """Generate lists by merging multiple meshes' lists"""
        
        cls.mesh_array = []
        cls.index_array = []
        for element_name in md.GameD.static_elements:
            element = md.GameD.static_elements[element_name]
            mesh_data, index_data = element.game_object.model.get_mesh()
            cls.mesh_array.extend(mesh_data)
            cls.index_array.extend(index_data)

    @classmethod
    def get_arrays(cls):
        """Returns mesh and index arrays"""
        
        if cls.mesh_array == [] or cls.index_array == []:
            return None, None
    
        else:
            return numpy.array(cls.mesh_array, ctypes.c_float), \
                   numpy.array(cls.index_array, ctypes.c_uint32)


class Vec3(ctypes.Structure):
    _fields_ = [('x', ctypes.c_float),
                ('y', ctypes.c_float),
                ('z', ctypes.c_float)]
    
    def __init__(self, parameters=None):
        if parameters is None:
            parameters = [0.0, 0.0, 0.0]
        t_x = parameters[0]
        t_y = parameters[1]
        t_z = parameters[2]
        super(Vec3, self).__init__(t_x, t_y, t_z)
    
    def to_list(self):
        """Converts Vec3 to list"""
        
        return [self.x, self.y, self.z]


class Vec4(ctypes.Structure):
    _fields_ = [('x', ctypes.c_float),
                ('y', ctypes.c_float),
                ('z', ctypes.c_float),
                ('w', ctypes.c_float)]
    
    def __init__(self, parameters=None):
        if parameters is None:
            parameters = [0.0, 0.0, 0.0, 0.0]
        
        t_x = parameters[0]
        t_y = parameters[1]
        t_z = parameters[2]
        t_w = parameters[3]
        super(Vec4, self).__init__(t_x, t_y, t_z, t_w)
    
    def to_list(self):
        """Converts Vec4 to list"""
        
        return [self.x, self.y, self.z, self.w]


class Transformations3(ctypes.Structure):
    _fields_ = [('translation', Vec3),
                ('rotation', Vec3),
                ('scale', Vec3)]
    
    def __init__(self, translation=None, rotation=None, scale=None):
        if translation is None:
            translation = [0.0, 0.0, 0.0]
        if rotation is None:
            rotation = [0.0, 0.0, 0.0]
        if scale is None:
            scale = [1.0, 1.0, 1.0]
        
        t_translation = Vec3(translation)
        t_rotation = Vec3(rotation)
        t_scale = Vec3(scale)
        
        super(Transformations3, self).__init__(
            t_translation, t_rotation, t_scale)
    
    def serialize(self):
        """Makes serialized data from necessary data for server"""
        
        tr3_list = [self.translation.x,
                    self.translation.z,
                    self.rotation.y]

        return tr3_list
    
    def to_list(self):
        """Convert Transformations3 to list"""
        
        return [self.translation.to_list(),
                self.rotation.to_list(),
                self.scale.to_list()]


class Transformations4(ctypes.Structure):
    _fields_ = [('translation', Vec4),
                ('rotation', Vec4),
                ('scale', Vec4)]
    
    def __init__(self, translation=None, rotation=None, scale=None):
        if translation is None:
            translation = [0.0, 0.0, 0.0, 0.0]
        if rotation is None:
            rotation = [0.0, 0.0, 0.0, 0.0]
        if scale is None:
            scale = [1.0, 1.0, 1.0, 0.0]
            
        t_translation = Vec4(translation)
        t_rotation = Vec4(rotation)
        t_scale = Vec4(scale)
        
        super(Transformations4, self).__init__(
            t_translation, t_rotation, t_scale)
    
    def to_list(self):
        """Convert Transformations3 to list"""
        
        return [self.translation.to_list(),
                self.rotation.to_list(),
                self.scale.to_list()]
