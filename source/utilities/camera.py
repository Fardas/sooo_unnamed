"""Camera class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import math

# External libraries
import numpy
import pyrr


class Camera:
    def __init__(self, position=pyrr.Vector3([0.0, 0.0, 0.0]),
                 front=pyrr.Vector3([0.0, 0.0, -1.0]),
                 up=pyrr.Vector3([0.0, 1.0, 0.0]),
                 right=pyrr.Vector3([1.0, 0.0, 0.0]),
                 yaw=-90.0, pitch=0.0,
                 world_up=pyrr.Vector3([0.0, 1.0, 0.0])):
        
        self.camera_position = position
        self.camera_front = front
        self.camera_up = up
        self.camera_right = right
        
        self.camera_yaw = yaw
        self.camera_pitch = pitch
        
        self.speed = 4.0
        self.sensitivity = 0.2
        self.zoom = 45.0
        
        self.WORLD_UP = world_up
        self.update_camera()

    def compute_position(self, forward, right, sprint, delta_time):
        """Compute position of free camera"""
        
        velocity = self.speed * delta_time
        
        if forward * right != 0:
            self.camera_position += pyrr.vector.normalise(
                self.camera_front * forward +
                self.camera_right * right) * velocity * sprint
        else:
            if forward != 0:
                self.camera_position += self.camera_front * \
                                        forward * velocity * sprint
            else:
                self.camera_position += self.camera_right * \
                                        right * velocity * sprint
        
        self.update_camera()

    def compute_direction(self, mouse_x_delta, mouse_y_delta,
                          constrain_pitch=True):
        """Compute direction of free camera"""
        
        mouse_x_delta *= self.sensitivity
        mouse_y_delta *= self.sensitivity
    
        self.camera_yaw += mouse_x_delta
        self.camera_pitch += mouse_y_delta
    
        if constrain_pitch:
            if self.camera_pitch > 89.0:
                self.camera_pitch = 89.0
            if self.camera_pitch < -89.0:
                self.camera_pitch = -89.0
    
        self.update_camera()

    def compute_zoom(self, mouse_scroll):
        """Computes zoom"""
        
        if 1.0 <= self.zoom <= 45.0:
            self.zoom -= mouse_scroll
        if self.zoom <= 1.0:
            self.zoom = 1.0
        if self.zoom >= 45.0:
            self.zoom = 45.0
        
        return self.zoom
    
    def update_camera(self):
        """Update camera parameters"""
        
        temp_front = pyrr.Vector3([0.0, 0.0, 0.0])
        
        temp_front.x = math.cos(math.radians(self.camera_yaw)) * \
                       math.cos(math.radians(self.camera_pitch))
        temp_front.y = math.sin(math.radians(self.camera_pitch))
        temp_front.z = math.sin(math.radians(self.camera_yaw)) * math.cos(
            math.radians(self.camera_pitch))
        
        self.camera_front = pyrr.vector.normalise(temp_front)
        self.camera_right = pyrr.vector.normalise(
            pyrr.vector3.cross(self.camera_front, self.WORLD_UP))
        self.camera_up = pyrr.vector.normalise(
            pyrr.vector3.cross(self.camera_right, self.camera_front))
    
    def set_camera_position(self, position):
        """Sets locked camera position"""
        
        self.camera_position = pyrr.Vector3(position.copy())
    
    def look_at(self):
        """Compute camera position and rotation"""
        
        target = self.camera_position + self.camera_front
        axis_z = pyrr.vector.normalise(self.camera_position - target)
        
        axis_x = pyrr.vector.normalise(
            pyrr.vector3.cross(self.camera_up, axis_z))
        
        axis_y = pyrr.vector3.cross(axis_z, axis_x)
        
        translation = pyrr.matrix44.create_identity(numpy.float32)
        translation[3][0] = -self.camera_position[0]
        translation[3][1] = -self.camera_position[1]
        translation[3][2] = -self.camera_position[2]
        
        rotation = pyrr.matrix44.create_identity(numpy.float32)
        rotation[0][0] = axis_x[0]
        rotation[1][0] = axis_x[1]
        rotation[2][0] = axis_x[2]
        rotation[0][1] = axis_y[0]
        rotation[1][1] = axis_y[1]
        rotation[2][1] = axis_y[2]
        rotation[0][2] = axis_z[0]
        rotation[1][2] = axis_z[1]
        rotation[2][2] = axis_z[2]
        
        return pyrr.matrix44.multiply(translation, rotation)
    
    def get_position(self):
        """Returns camera position"""
        
        return self.camera_position
    
    def get_up(self):
        """Returns UP vector of the camera"""
        
        return self.camera_up
    
    def get_target(self):
        """Returns camera target"""
        
        return self.camera_position + self.camera_front

