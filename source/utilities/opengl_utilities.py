"""OpenGL utilities"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# External libraries
from OpenGL.GL import *
import OpenGL.GL.shaders as shaders


def generate_texture_array(texture_dict):
    """Generates an array of textures"""
    
    tex_arr = glGenTextures(1)
    
    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D_ARRAY, tex_arr)
    
    glTexStorage3D(
        GL_TEXTURE_2D_ARRAY, 1,
        GL_RGBA8,
        1024, 1024, len(texture_dict)
    )
    
    i = 0
    for texture_path in texture_dict:
        glTexSubImage3D(
            GL_TEXTURE_2D_ARRAY, 0,
            0, 0, i,
            1024, 1024, 1,
            GL_RGBA, GL_UNSIGNED_BYTE,
            texture_dict[texture_path].image)
        i += 1
    
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    
    return tex_arr


def compile_shader(vertex_shader_path, fragment_shader_path):
    """Compiles vertex and fragment shaders"""
    
    vert_sh = \
        load_shader(vertex_shader_path, GL_VERTEX_SHADER)
    
    if vert_sh is None:
        return -1
    
    frag_sh = \
        load_shader(fragment_shader_path, GL_FRAGMENT_SHADER)
    
    if frag_sh is None:
        return -2
    
    return OpenGL.GL.shaders.compileProgram(vert_sh, frag_sh)
    

def load_shader(shader_path, shader_type):
    """Loads a shader by name and type"""
    
    if not shader_path.exists():
        return None
    
    with shader_path.open('r') as shader_file:
        shader_string = shader_file.read()

    try:
        shader = shaders.compileShader(shader_string, shader_type)
        return shader
        
    except RuntimeError:
        return None
