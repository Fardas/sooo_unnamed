"""Server utilities"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import ast
import inspect
import json
import math
import operator
import pathlib
import random
import time

from os import walk

# External libraries
import pyrr

# Packages
import source.network.net_event as ne
import source.utilities.utilities as uty

# Data classes
import source.data_classes.game_element as ge
import source.data_classes.game_object as go
import source.data_classes.main_data as md


class Match:
    MAX_PLAYERS = 20
    MAX_MATCH_TIME = 60  # In minutes
    MAX_SCORE = 99
    MATCH_WAIT_TIME = 5
    
    def __init__(self, map_name='map_1', game_mode='tdm',
                 max_players=10, match_time=5, max_score=20):
        
        # Match time
        self.match_time = match_time
        self.match_start_time = time.perf_counter() + 0.001
        self.current_match_time = self.match_time * 60

        if self.match_time > Match.MAX_MATCH_TIME:
            self.match_time = Match.MAX_MATCH_TIME
        
        elif self.match_time < 2:
            self.match_time = 2
        
        # Maximum number of players
        self.max_players = max_players

        if self.max_players > Match.MAX_PLAYERS:
            self.max_players = Match.MAX_PLAYERS

        elif self.max_players < 2:
            self.max_players = 2

        # Maximum score of a match
        self.max_score = max_score

        if self.max_score > Match.MAX_SCORE:
            self.max_score = Match.MAX_SCORE
        
        elif self.max_score < 2:
            self.max_score = 2
        
        # Score counters
        self.team_score = {'team_a': 0, 'team_b': 0}
        self.player_score = {}
        
        # Timing
        self.update_time = {}
        self.dynamic_update_time = {'delta': 0.0, 'last': 0.0}

        # Communication events
        self.player_events = {}
        self.lock_events = {}
        self.net_events = {}
        
        # Game data
        self.map_name = map_name
        self.game_mode = game_mode
        self.players = {}
        
        # Match data
        self.match_ended = False
        self.match_end_time = 0
        
        # Map elements
        self.static_elements = {}
        self.static_objects = {}
        
        # Dynamic elements
        self.dynamic_elements = {}
        self.dynamic_objects = {}
        
        # Collision
        self.collider = Collider()

        # Flag position
        self.flag_start_position = {'team_a': None, 'team_b': None}
        self.flag_at_base = {'team_a': True, 'team_b': True}
        self.flag_holders = {'flag_a': None, 'flag_b': None}
        
        # Bullet data
        self.bullet_timing = {}
        self.bullet_directions = {'team_0': {},
                                  'team_a': {},
                                  'team_b': {}}
        
    def init(self):
        """Initializes Match object"""
        
        # Preloads dynamic elements
        if self.preload_dynamic_elements() != 0:
            print("Couldn't preload dynamic elements!")
            return -1

        # Loads static elements of map
        if self.load_static_elements() != 0:
            print("Couldn't load map file!")
            return -1

        if self.game_mode == 'dm':
            if 'team_0_marker' not in self.static_elements:
                print('team_0_marker missing from map file!')
                return -1

        elif self.game_mode == 'tdm' or self.game_mode == 'ctf':
            if 'team_a_marker' not in self.static_elements:
                print('team_a_marker missing from map file!')
                return -1
    
            elif 'team_b_marker' not in self.static_elements:
                print('team_b_marker missing from map file!')
                return -1
    
            elif self.game_mode == 'ctf':
                if 'flag_a_marker' not in self.static_elements:
                    print('flag_a_marker missing from map file!')
                    return -1
        
                elif 'flag_b_marker' not in self.static_elements:
                    print('flag_b_marker miscsing from map file!')
                    return -1
        
                else:
                    self.get_flag_places()
                    self.reset_flags()

        # Creates bounding boxes of static elements
        self.collider.get_static_bounds(self.static_elements)

        return 0
    
    # Game functions
    def player_shot(self, shooting_player, shot_player):
        """Handle player got shot"""
    
        end = shot_player.dec_health()
    
        if end:
            print(shooting_player.name + ' shot down ' +
                  shot_player.name + '!')
        
            self.increase_player_score(shooting_player)
        
            if self.game_mode == 'tdm':
                self.increase_team_score(shooting_player)
        
            self.place_player(shot_player)
    
        self.player_health_event(shot_player)

    def flag_captured(self, player):
        """Player returned enemy flag to base"""
    
        print(player.name + ' returned the enemy flag to base!')
    
        self.increase_team_score(player)

    def place_player(self, player):
        """Place player to a marker of player's team"""
    
        team_marker = \
            self.static_elements[player.team + '_marker']
    
        index = \
            random.randint(0, len(team_marker.transformations) - 1)
    
        self.dynamic_elements[player.player_model]. \
            transformations[player.name] = \
            team_marker.transformations[index]
    
        if self.game_mode == 'ctf':
            if player.team == 'team_a':
                enemy_flag = 'flag_b'
        
            else:
                enemy_flag = 'flag_a'
        
            if player == self.flag_holders[enemy_flag]:
                self.flag_holders[enemy_flag] = None
    
        player.revive()

    def increase_player_score(self, player):
        """Increase player score and check if max score reached"""
    
        self.player_score[player.name] += 1
        self.player_score_event()
    
        if self.game_mode == 'dm':
            if self.player_score[player.name] >= self.max_score:
                self.end_match()
    
        print("Players' score:\n", self.player_score)

    def increase_team_score(self, player):
        """Increase team score and check if max score reached"""
    
        self.team_score[player.team] += 1
    
        self.team_score_event()
    
        if self.team_score[player.team] >= self.max_score:
            self.end_match()
    
        print("Teams' score\n", self.team_score)

    def reset_flags(self):
        """Resets flags' positions"""
        
        self.dynamic_elements['flag_a'].transformations['flag_a'] = \
            self.flag_start_position['team_a']
    
        self.dynamic_elements['flag_b'].transformations['flag_b'] = \
            self.flag_start_position['team_b']

    def get_flag_places(self):
        """Gets flag places from marker positions"""
    
        self.flag_start_position['team_a'] = \
            self.static_elements['flag_a_marker'].transformations[0]
    
        self.flag_start_position['team_b'] = \
            self.static_elements['flag_b_marker'].transformations[0]
    
    # Game element functions
    def update(self):
        """Updates game elements"""
    
        self.update_bullets_position()
    
        if self.game_mode == 'ctf':
            self.collider.flag_player_collision(self)
            self.collider.flag_base_collision(self)
        
            self.update_flags_position()

    def preload_dynamic_elements(self):
        """Preloads dynamic elements"""
    
        dgo_path = \
            md.MAIN_DIR.joinpath('resources', 'objects', 'dynamic')
    
        element_id = 0
        for (dir_path, dir_names, file_names) in walk(dgo_path):
        
            for file_name in file_names:
                object_path = pathlib.Path(dir_path, file_name)
            
                object_name = file_name.split('.')[0]
            
                game_object, success = \
                    self.load_object(object_path, 'dynamic')
            
                if success != 0:
                    return -1
            
                de = ge.DynamicElement(element_name=object_name,
                                       element_id=element_id,
                                       game_object=game_object,
                                       transformations={})
                self.dynamic_elements[game_object.object_name] = de
                element_id += 1
    
        return 0

    def load_static_elements(self):
        """Loads static elements from map file"""
    
        self.static_elements = {}
    
        map_path = md.MAIN_DIR.joinpath(
            'resources', 'maps', self.map_name + '.map')
        
        static_object_path = md.MAIN_DIR.joinpath(
            'resources', 'objects', 'static')
    
        if not map_path.exists():
            print('No map file named ' + self.map_name + '.map found!')
            return -7
    
        else:
            with map_path.open('r') as map_file:
                try:
                    map_dict = json.loads(map_file.read())
                
                except json.decoder.JSONDecodeError:
                    return -2
                
                element_id = 0
                for element_path in map_dict:
                    object_path = static_object_path.\
                        joinpath(element_path + '.go')
                    
                    object_name = object_path.stem
                    
                    game_object, success = \
                        self.load_object(object_path, 'static')
                    
                    if success != 0:
                        return -3
                    
                    tr_list = []
                    for item in map_dict[element_path]:
                        try:
                            trs = uty.Transformations3(
                                translation=item[0],
                                rotation=item[1],
                                scale=item[2])
                        
                        except IndexError:
                            return -5
                    
                        tr_list.append(trs)
                    
                    static_element = \
                        ge.StaticElement(element_name=object_name,
                                         element_id=element_id,
                                         game_object=game_object,
                                         transformations=tr_list)
                    
                    self.static_elements[
                        object_name] = static_element
                    
                    element_id += 1
                
            if self.game_mode == 'dm':
                if 'team_0_marker' not in \
                        self.static_elements:
                    return -4
            
            elif self.game_mode == 'tdm':
                if 'team_a_marker' not in \
                        self.static_elements:
                    return -4
                
                elif 'team_b_marker' not in \
                        self.static_elements:
                    return -4
            
            elif self.game_mode == 'ctf':
                if 'team_a_marker' not in \
                        self.static_elements:
                    return -4

                elif 'team_b_marker' not in \
                        self.static_elements:
                    return -4
                
                elif 'flag_a_marker' not in \
                        self.static_elements:
                    return -4

                elif 'flag_b_marker' not in \
                        self.static_elements:
                    return -4
            
            return 0

    def load_object(self, object_path, object_type):
        """Loads object from object file"""
    
        if not object_path.exists():
            return None, -1
        
        if object_path not in self.static_objects and \
                object_path not in self.dynamic_objects:
            with object_path.open('r') as game_object_file:
                args = inspect.getfullargspec(go.GameObject).args
                args.remove('self')
            
                attr_dict = {'object_id': None}
                for line in game_object_file:
                    attr_name, attr_value = line.strip('\n').split(
                        ':')
                
                    try:
                        attr_dict[attr_name] = \
                            ast.literal_eval(attr_value)
                
                    except (ValueError, SyntaxError):
                        attr_dict[attr_name] = attr_value
            
                if set(list(attr_dict)) == set(args):
                    # New object id
                    if object_type == 'static':
                        attr_dict['object_id'] = \
                            len(self.static_objects)
                
                    elif object_type == 'dynamic':
                        attr_dict['object_id'] = \
                            len(self.dynamic_objects)

                    # Model:
                    model_path = md.MAIN_DIR.joinpath(
                        'resources', 'meshes', attr_dict['model'])

                    model_name = str(model_path.stem)
                    
                    model = go.Model(model_name, 0, model_path)
                    success = model.load_data()

                    if success != 0:
                        return None, -2
                    
                    attr_dict['model'] = None
                    
                    # Texture
                    texture_path = md.MAIN_DIR.joinpath(
                        'resources', 'textures', attr_dict['texture'])

                    texture_name = str(texture_path.stem)
                    
                    texture = go.Texture(texture_name, 0, texture_path)

                    success = texture.load_data()

                    if success != 0:
                        return None, -3
                    
                    attr_dict['texture'] = None
                
                    # Collision type
                    attr_dict['collision_type'] = \
                        go.CollisionType[
                            attr_dict['collision_type']]
                
                    if object_type == 'static':
                        self.static_objects[object_path] = \
                            go.GameObject(**attr_dict)
                    
                        # Object
                        return self.static_objects[object_path], 0
                
                    elif object_type == 'dynamic':
                        self.dynamic_objects[object_path] = \
                            go.GameObject(**attr_dict)
                    
                        # Object
                        return self.dynamic_objects[object_path], 0
            
                else:
                    print('Error: missing or wrong attributes in file!')
                    return None, -4
    
        else:
            if object_type == 'static':
                return self.static_objects[object_path], 0
        
            elif object_type == 'dynamic':
                return self.dynamic_objects[object_path], 0

    def calculate_match_time(self):
        """Calculates match time,
        and creates time event, if necessary"""
    
        from_start_time = \
            int(time.perf_counter() - self.match_start_time)
    
        new_match_time = \
            int(self.match_time * 60 - from_start_time)
    
        if new_match_time != self.current_match_time:
            self.current_match_time = new_match_time
        
            for player in self.net_events:
                if new_match_time > 0:
                    self.net_events[player].append(
                        ne.NetEvent(ne.NetEventType.TIME,
                                    self.current_match_time))
            
                else:
                    self.current_match_time = 0
                    self.end_match()

    def player_health_event(self, sel_player):
        """Adds score event to the events list"""
    
        self.net_events[sel_player].append(
            ne.NetEvent(ne.NetEventType.PLAYER_HEALTH,
                        sel_player.health))

    def player_score_event(self):
        """Adds score event to the events list"""
    
        sorted_score = sorted(self.player_score.items(),
                              key=operator.itemgetter(1),
                              reverse=True)
        
        for player in self.net_events:
            self.net_events[player].append(
                ne.NetEvent(ne.NetEventType.PLAYER_SCORE,
                            sorted_score))

    def team_score_event(self):
        """Adds score event to the events list"""
    
        for player in self.net_events:
            self.net_events[player].append(
                ne.NetEvent(ne.NetEventType.TEAM_SCORE,
                            self.team_score))

    def new_player_event(self, sel_player):
        """Adds new player event to the events list"""
    
        for player in self.net_events:
            if player != sel_player:
                self.net_events[player].append(
                    ne.NetEvent(ne.NetEventType.NEW_PLAYER,
                                sel_player.__dict__))

    def disc_player_event(self, sel_player):
        """Adds disconnected player event to the event list"""
    
        for player in self.net_events:
            if player != sel_player:
                self.net_events[player].append(
                    ne.NetEvent(ne.NetEventType.DISC_PLAYER,
                                sel_player.name))

    def dynamic_data_event(self):
        """Adds dynamic data event to the events list"""
    
        dynamic_json = self.dynamic_to_json()
    
        for dynamic in dynamic_json.values():
            for player in self.net_events:
                self.net_events[player].append(
                    ne.NetEvent(ne.NetEventType.DYNAMIC_DATA,
                                dynamic))

    def update_bullets_position(self):
        """Updates position of bullets"""
    
        delete = []
        for team in self.bullet_directions:
            for bullet_name in self.bullet_directions[team]:
                bullet_tr = self.dynamic_elements['bullet']. \
                    transformations[bullet_name].to_list()
            
                move = self.bullet_directions[team][
                    bullet_name].copy()
            
                move *= (self.update_time['current'] -
                         self.update_time['last'])
            
                move *= md.BULLET_SPEED_CONST
            
                bullet_tr[0][0] += move[0]
                bullet_tr[0][2] += move[2]
            
                collision = self.collider.bullet_static_collision(
                    bullet_tr[0])
            
                if not collision:
                    collision = self.collider.bullet_player_collision(
                        self, bullet_tr[0], team, bullet_name)
            
                if collision or not (
                        -500 < bullet_tr[0][0] < 500 and
                        -500 < bullet_tr[0][2] < 500):
                
                    delete.append([team, bullet_name])
            
                else:
                    self.dynamic_elements['bullet']. \
                        transformations[bullet_name] = \
                        uty.Transformations3(*bullet_tr)
    
        for bullet in delete:
            del self.dynamic_elements['bullet']. \
                transformations[bullet[1]]
            del self.bullet_directions[bullet[0]][bullet[1]]

    def update_flags_position(self):
        """Updates flag position if there is a flag holder"""
    
        for team, flag in [('team_a', 'flag_a'),
                           ('team_b', 'flag_b')]:
            if self.flag_holders[flag] is not None:
                player_model = self.flag_holders[flag].player_model
                player_name = self.flag_holders[flag].name
                self.dynamic_elements[flag].transformations[flag] = \
                    self.dynamic_elements[player_model]. \
                        transformations[player_name]

    def update_player_position(self, readable_socket, movement_data):
        """Updates player position"""
    
        move_x = movement_data[0]
    
        move_z = movement_data[1]
    
        move = pyrr.Vector3([move_x, 0.0, move_z])
    
        player_model = self.players[readable_socket].player_model
        player_name = self.players[readable_socket].name
    
        size = self.dynamic_elements[player_model].game_object.size
    
        trs = self.dynamic_elements[player_model].\
            transformations[player_name]
    
        trs = trs.to_list()
    
        position = [trs[0][0], trs[0][2]]
        radius = trs[2][0] * size[0]
    
        if move_x != 0 or move_z != 0:
            move_normal = pyrr.vector.normalize(move)
        
            move = move_normal * (self.update_time['current'] -
                                  self.update_time['last'])
        
            move *= md.SPEED_CONST
        
            no_collision = True
            # Check rectangle bounds
            for bound in self.collider.static_bounds['rectangle']:
                new_move = Collider.check_rectangle_collision(
                    position, move, radius, bound)
                
                if new_move is not None:
                    # If there is a collision
                    no_collision = False
                    
                    new_normal = pyrr.vector.normalize(new_move)
                
                    move = new_move
        
            # Check circle bounds
            for bound in self.collider.static_bounds['circle']:
                new_move = Collider.check_circle_collision(
                    position, move, radius, bound)
            
                if new_move is not None:
                    # If there is a collision
                    no_collision = False
                
                    move = new_move
    
        player_name = self.players[readable_socket].name
        player_model = self.players[readable_socket].player_model
    
        tr_list = self.dynamic_elements[player_model]. \
            transformations[player_name].to_list()
    
        for value_index in range(0, 3):
            tr_list[0][value_index] += move[value_index]
    
        tr_list[1][1] = movement_data[2]
    
        self.dynamic_elements[player_model]. \
            transformations[player_name] = \
            uty.Transformations3(*tr_list)

    def shoot_bullet(self, player):
        """Creates a bullet at player position with player direction"""
    
        if self.update_time['current'] - self.bullet_timing[
            player] > \
                md.BULLET_TIME and not self.match_ended:
        
            self.bullet_timing[player] = self.update_time['current']
            player_name = player.name
            player_team = player.team
            player_model = player.player_model
        
            player_tr = self.dynamic_elements[player_model]. \
                transformations[player_name].to_list()
        
            rotation = player_tr[1][1]
            position = player_tr[0]
            position[1] += 1.0
        
            bullet_name = player_name + '_0'
            i = 0
            while bullet_name in self.bullet_directions[
                player_team]:
                bullet_name = player_name + '_' + str(i)
                i += 1
        
            dir_vec = [math.cos(rotation), 0, -math.sin(rotation)]
            dir_vec = pyrr.vector.normalize(pyrr.Vector3(dir_vec))
        
            self.bullet_directions[player_team][
                bullet_name] = dir_vec
        
            self.dynamic_elements['bullet']. \
                transformations[bullet_name] = \
                uty.Transformations3(*[position + dir_vec * 1.5,
                                       [0, rotation, 0],
                                       [1, 1, 1]])

    def dynamic_to_json(self):
        """Packs dynamic data into list of json bytes"""
    
        json_dict = {}
        for dynamic in self.dynamic_elements.values():
            if dynamic.transformations:
                
                json_dict[dynamic.element_name] = dynamic.serialize()
        
            else:
                json_dict[dynamic.element_name] = \
                    {'en': dynamic.element_name}
    
        return json_dict

    def end_match(self):
        """End match, set end time"""
    
        for player in self.net_events:
            self.net_events[player].append(
                ne.NetEvent(ne.NetEventType.TIME,
                            self.current_match_time))
        
            self.net_events[player].append(
                ne.NetEvent(ne.NetEventType.MATCH,
                            'MATCH_ENDED'))
    
        self.match_ended = True
        self.match_end_time = time.perf_counter()

    def restart_match(self):
        """Restart match"""
    
        self.match_ended = False
    
        self.match_start_time = time.perf_counter() + 0.001
    
        self.calculate_match_time()
    
        for player in self.net_events:
            self.net_events[player].append(
                ne.NetEvent(ne.NetEventType.MATCH,
                            'MATCH_STARTED'))
    
        self.team_score = {'team_a': 0, 'team_b': 0}
    
        for player in self.players.values():
            self.player_score[player.name] = 0
            self.place_player(player)
        
            self.player_health_event(player)
    
        self.player_score_event()
        self.team_score_event()
    
        if self.game_mode == 'ctf':
            self.reset_flags()


class Collider:
    def __init__(self):
        
        # Collision
        self.static_bounds = {}
    
    # Flag collision
    @staticmethod
    def flag_player_collision(match):
        """Checks if a player collides with a flag"""
        
        for team, flag in [('team_a', 'flag_a'), ('team_b', 'flag_b')]:
            f_trs = match.dynamic_elements[flag].\
                transformations[flag]

            f_trs = f_trs.to_list()
            
            f_pos = [f_trs[0][0], f_trs[0][2]]
            
            if match.flag_holders[flag] is None:
                for player in match.players.values():
                    player_model = player.player_model
                    player_name = player.name

                    p_size = match.dynamic_elements[player_model]. \
                        game_object.size
                    
                    p_trs = match.dynamic_elements[player_model]. \
                        transformations[player_name]

                    p_trs = p_trs.to_list()
    
                    p_pos = [p_trs[0][0], p_trs[0][2]]
                    radius = p_trs[2][0] * p_size[0]
                    radius_sqr = radius ** 2
                    dist_sqr = (f_pos[0] - p_pos[0]) ** 2 + \
                               (f_pos[1] - p_pos[1]) ** 2
    
                    # Checking collision
                    if dist_sqr < radius_sqr:
                        if player.team == team:
                            match.dynamic_elements[flag].\
                                transformations[flag] = \
                                match.flag_start_position[team]
                            match.flag_at_base[team] = True
                            
                        else:
                            match.flag_at_base[team] = False
                            match.flag_holders[flag] = player
    
    @staticmethod
    def flag_base_collision(match):
        """Checks if a flag collides with a flag base"""
        
        for team, flag in [('team_a', 'flag_a'), ('team_b', 'flag_b')]:
            f_trs = match.dynamic_elements[flag]. \
                transformations[flag]
    
            f_trs = f_trs.to_list()
    
            f_pos = [f_trs[0][0], f_trs[0][2]]
            
            for base_team in match.flag_start_position:
                if team != base_team:
                    b_pos = \
                        match.flag_start_position[base_team].to_list()[0]
                    
                    dist_sqr = (f_pos[0] - b_pos[0]) ** 2 + \
                               (f_pos[1] - b_pos[2]) ** 2
            
                    # Checking collision
                    if dist_sqr < 2.0:
                        if match.flag_at_base[base_team]:
                            match.dynamic_elements[flag].\
                                transformations[flag] = \
                                match.flag_start_position[team]
                            match.flag_captured(
                                match.flag_holders[flag])
                            match.flag_holders[flag] = None
    
    # Bullet collision
    def bullet_static_collision(self, position):
        """Check bullet's collision with static objects"""
        
        for collision_type in ['rectangle', 'circle']:
            for bound in self.static_bounds[collision_type]:
                if collision_type == 'rectangle':
                    if bound[1][0] <= position[0] <= bound[0][0] and \
                            bound[3][1] <= position[2] <= bound[0][1]:
                        return True
                
                elif collision_type == 'circle':
                    distance_sqr = (position[0] - bound[0][0])**2 + \
                                   (position[2] - bound[0][1])**2
                    radius_sqr = bound[1]**2
                    
                    if distance_sqr <= radius_sqr:
                        return True
        
        return False
    
    @staticmethod
    def bullet_player_collision(match, position, team, bullet):
        """Check bullet's collision with players"""
        
        for player in match.players.values():
            
            # Check if the bullet is the player's bullet
            bullet_player = bullet.rpartition('_')[0]
    
            if bullet_player != player.name:
                player_tr = \
                    match.dynamic_elements[player.player_model].\
                    transformations[player.name].to_list()
        
                distance_sqr = (position[0] - player_tr[0][0]) ** 2 + \
                               (position[2] - player_tr[0][2]) ** 2
                radius_sqr = \
                    match.dynamic_elements[player.player_model].\
                    game_object.size[0] ** 2
                
                if distance_sqr <= radius_sqr:
                    shooting_player = None
                    for pl in match.players.values():
                        if pl.name == bullet_player:
                            shooting_player = pl
            
                    if match.game_mode == 'dm':
                        match.player_shot(shooting_player, player)
            
                    elif team != player.team:
                        match.player_shot(shooting_player, player)
            
                    return True
    
        return False
    
    # Collision detection
    @staticmethod
    def check_rectangle_collision(position, move, radius, bound):
        """Checks collision with rectangle"""
    
        a = bound[0]
        b = bound[1]
        c = bound[2]
        d = bound[3]
    
        in_larger = False
        rect_collision = False
        circ_collision = False
    
        new_pos = [position[0] + move[0], position[1] + move[2]]
    
        # Check rectangle plus radius (larger rectangle)
        # Check for x:
        if b[0] - radius < new_pos[0] < a[0] + radius:
            # Check for z:
            if c[1] - radius < new_pos[1] < a[1] + radius:
                # Circle is in larger rectangle
                in_larger = True
    
        circ_dist = 0
        circ_center = None
    
        if in_larger:
            # Check rectangle plus radius (vertical rectangle)
            # Check for x:
            if b[0] < new_pos[0] < a[0]:
                # Check for z:
                if c[1] - radius < new_pos[1] < a[1] + radius:
                    # Circle is in larger rectangle
                    rect_collision = True
        
            # Check rectangle plus radius (horizontal rectangle)
            elif b[0] - radius < new_pos[0] < a[0] + radius:
                # Check for z:
                if c[1] < new_pos[1] < a[1]:
                    # Circle is in horizontal rectangle
                    rect_collision = True
        
            radius_sqr = radius ** 2
            dist_a_sqr = (a[0] - new_pos[0]) ** 2 + \
                         (a[1] - new_pos[1]) ** 2
        
            # Checking corner a
            if dist_a_sqr < radius_sqr:
                circ_collision = True
                circ_dist = dist_a_sqr
                circ_center = a
        
            else:
                dist_b_sqr = (b[0] - new_pos[0]) ** 2 + \
                             (b[1] - new_pos[1]) ** 2
            
                # Checking corner b
                if dist_b_sqr < radius_sqr:
                    circ_collision = True
                    circ_dist = dist_b_sqr
                    circ_center = b
            
                else:
                    dist_c_sqr = (c[0] - new_pos[0]) ** 2 + \
                                 (c[1] - new_pos[1]) ** 2
                
                    # Checking corner c
                    if dist_c_sqr < radius_sqr:
                        circ_collision = True
                        circ_dist = dist_c_sqr
                        circ_center = c
                
                    else:
                        dist_d_sqr = (d[0] - new_pos[0]) ** 2 + \
                                     (d[1] - new_pos[1]) ** 2
                    
                        # Checking corner d
                        if dist_d_sqr < radius_sqr:
                            circ_collision = True
                            circ_dist = dist_d_sqr
                            circ_center = d
    
        # If collision, return the axis perpendicular to colliding side
        if rect_collision or circ_collision:
            # da
            if d[1] - radius < new_pos[1] < a[1] + radius and \
                    new_pos[0] >= a[0] + radius / 10:
                if rect_collision:
                    dist_x = a[0] + radius - new_pos[0] + 0.001
                    normal = pyrr.Vector3([1.0, 0.0, 0.0])
                    normal *= dist_x
                    new_move = move + normal
                    return new_move
            
                elif circ_collision:
                    normal = pyrr.vector.normalize(
                        pyrr.Vector3([circ_center[0] - new_pos[0], 0,
                                      circ_center[1] - new_pos[1]]))
                    circ_dist = math.sqrt(circ_dist)
                    normal *= radius - circ_dist
                    new_move = move - normal
                    return new_move
        
            # bc
            elif d[1] - radius < new_pos[1] < a[1] + radius and \
                    new_pos[0] <= b[0] - radius / 10:
                if rect_collision:
                    dist_x = b[0] - radius - new_pos[0] - 0.001
                    normal = pyrr.Vector3([1.0, 0.0, 0.0])
                    normal *= dist_x
                    new_move = move + normal
                    return new_move
            
                elif circ_collision:
                    normal = pyrr.vector.normalize(
                        pyrr.Vector3([circ_center[0] - new_pos[0], 0,
                                      circ_center[1] - new_pos[1]]))
                    circ_dist = math.sqrt(circ_dist)
                    normal *= radius - circ_dist
                    new_move = move - normal
                    return new_move
        
            # ab
            elif b[0] - radius < new_pos[0] < a[0] + radius and \
                    new_pos[1] >= a[1] + radius / 10:
                if rect_collision:
                    dist_y = a[1] + radius - new_pos[1] + 0.001
                    normal = pyrr.Vector3([0.0, 0.0, 1.0])
                    normal *= dist_y
                    new_move = move + normal
                    return new_move
            
                elif circ_collision:
                    normal = pyrr.vector.normalize(
                        pyrr.Vector3([circ_center[0] - new_pos[0], 0,
                                      circ_center[1] - new_pos[1]]))
                    circ_dist = math.sqrt(circ_dist)
                    normal *= radius - circ_dist
                    new_move = move - normal
                    return new_move
        
            # cd
            elif b[0] - radius < new_pos[0] < a[0] + radius and \
                    new_pos[1] <= c[1] - radius / 10:
                if rect_collision:
                    dist_y = c[1] - radius - new_pos[1] - 0.001
                    normal = pyrr.Vector3([0.0, 0.0, 1.0])
                    normal *= dist_y
                    new_move = move + normal
                    return new_move
            
                elif circ_collision:
                    normal = pyrr.vector.normalize(
                        pyrr.Vector3([circ_center[0] - new_pos[0], 0,
                                      circ_center[1] - new_pos[1]]))
                    circ_dist = math.sqrt(circ_dist)
                    normal *= radius - circ_dist
                    new_move = move - normal
                    return new_move
    
        else:
            return None

    @staticmethod
    def check_circle_collision(position, move, radius, bound):
        """Checks collision with circle"""
    
        new_pos = [position[0] + move[0], position[1] + move[2]]
    
        o = bound[0]
        radius_sqr = (bound[1] + radius) ** 2
        dist_sqr = (o[0] - new_pos[0]) ** 2 + \
                   (o[1] - new_pos[1]) ** 2
    
        # Checking collision
        if dist_sqr < radius_sqr:
            dist_x = new_pos[0] - o[0]
            dist_z = new_pos[1] - o[1]
        
            normal = \
                pyrr.vector.normalize(pyrr.Vector3([dist_x, 0, dist_z]))
        
            circ_dist = math.sqrt(dist_sqr)
            normal *= bound[1] + radius - circ_dist
            new_move = move + normal
            return new_move
    
        return None
    
    # Bounding box calculation
    def get_static_bounds(self, static_elements):
        """Gets the bounding boxes of static elements"""
    
        self.static_bounds = {'rectangle': [], 'circle': []}
    
        for element in static_elements.values():
            if element.game_object.collision_type == \
                    go.CollisionType.RECTANGLE:
                for i in range(0, len(element.transformations)):
                    self.static_bounds['rectangle'].append(
                        self.get_rectangle_area(
                            element.game_object.size.copy(),
                            element.transformations[i]))
        
            elif element.game_object.collision_type == \
                    go.CollisionType.CIRCLE:
                for i in range(0, len(element.transformations)):
                    self.static_bounds['circle'].append(
                        self.get_circle_area(
                            element.game_object.size,
                            element.transformations[i]))
    
    @staticmethod
    def get_circle_area(size, transformations):
        """Returns the position and radius of circle bounding boxes"""
    
        trs = transformations.to_list()
    
        radius = size[0] * trs[2][0]
    
        return [[trs[0][0], trs[0][2]], radius]

    @staticmethod
    def get_rectangle_area(size, transformations):
        """Returns the vertices of rectangle bounding boxes"""
    
        trs = transformations.to_list()
    
        size_x_h = size[0] * trs[2][0] / 2.0
        size_z_h = size[1] * trs[2][2] / 2.0
    
        a = [trs[0][0] + size_x_h, trs[0][2] + size_z_h]
        b = [trs[0][0] - size_x_h, trs[0][2] + size_z_h]
        c = [trs[0][0] - size_x_h, trs[0][2] - size_z_h]
        d = [trs[0][0] + size_x_h, trs[0][2] - size_z_h]
        o = [trs[0][0], trs[0][2]]
    
        return [a, b, c, d, o]