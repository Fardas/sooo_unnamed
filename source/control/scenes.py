"""Separate scenes for separate purposes"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import math

from OpenGL.GL import *

# External libraries
import glfw
import pyrr

# Packages
#   Data classes
import source.data_classes.game_element as ge
import source.data_classes.main_data as md
#   Control
import source.control.unnamed_game as ug
#   Drawers
import source.drawers.dynamic_drawer as dd
import source.drawers.static_drawer as sd
import source.drawers.ung_drawer as ud
#   GUI
import source.gui.gui_instances as gi
#   Network
import source.network.client as uc
import source.network.net_event as ne
#   Utilities
import source.utilities.camera as cam
import source.utilities.unnamed_joystick as uj


class Scene:
    def __init__(self):
        pass
    
    def init(self):
        """Initialize scene"""
        pass
    
    def update(self):
        """Update scene"""
        pass
    
    def render(self):
        """Render scene"""
        pass
    
    @staticmethod
    def update_viewport(width, height):
        """Updates the viewport of screen"""
    
        glViewport(0, 0, width, height)

    def process_key_press(self, key):
        """Processes key press"""
        pass

    def process_key_release(self, key):
        """Processes key release"""
        pass

    def process_chars(self, char):
        """Processes character key hits"""
        pass

    def process_mouse_button_press(self, button):
        """Processes mouse key press"""
        pass

    def process_mouse_button_release(self, button):
        """Processes mouse key release"""
        pass

    def process_mouse_movement(self, delta_x, delta_y):
        """Processes mouse movement"""
        pass

    def process_mouse_scroll(self, offset):
        """Processes mouse scrolling"""
        pass


class MainMenuScene(Scene):
    def __init__(self, gui_name: str = 'main_menu'):
        
        self.static_drawer = None
        self.dynamic_drawer = None
        
        self.guis = {'main_menu': gi.MainMenuGUI(),
                     'game_select': gi.GameSelectGUI(),
                     'joystick_settings': gi.JoystickSettingsGUI(),
                     'disconnected': gi.DisconnectedGUI()}
        self.active_gui_name = gui_name
        
        if gui_name in self.guis:
            for gui in self.guis.values():
                gui.enabled = False
            
            self.guis[gui_name].enabled = True
        
        self.ung_drawer = ud.UNGDrawer(guis=self.guis)
    
    def init(self):
        """Initialize MainMenuScene"""

        # Initialize guis
        for gui_name in list(self.guis):
            if self.guis[gui_name].init() != 0:
                return -1

        # GL options
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        
        md.UInputD.cursor_enabled = True
        
        # ud.UNGDrawer initialization
        
        if self.ung_drawer.init() != 0:
            return -2
        
        return 0
    
    def render(self):
        """Render MainMenuScene"""

        # Clear color and depth buffer before new draw
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        
        # Draw GUI
        self.ung_drawer.draw()

        # Swap the buffers
        glfw.swap_buffers(md.OpenGLD.window)
    
    def process_key_press(self, key):
        """Processes key press"""

        if key == glfw.KEY_F1:
            if glfw.get_window_monitor(md.OpenGLD.window):
                md.GameD.client_settings['fullscreen'] = False
                md.OpenGLD.window_width = 1280
                md.OpenGLD.window_height = 720
                glfw.set_window_monitor(md.OpenGLD.window,
                                        None, 40, 40,
                                        md.OpenGLD.window_width,
                                        md.OpenGLD.window_height,
                                        glfw.DONT_CARE)
    
            else:
                md.GameD.client_settings['fullscreen'] = True
                mode = \
                    glfw.get_video_mode(glfw.get_primary_monitor())
                glfw.set_window_monitor(md.OpenGLD.window,
                                        glfw.get_primary_monitor(),
                                        0, 0,
                                        mode.size.width,
                                        mode.size.height,
                                        glfw.DONT_CARE)
        
        elif key == glfw.KEY_BACKSPACE:
            self.guis[self.active_gui_name].delete_char('b')

        elif key == glfw.KEY_DELETE:
            self.guis[self.active_gui_name].delete_char('d')

        elif key == glfw.KEY_LEFT:
            self.guis[self.active_gui_name].move_cursor(-1)

        elif key == glfw.KEY_RIGHT:
            self.guis[self.active_gui_name].move_cursor(1)
        
        elif key == glfw.KEY_ESCAPE:
            md.UInputD.key_catch = False
    
    def process_key_release(self, key):
        """Processes key release"""
        pass
    
    def process_chars(self, char):
        """Processes character key hits"""
        
        if md.UInputD.cursor_enabled:
            self.guis[self.active_gui_name].input_char(char)
    
    def process_mouse_button_press(self, button):
        """Processes mouse key press"""
        pass
    
    def process_mouse_button_release(self, button):
        """Processes mouse key release"""
        
        if md.UInputD.cursor_enabled:
            saved_element = None
            saved_gui_name = ''
            for gui_name in self.ung_drawer.guis:
                if self.ung_drawer.guis[gui_name].enabled:
                    element = self.ung_drawer.guis[gui_name]. \
                        click_detect()
            
                    if element is not None:
                        saved_element = element
                        saved_gui_name = gui_name
    
            if saved_element is not None:
                self.ung_drawer.guis[saved_gui_name].process_gui_event(
                    button, saved_element)
    
    def process_mouse_movement(self, delta_x, delta_y):
        """Processes mouse movement"""
        
        if md.UInputD.cursor_enabled:
            for gui_name in self.guis:
                if self.guis[gui_name].enabled:
                    self.guis[gui_name].\
                        mouse_hover_detect()
    
    def process_mouse_scroll(self, offset):
        """Processes mouse scrolling"""
        pass


class GameScene(Scene):
    def __init__(self, server_address, server_port,
                 player_name, player_model, player_team):
        
        self.guis = {'game': gi.GameGUI()}
        self.guis['game'].enabled = True
        self.active_gui_name = list(self.guis)[0]
        
        md.GameD.players = {}
        md.GameD.active_player = ge.Player(name=player_name,
                                        team=player_team,
                                        player_model=player_model)
        
        self.client = uc.UnnamedClient(server_address, server_port)
        
    def init(self):
        """Initialize GameScene"""

        # Initialize guis
        for gui_name in list(self.guis):
            if self.guis[gui_name].init() != 0:
                return -1

        # GL options
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        # Reset game_object classes
        self.static_drawer = sd.StaticDrawer(map_name=md.GameD.map_name)
        self.dynamic_drawer = dd.DynamicDrawer()
        self.ung_drawer = ud.UNGDrawer(guis=self.guis)

        # ud.UNGDrawer initialization
        if self.ung_drawer.init() != 0:
            return -2

        # sd.StaticDrawer initialization
        if self.static_drawer.init() != 0:
            return -3
        
        # dd.DynamicDrawer initialization
        if self.dynamic_drawer.init() != 0:
            return -4

        self.change_control_state(True)
        
        # Dynamic elements preloading
        ge.DynamicElement.preload_elements()
        
        # Camera
        md.OpenGLD.camera = cam.Camera()
        md.OpenGLD.camera.camera_pitch = -45
        md.OpenGLD.camera.update_camera()
        
        # Enable cursor
        md.UInputD.cursor_enabled = True

        connected = self.client.exchange_data()
        
        if connected != 0:
            return connected - 4
        
        return 0
    
    def connect(self):
        """Try to connect to server"""
        
        connected = self.client.connect_to_server()
        
        if connected != 0:
            return connected
        
        return 0
        
    def update(self):
        """Update GameScene"""
        
        connected = self.client.exchange_data()

        self.guis['game'].refresh_stats()
        
        if connected != 0:
            mm_scene = MainMenuScene('disconnected')

            md.GameD.active_scene.change_control_state(False)
            ug.UnnamedGame.change_scene(mm_scene)
            mm_scene.guis['disconnected'].change_text(connected)
            
            return connected

        self.dynamic_drawer.reinit()
        
        camera_position = \
            md.GameD.dynamic_elements[
                md.GameD.active_player.player_model].\
            transformations[md.GameD.active_player.name].\
            translation.to_list()
        
        camera_position += pyrr.Vector3([0.0, 25.0, 25.0])

        md.OpenGLD.camera.set_camera_position(camera_position)
        
        # Generate model_matrix
        md.OpenGLD.model = pyrr.matrix44.create_identity()
        
        # Generate view matrix
        md.OpenGLD.view = md.OpenGLD.camera.look_at()

        md.OpenGLD.projection = pyrr. \
            matrix44.create_perspective_projection_matrix(
                md.OpenGLD.fov,
                md.OpenGLD.aspect_ratio[0] / md.OpenGLD.aspect_ratio[1],
                0.1, 1000.0)

        md.OpenGLD.eye = md.OpenGLD.camera.get_position()
        
        if md.UInputD.joystick_enabled and md.OpenGLD.window_focus:
            self.process_joystick_axes()
            self.process_joystick_buttons()
    
    def render(self):
        """Render GameScene"""

        # Clear color and depth buffer before new draw
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Draw dynamic elements
        self.dynamic_drawer.draw()
        
        # Draw static elements
        self.static_drawer.draw()

        # Draw GUI
        self.ung_drawer.draw()

        # Swap the buffers
        glfw.swap_buffers(md.OpenGLD.window)
    
    def process_key_press(self, key):
        """Processes key press"""

        if key == glfw.KEY_F1:
            if glfw.get_window_monitor(md.OpenGLD.window):
                md.GameD.client_settings['fullscreen'] = False
                md.OpenGLD.window_width = 1280
                md.OpenGLD.window_height = 720
                glfw.set_window_monitor(md.OpenGLD.window,
                                        None, 40, 40,
                                        md.OpenGLD.window_width,
                                        md.OpenGLD.window_height,
                                        glfw.DONT_CARE)
    
            else:
                md.GameD.client_settings['fullscreen'] = True
                mode = \
                    glfw.get_video_mode(glfw.get_primary_monitor())
                glfw.set_window_monitor(md.OpenGLD.window,
                                        glfw.get_primary_monitor(),
                                        0, 0,
                                        mode.size.width,
                                        mode.size.height,
                                        glfw.DONT_CARE)
        
        elif key == glfw.KEY_ESCAPE:
            exit_frame = md.GameD.active_scene.guis['game'].\
                search_in_elements('exit_frame')
            
            if exit_frame is not None:
                if exit_frame.visible:
                    md.GameD.active_scene.guis['game'].\
                        hide_element(exit_frame)
                else:
                    md.GameD.active_scene.guis['game']. \
                        reveal_element(exit_frame)
        
                self.change_control_state(not exit_frame.visible)
        
        elif key == glfw.KEY_F2:
            if md.UInputD.joystick_connected and \
                    not md.UInputD.joystick_enabled:
                md.UInputD.joystick_enabled = True
            
            else:
                md.UInputD.joystick_enabled = False
        
        if md.UInputD.enable_control and \
                not md.UInputD.joystick_enabled:
            if key == glfw.KEY_W:
                md.UInputD.move_north = -1
            elif key == glfw.KEY_S:
                md.UInputD.move_south = 1
            elif key == glfw.KEY_A:
                md.UInputD.move_west = -1
            elif key == glfw.KEY_D:
                md.UInputD.move_east = 1

        if not md.UInputD.enable_control:
            if key == glfw.KEY_BACKSPACE:
                self.guis[self.active_gui_name].delete_char('b')
    
            elif key == glfw.KEY_DELETE:
                self.guis[self.active_gui_name].delete_char('d')
    
            elif key == glfw.KEY_LEFT:
                self.guis[self.active_gui_name].move_cursor(-1)
    
            elif key == glfw.KEY_RIGHT:
                self.guis[self.active_gui_name].move_cursor(1)
    
    def process_key_release(self, key):
        """Processes key release"""
        
        if md.UInputD.enable_control:
            if key == glfw.KEY_W:
                md.UInputD.move_north = 0
            elif key == glfw.KEY_S:
                md.UInputD.move_south = 0
            elif key == glfw.KEY_A:
                md.UInputD.move_west = 0
            elif key == glfw.KEY_D:
                md.UInputD.move_east = 0
    
    def process_chars(self, char):
        """Processes character key hits"""
        
        if not md.UInputD.enable_control:
            self.guis[self.active_gui_name].input_char(char)
    
    def process_mouse_button_press(self, button):
        """Processes mouse key press"""
        
        if md.UInputD.enable_control and \
                not md.UInputD.joystick_enabled:
            if button == glfw.MOUSE_BUTTON_LEFT:
                GameScene.shoot()
    
    def process_mouse_button_release(self, button):
        """Processes mouse key release"""
        
        if not md.UInputD.enable_control:
            saved_element = None
            saved_gui_name = ''
            for gui_name in self.ung_drawer.guis:
                if self.ung_drawer.guis[gui_name].enabled:
                    element = self.ung_drawer.guis[gui_name].\
                        click_detect()
            
                    if element is not None:
                        saved_element = element
                        saved_gui_name = gui_name
    
            if saved_element is not None:
                self.ung_drawer.guis[saved_gui_name].process_gui_event(
                    button, saved_element)
    
    def process_mouse_movement(self, delta_x, delta_y):
        """Processes mouse movement"""

        if md.UInputD.enable_control and \
                not md.UInputD.joystick_enabled:
            delta_x = (md.OpenGLD.window_width / 2
                       - md.UInputD.last_x)
            delta_y = (md.OpenGLD.window_height / 2
                       - md.UInputD.last_y) * 2
    
            if delta_y == 0:
                delta_y = 0.01
            # Fix the angle issues caused by 45° camera angle
            delta_y /= 1.41
            md.UInputD.player_rotation = \
                math.atan2(delta_x, delta_y) + math.pi / 2
        
        else:
            for gui_name in self.guis:
                if self.guis[gui_name].enabled:
                    self.guis[gui_name].\
                        mouse_hover_detect()
    
    def process_mouse_scroll(self, offset):
        """Processes mouse scrolling"""
        pass

    @staticmethod
    def process_joystick_axes():
        """Processes joystick axes"""
        
        arr, count = \
            glfw.get_joystick_axes(md.UInputD.u_joystick.js_id)
        
        try:
            if md.UInputD.enable_control:
                md.UInputD.move_north, md.UInputD.move_south = \
                    md.UInputD.u_joystick.check_move_y(arr)

                md.UInputD.move_west, md.UInputD.move_east = \
                    md.UInputD.u_joystick.check_move_x(arr)
                
                angle = md.UInputD.u_joystick.get_look_angle(arr)
                
                if angle is not None:
                    md.UInputD.player_rotation = angle
            
            return 0
        
        except ValueError:
            # If joystick gets disconnected
            md.UInputD.move_north = 0
            md.UInputD.move_south = 0
            md.UInputD.move_west = 0
            md.UInputD.move_east = 0
            return -1
    
    def process_joystick_buttons(self):
        """Processes joystick buttons"""
        
        arr, count = \
            glfw.get_joystick_buttons(md.UInputD.u_joystick.js_id)

        try:
            fire_button = md.UInputD.u_joystick.fire_button
            
            select_button = md.UInputD.u_joystick.select_button
            
            if md.UInputD.u_joystick.button_event(
                    'fire_button', arr[fire_button]) == \
                    uj.JoystickEvent.PRESSED:
                if md.UInputD.enable_control:
                    GameScene.shoot()
                
                else:
                    self.guis['game'].exit_game()
            
            if md.UInputD.u_joystick.button_event(
                    'select_button', arr[select_button]) == \
                    uj.JoystickEvent.PRESSED:
                exit_frame = md.GameD.active_scene.guis['game'].\
                        search_in_elements('exit_frame')
                
                if exit_frame is not None:
                    if exit_frame.visible:
                        md.GameD.active_scene.guis['game'].\
                            hide_element(exit_frame)
                    else:
                        md.GameD.active_scene.guis['game'].\
                            reveal_element(exit_frame)
            
                    self.change_control_state(not exit_frame.visible)
            
            return 0
            
        except ValueError:
            # If joystick gets disconnected
            return -1
        
    @staticmethod
    def shoot():
        """Place SHOT event into event_list"""
        
        shot = ne.NetEvent(ne.NetEventType.SHOT, [])
        
        md.GameD.event_list.append(shot)
    
    @staticmethod
    def change_control_state(state):
        """Changes if player can control the character, or not"""
        
        md.UInputD.enable_control = state
    
        if not md.UInputD.enable_control:
            md.UInputD.move_north = 0
            md.UInputD.move_south = 0
            md.UInputD.move_west = 0
            md.UInputD.move_east = 0
