"""Unnamed game main class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# External libraries
import glfw

# Packages
#   Data classes
import source.data_classes.main_data as md
#   GUI
import source.gui.gui_instances as gi
#   Utilities
import source.utilities.unnamed_joystick as uj


class UnnamedGame:
    @classmethod
    def initialize_game(cls):
        """Initializes game components"""
        
        # Setup glfw
        cls.setup_glfw()
        
        cls.resize_window(md.OpenGLD.window,
                          md.OpenGLD.window_width,
                          md.OpenGLD.window_height)
        
        # Load joystick settings
        md.UInputD.u_joystick = uj.UnnamedJoystick.load_from_file()
        
        # Load client settings
        md.GameD.client_settings = gi.GameSelectGUI.load_from_file()
        
        # Renderer initialization
        if md.GameD.active_scene.init() != 0:
            return -1
        
        # Hop into main loop
        cls.main_loop()
        
        glfw.terminate()
        
        return 0
            
    @classmethod
    def change_scene(cls, scene):
        """Changes game scene"""
        
        old_scene = md.GameD.active_scene
        
        md.GameD.active_scene = scene

        # Renderer initialization
        success = scene.init()
        
        if success != 0:
            md.GameD.active_scene = old_scene
            old_scene.init()
            return success
        
        cls.resize_window(md.OpenGLD.window,
                          md.OpenGLD.window_width,
                          md.OpenGLD.window_height)
        
        if md.UInputD.cursor_enabled:
            glfw.set_input_mode(md.OpenGLD.window, glfw.CURSOR,
                                glfw.CURSOR_NORMAL)

        else:
            glfw.set_input_mode(md.OpenGLD.window, glfw.CURSOR,
                                glfw.CURSOR_DISABLED)
        
        return 0
    
    @classmethod
    def exit_game(cls):
        """Exits game"""
        
        glfw.set_window_should_close(md.OpenGLD.window, True)
        
    @classmethod
    def main_loop(cls):
        """Main loop of graphics engine. Drawing, update."""
            
        md.GameD.update_delta_time = glfw.get_time()
        while not glfw.window_should_close(md.OpenGLD.window):
            
            # Compute time between frames
            current_frame = glfw.get_time()
            md.OpenGLD.delta_time = \
                current_frame - md.OpenGLD.last_frame
            md.OpenGLD.last_frame = current_frame
            md.OpenGLD.update_delta_time += md.OpenGLD.delta_time
            
            if md.OpenGLD.update_delta_time > md.OpenGLD.FPS_LIMIT:
                # Update current scene
                md.GameD.active_scene.update()
                
                # Render current scene
                md.GameD.active_scene.render()
                
                # FPS limiter computations
                if md.OpenGLD.FPS_LIMIT > 0.0:
                    md.OpenGLD.update_delta_time -= \
                        md.OpenGLD.FPS_LIMIT * \
                        (md.OpenGLD.update_delta_time //
                         md.OpenGLD.FPS_LIMIT)

            glfw.poll_events()
            
    @classmethod
    def setup_glfw(cls):
        """GLFW window setup"""
        
        # initialize glfw
        if glfw.init() is None:
            print('Error: Initialization of glfw failed!')
            return -1
        glfw.window_hint(glfw.RESIZABLE, False)
        md.OpenGLD.window = \
            glfw.create_window(md.OpenGLD.window_width,
                               md.OpenGLD.window_height,
                               'Unnamed Game', None, None)
        
        # create window
        if md.OpenGLD.window is None:
            print('Error: Window creation failed!')
            glfw.terminate()
            return -2
        
        # Window settings
        glfw.make_context_current(md.OpenGLD.window)
        glfw.set_window_size_callback(md.OpenGLD.window,
                                      cls.resize_window)
        glfw.set_window_focus_callback(md.OpenGLD.window,
                                       cls.process_window_focus)
        
        # Keyboard callbacks
        glfw.set_key_callback(md.OpenGLD.window, cls.process_keys)
        glfw.set_char_mods_callback(md.OpenGLD.window, cls.process_chars)
        
        # Mouse callbacks
        glfw.set_mouse_button_callback(md.OpenGLD.window,
                                       cls.process_mouse_buttons)
        
        glfw.set_cursor_pos_callback(md.OpenGLD.window,
                                     cls.process_mouse_movement)
        
        glfw.set_scroll_callback(md.OpenGLD.window,
                                 cls.process_mouse_scroll)
        
        # Joystick callback
        glfw.set_joystick_callback(cls.process_joystick_connection)
        
        if glfw.joystick_present(glfw.JOYSTICK_1):
            md.UInputD.joystick_connected = True
        
        else:
            md.UInputD.joystick_connected = False
            md.UInputD.joystick_enabled = False
        
        if md.UInputD.cursor_enabled:
            glfw.set_input_mode(md.OpenGLD.window, glfw.CURSOR,
                                glfw.CURSOR_NORMAL)
        
        else:
            glfw.set_input_mode(md.OpenGLD.window, glfw.CURSOR,
                                glfw.CURSOR_DISABLED)

        cls.resize_window(md.OpenGLD.window,
                          md.OpenGLD.window_width,
                          md.OpenGLD.window_height)
        
        glfw.set_window_aspect_ratio(md.OpenGLD.window,
                                     *md.OpenGLD.aspect_ratio)
    
    @classmethod
    def resize_window(cls, window, width, height):
        """Aligns window parameters to window resize"""

        md.OpenGLD.window_width = width
        md.OpenGLD.window_height = height
        md.OpenGLD.aspect_ratio = [md.OpenGLD.window_width,
                                   md.OpenGLD.window_height]

        if md.OpenGLD.aspect_ratio[0] == 0:
            md.OpenGLD.aspect_ratio[0] = 0.01
        
        if md.OpenGLD.aspect_ratio[1] == 0:
            md.OpenGLD.aspect_ratio[1] = 0.01
        
        if md.GameD.active_scene is not None:
            md.GameD.active_scene. \
                update_viewport(md.OpenGLD.window_width,
                                md.OpenGLD.window_height)
    
    @classmethod
    def process_keys(cls, window, key, scancode, action, mods):
        """Processes key hits"""
        
        if action == glfw.PRESS:

            md.GameD.active_scene.process_key_press(key)
        
        elif action == glfw.RELEASE:
    
            md.GameD.active_scene.process_key_release(key)
    
    @classmethod
    def process_chars(cls, window, codepoint, mods):
        """Processes character key hits"""

        md.GameD.active_scene.process_chars(chr(codepoint))
    
    @classmethod
    def process_mouse_buttons(cls, window, button, action, mods):
        """Processes mouse key hits"""
        
        if action == glfw.PRESS:
            md.GameD.active_scene.\
                process_mouse_button_press(button)
        
        elif action == glfw.RELEASE:
            md.GameD.active_scene.\
                process_mouse_button_release(button)
    
    @classmethod
    def process_mouse_movement(cls, window, pos_x, pos_y):
        """Processes mouse movement"""
        
        if md.UInputD.first_mouse_interaction:
            md.UInputD.last_x = pos_x
            md.UInputD.last_y = pos_y
            md.UInputD.first_mouse_interaction = False
        
        mouse_x_delta = pos_x - md.UInputD.last_x
        mouse_y_delta = md.UInputD.last_y - pos_y
        
        md.GameD.active_scene.\
            process_mouse_movement(mouse_x_delta,
                                   mouse_y_delta)

        md.UInputD.last_x = pos_x
        md.UInputD.last_y = pos_y
    
    @classmethod
    def process_mouse_scroll(cls, window, offset_x, offset_y):
        """Processes mouse scrolling"""

        md.GameD.active_scene.process_mouse_scroll(offset_y)
    
    @classmethod
    def process_joystick_connection(cls, jid, event):
        """Processes joystick connection state"""
        
        if event == glfw.CONNECTED:
            md.UInputD.joystick_connected = True
        
        elif event == glfw.DISCONNECTED:
            md.UInputD.joystick_connected = False
            md.UInputD.joystick_enabled = False
    
    @classmethod
    def process_window_focus(cls, window, focused):
        """Process the window's focus change"""
        
        if focused:
            md.OpenGLD.window_focus = True
        
        else:
            md.OpenGLD.window_focus = False
