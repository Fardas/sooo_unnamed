"""Network events class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import json

from enum import Enum


class NetEvent:
    def __init__(self, event_type: 'NetEventType', event_data):
        self.event_type = event_type
        self.event_data = event_data

    def to_json(self):
        """Encode the necessary data to json to send over network"""
        
        json_bytes = json.dumps(
            {'event_type': self.event_type.value,
             'event_data': self.event_data}).encode()
    
        return json_bytes


class NetEventType(Enum):
    MOVEMENT = 0
    SHOT = 1
    PLAYER_HEALTH = 2
    PLAYER_SCORE = 3
    TEAM_SCORE = 4
    NEW_PLAYER = 5
    DISC_PLAYER = 6
    TIME = 7
    MATCH = 8
    DYNAMIC_DATA = 9
