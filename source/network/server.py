"""Server class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import json
import socket
import select
import struct
import threading
import time

# Packages
#   Data classes
import source.data_classes.game_element as ge
#   Network
import source.network.net_event as ne
#   Utilities
import source.utilities.server_utilities as s_uty


# Thread class for receive data from a client
class ReadableThread(threading.Thread):
    def __init__(self, server, socket_port):
        self.server = server
        self.socket_port = socket_port
        threading.Thread.__init__(self)
    
    def run(self):
        
        while self.server.match.lock_events[self.socket_port]:
            continue
    
        self.server.match.lock_events[self.socket_port] = True
        
        connected = self.server.receive_events(self.socket_port)
        
        if connected == 0:
            if self.socket_port not in self.server.outputs:
                self.server.outputs[self.socket_port] = \
                    self.server.socket_ports[self.socket_port]
    
        else:
            self.server.removable[self.socket_port] = connected
        
        self.server.ready_connections[self.socket_port] = True
        self.server.match.lock_events[self.socket_port] = False
        

class UnnamedServer(threading.Thread):
    UPDATE_DELTA = 0.01
    
    def __init__(self, match,
                 server_address='127.0.0.1', server_port=8000):
    
        threading.Thread.__init__(self, name='server')
        
        # Stop event for stopping server thread
        self.stop = threading.Event()
        
        # Socket data
        self.server_address = (server_address, server_port)
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        # Threading
        self.r_thrs = {}
        
        # Socket - port pairs
        self.socket_ports = {}
        
        # Sockets from where data may come
        self.inputs = {server_port: self.server}
        
        # Sockets where data may be sent
        self.outputs = {}
        self.ready_connections = {}
        self.removable = {}
        
        # Match data
        self.match = match
        
        print('Server address: ', server_address)
        print('Server port: ', server_port)
        print('Map name: ', self.match.map_name)
        print('Game mode: ', self.match.game_mode)
        print('Max players: ', self.match.max_players)
        print('Match time: ', self.match.match_time)
        print('Max score: ', self.match.max_score)
    
    def init(self):
        """Initializes UnnamedServer object"""
        try:
            self.server.setblocking(False)
    
            self.server.bind(self.server_address)
            self.server.listen(5)
            success = self.match.init()
        
        except socket.gaierror:
            print('Incorrect IP address!')
            return -1
        
        except socket.error:
            print('Port is used by another process!')
            return -1
        
        if success != 0:
            return -1
        
        return 0
        
    def run(self):
        """Run server"""
        
        self.match.update_time = {'last': time.perf_counter(),
                                  'current': time.perf_counter()}
        
        while self.inputs and not self.stop.is_set():
            readable, writable, exceptional = select.select(
                self.inputs.values(), self.outputs.values(),
                self.inputs.values(), 0)
            
            for wrong_port, wrong_socket in self.socket_ports.items():
                if wrong_socket.fileno() == -1:
                    if wrong_port not in self.removable:
                        self.removable[wrong_port] = -1
            
            removable_list = []
            for port, error_code in self.removable.items():
                removable_list.append((port, error_code))
            
            for port, error_code in removable_list:
                del self.removable[port]
                self.remove_player(port, error_code)
            
            if self.match.match_ended and \
                    self.match.match_end_time + \
                    s_uty.Match.MATCH_WAIT_TIME < time.perf_counter():
                self.match.restart_match()
                for connection in self.ready_connections:
                    self.ready_connections[connection] = True
            
            # Receive
            self.process_readable(readable)
            
            # Send
            self.process_writable(writable)
            
            # Exceptions
            self.process_exceptional(exceptional)
    
    def stop_server(self):
        """Stop server thread"""

        print('Server stopped!')
        
        self.stop.set()
    
    # Select branches
    def process_readable(self, readable):
        """Processes readable list of objects"""
        
        if readable:
            self.match.update_time['last'] = \
                self.match.update_time['current']
            self.match.update_time['current'] = time.perf_counter()
            
            for readable_socket in readable:
                if readable_socket is self.server:
                    readable_port, connected = \
                        self.add_new_connection(readable_socket)
        
                    if connected != 0:
                        self.removable[readable_port] = connected
                
                else:
                    try:
                        readable_port = readable_socket.getpeername()[1]
                        
                        if readable_port not in self.removable:
                            if readable_port in self.r_thrs:
                                if not self.r_thrs[readable_port].\
                                        isAlive():
                                    r_thr = ReadableThread(
                                        self, readable_port)
                                    self.r_thrs[readable_port] = r_thr
                                    r_thr.start()
                            
                            else:
                                r_thr = ReadableThread(
                                    self, readable_port)
                                self.r_thrs[readable_port] = r_thr
                                r_thr.start()
                        
                    except OSError:
                        pass
            
            for player_port in self.match.players:
                self.process_events(player_port)

            self.match.update()
        
        if time.perf_counter() - self.match.update_time['last'] >= 0.05:
            self.match.update_time['last'] = \
                self.match.update_time['current']
            self.match.update_time['current'] = time.perf_counter()
            
            for player_port in self.match.players:
                self.process_events(player_port)

            self.match.update()
    
    def process_writable(self, writable):
        """Processes writable list of objects"""

        self.match.dynamic_update_time['delta'] += \
            time.perf_counter() - self.match.dynamic_update_time['last']
        self.match.dynamic_update_time['last'] = time.perf_counter()
        
        if writable:
            # Calculate match time in minutes and seconds
            if not self.match.match_ended:
                self.match.calculate_match_time()
            
            if self.match.dynamic_update_time['delta'] > \
                    self.UPDATE_DELTA:
                self.match.dynamic_data_event()
                self.match.dynamic_update_time['delta'] = 0.0
            
            for writable_socket in writable:
                if writable_socket.fileno() != -1:
                    writable_port = writable_socket.getpeername()[1]
                    
                    if writable_port in self.ready_connections and \
                            self.ready_connections[writable_port]:
        
                        self.ready_connections[writable_port] = False
                        
                        connected = self.send_events(
                            writable_socket, writable_port)
                        
                        if connected != 0:
                            self.removable[writable_port] = connected
                        
                    else:
                        del self.outputs[writable_port]
        
    def process_exceptional(self, exceptional):
        """Processes exceptional list of objects"""
        
        for wrong_socket in exceptional:
            wrong_port = wrong_socket.getpeername()[1]
            self.removable[wrong_port] = -2
    
    # Connection handling
    def add_new_connection(self, readable_socket):
        """Accepts new client connection and player data"""
        
        # Accept new client connection (1)
        connection, client_address = readable_socket.accept()
        connection.setblocking(True)
        
        socket_port = client_address[1]
        
        self.inputs[socket_port] = connection
        
        # Receive player data (2)
        json_string, connected = self.receive_json(connection)
        
        if connected != 0:
            return socket_port, connected
        
        try:
            json_dict = json.loads(json_string)

        except json.JSONDecodeError:
            return socket_port, -2
        
        # Validate or change name
        names = [player.name for player in self.match.players.values()]
        
        new_name = json_dict['name']
        
        i = 0
        while new_name in names:
            new_name = json_dict['name'] + '_' + str(i)
            i += 1
        
        json_dict['name'] = new_name
        
        # Check if player number reached the player limit
        if len(self.inputs) - 1 > self.match.max_players:
            json_bytes = b'SERVER FULL'
    
            self.send_json(connection, json_bytes)
            
            del self.inputs[socket_port]
            
            print('Warning:' + new_name +
                  ' tried to connect to the game, but server is full!')
            
            connection.close()
            
            return socket_port, 0
        
        # Create socket-port pair
        self.socket_ports[socket_port] = connection
        
        # Team check
        self.choose_team(json_dict)
        
        # Place player to spawn point
        self.match.players[socket_port] = \
            ge.Player(**json_dict)
        
        self.match.place_player(self.match.players[socket_port])
        
        # Send player data back (3)
        json_bytes = self.match.players[socket_port].to_json()
        
        connected = self.send_json(connection, json_bytes)
        
        if connected != 0:
            return socket_port, connected
        
        # Send map name and game_mode (4)
        json_bytes = \
            json.dumps([self.match.map_name,
                        self.match.game_mode]).encode()
        connected = self.send_json(connection, json_bytes)

        self.add_player(socket_port, self.match.players[socket_port])
        
        if connected != 0:
            return socket_port, connected
        
        return socket_port, 0
    
    def choose_team(self, json_dict):
        """Choose a team for new player"""
        
        if self.match.game_mode in ['tdm', 'ctf']:
            if json_dict['team'] == 'team_0':
            
                len_a = 0
                len_b = 0
                for player in self.match.players.values():
                    if player.team == 'team_a':
                        len_a += 1
                
                    elif player.team == 'team_b':
                        len_b += 1
            
                if len_a > len_b:
                    json_dict['team'] = 'team_b'
            
                else:
                    json_dict['team'] = 'team_a'
    
        elif self.match.game_mode == 'dm':
            json_dict['team'] = 'team_0'
    
    def add_player(self, socket_port, new_player):
        """Adds player to data containers"""
        
        print(new_player.name + ' joined the game!')
        
        # Timing
        self.match.bullet_timing[new_player] = 0.0
        
        # Score
        self.match.player_score[new_player.name] = 0
        
        # Set ready connections
        self.ready_connections[socket_port] = False
        
        # Events
        self.match.player_events[socket_port] = \
            {0: ne.NetEvent(ne.NetEventType.MOVEMENT, [0.0, 0.0, 0.0])}

        self.match.lock_events[socket_port] = False

        self.match.net_events[new_player] = []
        for player in self.match.net_events:
            if player != new_player:
                self.match.net_events[new_player].append(
                    ne.NetEvent(ne.NetEventType.NEW_PLAYER,
                                player.__dict__))
        
        self.match.net_events[self.match.players[socket_port]].\
            append(ne.NetEvent(ne.NetEventType.TIME,
                               self.match.current_match_time))

        self.match.new_player_event(new_player)

        self.match.player_score_event()
        self.match.team_score_event()

        self.match.dynamic_data_event()
    
    def remove_player(self, socket_port, error_code):
        """Remove player from server data"""
        
        if error_code == -1:
            print('Server: ' + self.match.players[socket_port].name +
                  ' lost connection due to timeout!')
        
        elif error_code == -2:
            print('Server: ' + self.match.players[socket_port].name +
                  ' lost connection due to connection error!')
        
        else:
            print('Server: ' + self.match.players[socket_port].name +
                  ' disconnected!')
        
        # Delete server data
        del self.inputs[socket_port]
        
        if socket_port in self.outputs:
            del self.outputs[socket_port]
        
        if socket_port in self.ready_connections:
            del self.ready_connections[socket_port]

        # Send events
        player_name = self.match.players[socket_port].name
        player_model = self.match.players[socket_port].player_model

        del self.match.dynamic_elements[player_model]. \
            transformations[player_name]
        
        self.match.dynamic_data_event()
        
        self.match.disc_player_event(self.match.players[socket_port])
        del self.match.player_score[player_name]
        
        self.match.player_score_event()
        
        # Delete match data
        if self.match.game_mode == 'ctf':
            if self.match.flag_holders['flag_a'] == \
                    self.match.players[socket_port]:
                self.match.flag_holders['flag_a'] = None
            
            elif self.match.flag_holders['flag_b'] == \
                    self.match.players[socket_port]:
                self.match.flag_holders['flag_b'] = None
        
        del self.match.net_events[self.match.players[socket_port]]
        del self.match.players[socket_port]
        
        self.socket_ports[socket_port].close()
        
        del self.socket_ports[socket_port]

    def process_events(self, player_port):
        """Iterates through events, and processes them by type"""
    
        while self.match.lock_events[player_port]:
            continue

        self.match.lock_events[player_port] = True
    
        player_events = self.match.player_events[player_port]
    
        movement = self.match.player_events[player_port][0]
        self.match.player_events[player_port] = {}
        self.match.player_events[player_port][0] = movement

        self.match.lock_events[player_port] = False
    
        for event_type in player_events:
            if ne.NetEventType(event_type) == ne.NetEventType.MOVEMENT:
            
                # Update player position
                event_data = player_events[event_type].event_data
                self.match.update_player_position(player_port,
                                                  event_data)
        
            elif ne.NetEventType(
                    event_type) == ne.NetEventType.SHOT:
            
                # Shot handling
                player = self.match.players[player_port]
                self.match.shoot_bullet(player)
    
    # Data exchange
    def receive_events(self, socket_port):
        """Receive events from client"""
        
        readable_socket = self.socket_ports[socket_port]
        
        try:
            received_data = readable_socket.recv(4)

            if not received_data:
                return -3
            
            event_count = struct.unpack('L', received_data)[0]
            for i in range(0, event_count):
                json_string, connected = \
                    self.receive_json(readable_socket)
        
                if connected != 0:
                    return connected
        
                json_dict = json.loads(json_string)
        
                event_type = json_dict['event_type']
        
                # MOVEMENT
                if event_type == 0:
                    if self.match.match_ended:
                        json_dict['event_data'][0] = 0
                        json_dict['event_data'][1] = 0
                    self.match.player_events[socket_port][
                        event_type] = ne.NetEvent(**json_dict)
        
                # SHOT
                elif event_type == 1:
                    if event_type in \
                            self.match.player_events[socket_port]:
                        self.match.player_events[socket_port][
                            event_type].append(ne.NetEvent(**json_dict))
                    else:
                        self.match.player_events[socket_port][
                            event_type] = [ne.NetEvent(**json_dict)]
            
            return 0
        
        except socket.timeout:
            return -1

        except socket.error:
            return -2
        
        except struct.error:
            return -2
        
        except json.JSONDecodeError:
            return -2

    def send_events(self, writable_socket, writable_port):
        """Send net events to player"""
    
        player = self.match.players[writable_port]
        try:
            writable_socket.sendall(
                struct.pack('L', len(self.match.net_events[player])))
        
            for net_event in self.match.net_events[player]:
                json_bytes = net_event.to_json()
            
                connected = self.send_json(writable_socket, json_bytes)
            
                if connected != 0:
                    return connected

            self.match.net_events[player] = []
            return 0
    
        except socket.timeout:
            return -1
    
        except socket.error:
            return -2
        
        except struct.error:
            return -2
        
        except json.JSONDecodeError:
            return -2

    # Send/receive json
    @staticmethod
    def send_json(writable_socket, json_bytes):
        """Sends json data over socket"""

        length = len(json_bytes)
        
        try:
            # Send the length of json bytes
            writable_socket.sendall(struct.pack('Q', length))
            
            # Send the json bytes
            writable_socket.sendall(json_bytes)
            
            return 0

        except socket.timeout:
            return -1

        except socket.error:
            return -2
        
        except struct.error:
            return -2
        
        except json.JSONDecodeError:
            return -2
    
    @staticmethod
    def receive_json(readable_socket):
        """Receives json data over socket, and returns it"""

        try:
            # Receive json data length
            length_packed = readable_socket.recv(8)
            
            if not length_packed:
                return '', -3
            
            length = struct.unpack('Q', length_packed)[0]
            
            # Receive json data
            iterations = length // 256
            rest = length % 256
            json_bytes = b''
            for i in range(0, iterations):
                j_b = readable_socket.recv(256)
                
                if not j_b:
                    return '', -4
                
                json_bytes += j_b
            
            j_b = readable_socket.recv(rest)
            
            if not j_b:
                return '', -4

            json_bytes += j_b
            
            json_string = json_bytes.decode()
            return json_string, 0

        except socket.timeout:
            return '', -1

        except socket.error:
            return '', -2
        
        except struct.error:
            return '', -2
        
        except json.JSONDecodeError:
            return '', -2
