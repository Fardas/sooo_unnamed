"""Network client class"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import json
import socket
import struct

# Packages
#   Data classes
import source.data_classes.game_element as ge
import source.data_classes.main_data as md
#   Network
import source.network.net_event as ne


class UnnamedClient:
    def __init__(self, server_address='127.0.0.1', server_port=8000):
        
        # Server data
        self.server_address = server_address
        self.server_port = server_port
        
        # Client data
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.setblocking(True)
        self.client.settimeout(5)
        
        self.should_close = False
        
        # Match end indicator
        self.match_ended = False
        
        # Events for processing
        self.net_events = []
        
        # Received dynamic data
        self.dynamic_ready = False
        self.dynamic_json = {}
        
    # Connection handling
    def connect_to_server(self):
        """Client tries connecting to server"""
        
        # Connect to server (1)
        try:
            self.client.connect((self.server_address, self.server_port))
            
        except socket.timeout:
            return -1
        
        except socket.error:
            return -2
    
        # Send player data to server (2)
        json_bytes = md.GameD.active_player.to_json()
        
        connected = self.send_json(json_bytes)
        
        if connected != 0:
            return connected
        
        # Receive accepted player data (3)
        json_string, connected = self.receive_json()

        if connected != 0:
            return connected

        if json_string == 'SERVER FULL':
            return -3
        
        try:
            json_dict = json.loads(json_string)
        
        except json.JSONDecodeError:
            return -2
        
        md.GameD.players[json_dict['name']] = ge.Player(**json_dict)
        md.GameD.active_player = md.GameD.players[json_dict['name']]
        
        # Receive map name and game mode (4)
        map_and_mode, connected = self.receive_json()
        try:
            map_and_mode = json.loads(map_and_mode)

        except json.JSONDecodeError:
            return -2
        
        md.GameD.map_name = map_and_mode[0]
        md.GameD.game_mode = map_and_mode[1]
        
        if connected != 0:
            return connected
        
        return 0

    # Data exchange
    def exchange_data(self):
        """Send and receive data to and from server"""
        
        connected = self.send_events()
        
        connected += self.receive_events()
        
        if connected != 0:
            return connected
        
        self.process_events()
        
        return 0

    def send_events(self):
        """Send player data in json format to server"""
        
        md.GameD.event_list.append(
            ne.NetEvent(
                ne.NetEventType.MOVEMENT,
                [md.UInputD.move_west+md.UInputD.move_east,
                 md.UInputD.move_north+md.UInputD.move_south,
                 md.UInputD.player_rotation]))
        
        try:
            self.client.sendall(
                struct.pack('L', len(md.GameD.event_list)))
            
            for pe in md.GameD.event_list:
                json_bytes = pe.to_json()
                connected = self.send_json(json_bytes)
    
                if connected != 0:
                    return connected
    
                md.GameD.event_list = []
        
            return 0
        
        except socket.timeout:
            return -1
    
        except socket.error:
            return -2
        
        except struct.error:
            return -2
        
        except json.JSONDecodeError:
            return -2
    
    def receive_events(self):
        """Receive events from server"""
        
        try:
            received_data = self.client.recv(4)
            
            if not received_data:
                return -3
            
            event_count = struct.unpack('L', received_data)[0]
            for i in range(0, event_count):
                json_string, connected = \
                    self.receive_json()
                
                if connected != 0:
                    return connected
                
                json_dict = json.loads(json_string)
                
                json_dict['event_type'] = \
                    ne.NetEventType(json_dict['event_type'])
            
                self.net_events.append(ne.NetEvent(**json_dict))
            return 0
    
        except socket.timeout:
            return -1
    
        except socket.error:
            return -2
        
        except struct.error:
            return -2
        
        except json.JSONDecodeError:
            return -2
    
    def process_events(self):
        """Iterates through events, and processes them by type"""
        
        for event in self.net_events:
            if event.event_type == ne.NetEventType.PLAYER_HEALTH:
                md.GameD.active_player.health = \
                    event.event_data
            
            elif event.event_type == ne.NetEventType.PLAYER_SCORE:
                md.GameD.player_score = event.event_data
        
            elif event.event_type == ne.NetEventType.TEAM_SCORE:
                md.GameD.team_score = event.event_data
            
            elif event.event_type == ne.NetEventType.NEW_PLAYER:
                md.GameD.players[event.event_data['name']] = \
                    ge.Player(**event.event_data)
                
            elif event.event_type == ne.NetEventType.DISC_PLAYER:
                del md.GameD.players[event.event_data]
            
            elif event.event_type == ne.NetEventType.TIME:
                md.GameD.current_match_time = \
                    [event.event_data // 60, event.event_data % 60]
            
            elif event.event_type == ne.NetEventType.MATCH:
                if event.event_data == 'MATCH_ENDED':
                    if md.GameD.game_mode == 'dm':
                        if len(md.GameD.player_score) > 1 and \
                                md.GameD.player_score[0][1] == \
                                md.GameD.player_score[1][1]:
                            message = 'Draw!'
                        
                        else:
                            winner = md.GameD.player_score[0][0]
                            message = 'Winner is ' + winner + '!'
                    
                    else:
                        if md.GameD.team_score['team_a'] > \
                                md.GameD.team_score['team_b']:
                            message = 'Winner is team_a!'
                        
                        elif md.GameD.team_score['team_a'] < \
                                md.GameD.team_score['team_b']:
                            message = 'Winner is team_b!'
                        
                        else:
                            message = 'Draw!'
                    
                    md.GameD.active_scene.guis['game'].\
                        show_match_end(message)
                
                elif event.event_data == 'MATCH_STARTED':
                    md.GameD.active_scene.guis['game'].hide_match_end()
            
            elif event.event_type == ne.NetEventType.DYNAMIC_DATA:
                if 'tr' in event.event_data:
                    element = md.GameD.\
                        dynamic_elements[event.event_data['en']]
                    
                    element.load_transformations(event.event_data['tr'])
                    
                else:
                    md.GameD.dynamic_elements[event.event_data['en']].\
                        transformations = {}

        self.net_events = []
    
    # Send/receive json
    def send_json(self, json_bytes):
        """Sends json data over socket"""
        
        length = len(json_bytes)
        try:
            # Send the length of json bytes
            self.client.sendall(struct.pack('Q', length))
            
            # Send the json bytes
            self.client.sendall(json_bytes)
            
            return 0

        except socket.timeout:
            return -1

        except socket.error:
            return -2
        
        except struct.error:
            return -2
        
        except json.JSONDecodeError:
            return -2

    def receive_json(self):
        """Receives json data over socket, and returns it"""
        
        try:
            # Receive json data length
            length_packed = self.client.recv(8)
            length = struct.unpack('Q', length_packed)[0]
            
            # Receive json data
            iterations = length // 256
            rest = length % 256
            json_bytes = b''
            for i in range(0, iterations):
                json_bytes += self.client.recv(256)
            
            json_bytes += self.client.recv(rest)
            
            json_string = json_bytes.decode()
            
            return json_string, 0
            
        except socket.timeout:
            return '', -1

        except socket.error:
            return '', -2
        
        except struct.error:
            return '', -2
        
        except json.JSONDecodeError:
            return '', -2
    
    # Close connection
    def close(self):
        """Close client socket (disconnect)"""
        
        self.client.close()
