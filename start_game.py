"""Start game"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import pathlib

# Packages
import source.control.scenes as scn
import source.data_classes.main_data as md

from source.control.unnamed_game import UnnamedGame


if __name__ == "__main__":
    md.MAIN_DIR = pathlib.Path.cwd()
    md.GameD.active_scene = scn.MainMenuScene()
    UnnamedGame.initialize_game()
