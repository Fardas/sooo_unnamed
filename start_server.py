"""Start server"""

__author__ = 'Kátai Kristóf'
__email__ = 'katai.kristof8@gmail.com'

# Standard libraries
import ast
import pathlib

# Packages
import source.data_classes.main_data as md

import source.utilities.server_utilities as sr_uty

from source.network.server import UnnamedServer


def load_server_config():
    """Loads server configurations from config file"""
    
    config_path = md.MAIN_DIR.joinpath('server.config')
    
    if not config_path.exists():
        with config_path.open('w') as server_config:
            map_name = 'map_1'
            game_mode = 'tdm'
            max_players = '10'
            match_time = '5'
            max_score = '20'
            server_address = '127.0.0.1'
            server_port = '5200'
            
            server_config.write('#Server config:\n')
            server_config.write('map_name:' + map_name + '\n')
            server_config.write('#Game modes: dm, tdm, ctf\n')
            server_config.write('game_mode:' + game_mode + '\n')
            server_config.write('#Player limit from 2 to 20\n')
            server_config.write('max_players:' + max_players + '\n')
            server_config.write('#Match time limit from 2 to 60\n')
            server_config.write('match_time:' + match_time + '\n')
            server_config.write('#Max score limit from 5 to 99\n')
            server_config.write('max_score:' + max_score + '\n')
            server_config.write('server_address:' +
                                server_address + '\n')
            server_config.write('server_port:' + server_port)
            
            server_config.flush()
    
    match_attr = {}
    server_attr = {}
    with config_path.open('r') as server_config:
        configurations = server_config.read()
        
        attr_ref = ['map_name', 'game_mode',
                    'server_address', 'server_port', 'max_players',
                    'match_time', 'max_score']
        
        for line in configurations.split('\n'):
            if '#' in line:
                continue
            
            attr_name, attr_value = line.split(':')[0:2]
            if attr_name in attr_ref:
                if attr_name == 'game_mode' and \
                        attr_value not in['dm', 'tdm', 'ctf']:
                    print('Wrong game mode name, loading default tdm')
                    continue
                
                elif attr_name in ['server_address', 'server_port']:
                    try:
                        server_attr[attr_name] = \
                            ast.literal_eval(attr_value)
                    
                    except (ValueError, SyntaxError):
                        server_attr[attr_name] = attr_value
                
                else:
                    try:
                        match_attr[attr_name] = \
                            ast.literal_eval(attr_value)
                    
                    except (ValueError, SyntaxError):
                        match_attr[attr_name] = attr_value
    
    return match_attr, server_attr


if __name__ == "__main__":
    md.MAIN_DIR = pathlib.Path.cwd()
    match_attr, server_attr = load_server_config()
    match = sr_uty.Match(**match_attr)
    unnamed_server = UnnamedServer(match, **server_attr)
    init_success = unnamed_server.init()
    
    if init_success == 0:
        stop = False
        try:
            unnamed_server.start()

            print('To exit, type "exit"!')
            while not stop:
                input_text = input()

                if input_text == 'exit':
                    stop = True

            unnamed_server.stop_server()
            
        except Exception:
            print("Couldn't start server thread!")
            stop = True
    
    else:
        print("Server couldn't initialize!")
        
        input("Press ENTER to exit!")
