from unittest import TestCase

import pathlib

import source.data_classes.game_object as go
import source.data_classes.main_data as md


class TestGameObject(TestCase):
    def setUp(self):
        self.object_path = pathlib.Path.cwd()
        self.object_path = self.object_path.parents[1]
        md.MAIN_DIR = self.object_path
        self.object_path = \
            self.object_path.joinpath('resources', 'objects')
        
    def test_load_object_good(self):
        object_path = self.object_path.joinpath('static', 'trunk.go')
        game_object, success = \
            go.GameObject.load_object(object_path, 'static')
        
        self.assertIsNotNone(game_object)
        self.assertEqual(0, success)
    
    def test_load_object_no_file(self):
        object_path = self.object_path.joinpath('no_file.go')
        game_object, success = \
            go.GameObject.load_object(object_path, 'static')
        
        self.assertIsNone(game_object)
        self.assertEqual(-1, success)

    def test_load_object_wrong_mesh(self):
        object_path = self.object_path.joinpath('wrong_mesh.go')
        game_object, success = \
            go.GameObject.load_object(object_path, 'static')
        
        self.assertIsNone(game_object)
        self.assertEqual(-2, success)
    
    def test_load_object_corrupted(self):
        object_path = self.object_path.joinpath('corrupted.go')
        
        game_object, success = \
            go.GameObject.load_object(object_path, 'static')
        
        self.assertIsNone(game_object)
        self.assertEqual(-4, success)

        object_path = self.object_path.joinpath('empty.go')
        game_object, success = \
            go.GameObject.load_object(object_path, 'static')
        
        self.assertIsNone(game_object)
        self.assertEqual(-4, success)


class TestModel(TestCase):
    def setUp(self):
        self.model_path = pathlib.Path.cwd()
        self.model_path = self.model_path.parents[1]
        self.model_path = \
            self.model_path.joinpath('resources', 'meshes')
        
    def test_load_data_good(self):
        model_path = self.model_path.joinpath('good.obj')
        model = go.Model('name', 0, model_path)
        success = model.load_data()
        
        self.assertEqual(0, success)
    
    def test_load_data_no_file(self):
        model_path = self.model_path.joinpath('no_file.obj')
        model = go.Model('name', 0, model_path)
        success = model.load_data()
        
        self.assertEqual(-1, success)

    def test_load_data_value_error(self):
        model_path = self.model_path.joinpath('value_error.obj')
        model = go.Model('name', 0, model_path)
        success = model.load_data()
        
        self.assertEqual(-2, success)
    
    def test_load_data_index_error(self):
        model_path = self.model_path.joinpath('index_error.obj')
        model = go.Model('name', 0, model_path)
        success = model.load_data()
        
        self.assertEqual(-3, success)
    
    def test_get_mesh(self):
        model = go.Model('test1', 0, 'testpath')
        model.vertices = [1.0, 1.0, 1.0]
        model.tex_coords = [1.0, 0.0]
        model.normals = [1.0, 0.0, 0.0]
        model.indices = [0]
        
        mesh, indices = model.get_mesh()
        
        self.assertListEqual(
            [1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0], mesh)

        self.assertListEqual(indices, [0])


class TestTexture(TestCase):
    def setUp(self):
        self.model_path = pathlib.Path.cwd()
        self.model_path = self.model_path.parents[1]
        self.model_path = \
            self.model_path.joinpath('resources', 'textures')
        
    def test_load_data_no_file(self):
        texture_path = self.model_path.joinpath('no_file.png')
        texture = go.Texture('test', 0, texture_path)
        
        success = texture.load_data()
        
        self.assertEqual(-1, success)
        
    def test_load_data_corrupt(self):
        texture_path = self.model_path.joinpath('wrong.png')
        texture = go.Texture('test', 0, texture_path)
        
        success = texture.load_data()
        
        self.assertEqual(-2, success)
        
    def test_load_data_good(self):
        texture_path = self.model_path.joinpath('good.png')
        texture = go.Texture('test', 0, texture_path)
        
        success = texture.load_data()
        
        self.assertEqual(0, success)
