from unittest import TestCase

import pathlib

import source.data_classes.game_element as ge
import source.data_classes.game_object as go
import source.data_classes.main_data as md
import source.utilities.utilities as uty


class TestDynamicElement(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
    
    def test_preload_elements_fail(self):
        go.GameObject.dynamic_objects = {}
        success = ge.DynamicElement.preload_elements()
        self.assertEqual(-1, success)
        
    def test_preload_elements_success(self):
        uty.DynamicLoader.load_elements()
        success = ge.DynamicElement.preload_elements()
        self.assertEqual(0, success)
        
    def test_serialize(self):
        uty.DynamicLoader.load_elements()
        ge.DynamicElement.preload_elements()
        element = md.GameD.dynamic_elements['bullet']
        json_dict = element.serialize()
        self.assertEqual({'en': 'bullet', 'tr': {}}, json_dict)
    
    def test_load_transformations(self):
        uty.DynamicLoader.load_elements()
        ge.DynamicElement.preload_elements()
        element = md.GameD.dynamic_elements['bullet']
        tr3_def1 = uty.Transformations3()
        tr3_def2 = uty.Transformations3()
        
        md.GameD.dynamic_elements['bullet'].transformations = \
            {'bullet1': tr3_def1, 'bullet2': tr3_def2}
        
        tr_dict = {'bullet1': [5.0, 10.0, 0.5],
                   'bullet2': [7.0, 14.0, 0.8]}
        
        tr3_1 = uty.Transformations3()
        tr3_1.translation.x = 5.0
        tr3_1.translation.z = 10.0
        tr3_1.rotation.y = 0.5
        tr3_2 = uty.Transformations3()
        tr3_2.translation.x = 7.0
        tr3_2.translation.z = 14.0
        tr3_2.rotation.y = 0.8
        
        element.load_transformations(tr_dict)
        
        self.addTypeEqualityFunc(uty.Vec3, vec3_equal)
        self.assertEqual(tr3_1.translation,
                         element.transformations['bullet1'].translation)
        self.assertEqual(tr3_1.rotation,
                         element.transformations['bullet1'].rotation)
        self.assertEqual(tr3_1.scale,
                         element.transformations['bullet1'].scale)
        
        self.assertEqual(tr3_2.translation,
                         element.transformations['bullet2'].translation)
        self.assertEqual(tr3_2.rotation,
                         element.transformations['bullet2'].rotation)
        self.assertEqual(tr3_2.scale,
                         element.transformations['bullet2'].scale)


class TestPlayer(TestCase):
    def setUp(self):
        self.player = ge.Player('player', 'team_0', 'male')
    
    def test_to_json(self):
        player_json = self.player.to_json()
        self.assertEqual(
            b'{"name": "player", "team": "team_0", ' +
            b'"player_model": "male", "health": 5}', player_json)
    
    def test_dec_health_dead(self):
        self.player.health = 1
        dead = self.player.dec_health()
        self.assertEqual(0, self.player.health)
        self.assertTrue(dead)
    
    def test_dec_health_live(self):
        self.player.health = 2
        dead = self.player.dec_health()
        self.assertEqual(1, self.player.health)
        self.assertFalse(dead)
    
    def test_revive(self):
        self.player.health = ge.Player.MAX_HEALTH - 1
        self.assertEqual(ge.Player.MAX_HEALTH - 1, self.player.health)
        self.player.revive()
        self.assertEqual(ge.Player.MAX_HEALTH, self.player.health)
        

def vec3_equal(first, second, msg=None):
    return type(first) == uty.Vec3 and type(first) == type(
        second) and \
           first.x == second.x and first.y == second.y and \
           first.z == second.z
