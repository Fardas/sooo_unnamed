from unittest import TestCase

import source.network.net_event as ne


class TestNetEvent(TestCase):
    def setUp(self):
        self.shot_event = \
            ne.NetEvent(ne.NetEventType.SHOT, ['somet'])
        self.time_event = \
            ne.NetEvent(ne.NetEventType.TIME, [11, 33])
        
    def test_to_json(self):
        shot_json = self.shot_event.to_json()
        time_json = self.time_event.to_json()
        
        self.assertEqual(b'{"event_type": 1, "event_data": ["somet"]}',
                         shot_json)
        self.assertEqual(b'{"event_type": 7, "event_data": [11, 33]}',
                         time_json)
