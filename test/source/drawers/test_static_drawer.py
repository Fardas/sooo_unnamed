from unittest import TestCase

import pathlib

import source.drawers.static_drawer as sd
import source.data_classes.main_data as md
import source.control.scenes as sc


class TestStaticDrawer(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[1]
        self.static_drawer = sd.StaticDrawer()
    
    def test_init_no_map(self):
        success = self.static_drawer.init()
        self.assertEqual(-1, success)
    
    def test_init_wrong_map(self):
        self.static_drawer.map = 'corrupted'
        success = self.static_drawer.init()
        self.assertEqual(-1, success)
    
    def test_init_wrong_scene(self):
        self.static_drawer.map = 'correct'
        md.GameD.active_scene = sc.MainMenuScene()
        success = self.static_drawer.init()
        self.assertEqual(-1, success)
