from unittest import TestCase

import pathlib

import source.drawers.dynamic_drawer as dd
import source.data_classes.main_data as md
import source.control.scenes as sc


class TestDynamicDrawer(TestCase):
    def setUp(self):
        self.dynamic_drawer = dd.DynamicDrawer()
    
    def test_init_wrong_scene(self):
        md.GameD.active_scene = sc.MainMenuScene()
        
        success = self.dynamic_drawer.init()
        
        self.assertEqual(-1, success)
