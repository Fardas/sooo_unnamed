from unittest import TestCase

import pathlib
import glfw

import source.data_classes.main_data as md
import source.drawers.ung_drawer as ud


class TestUNGDrawer(TestCase):
    def setUp(self):
        if glfw.init() is None:
            print('Error: Initialization of glfw failed!')
            return -1
        
        glfw.window_hint(glfw.RESIZABLE, False)
        md.OpenGLD.window = \
            glfw.create_window(md.OpenGLD.window_width,
                               md.OpenGLD.window_height,
                               'Unnamed Game', None, None)

        # create window
        if md.OpenGLD.window is None:
            print('Error: Window creation failed!')
            glfw.terminate()
            return -2

        # Window settings
        glfw.make_context_current(md.OpenGLD.window)
        
        self.ung_drawer = ud.UNGDrawer()
        self.ung_drawer.guis = []
    
    def test_init_good_shaders(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
    
        self.shader_path = \
            md.MAIN_DIR.joinpath('source', 'shaders')
        
        success = self.ung_drawer.init()
        
        self.assertEqual(0, success)
    
    def test_init_wrong_shaders(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[1]
        
        success = self.ung_drawer.init()
        
        self.assertEqual(-1, success)
