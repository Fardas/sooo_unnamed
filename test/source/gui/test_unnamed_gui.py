from unittest import TestCase

import pathlib

import source.gui.gui_instances as gi
import source.data_classes.main_data as md
import source.gui.unnamed_gui as ung


class TestUNG(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
        self.gui = ung.UNG(name='disconnected')
        md.GameD.active_scene = DummyScene()
        
    def test_init_success(self):
        success = self.gui.init()
        
        self.assertEqual(0, success)
        
    def test_init_fail(self):
        wrong_gui = ung.UNG(name='wrong')
        success = wrong_gui.init()
        
        self.assertEqual(-1, success)
    
    def test_create_element(self):
        parameters = {'name': 'button', 'size': [0.5, 0.5]}
        class_name = 'UNGButton'
        
        element = self.gui.create_element(class_name, parameters)
        
        self.assertEqual('button', element.name)
        self.assertListEqual([0.5, 0.5], element.size)
        self.assertListEqual([0, 0], element.position)
    
    def test_change_enabled_state(self):
        self.gui.change_enabled_state(False)
        self.assertFalse(self.gui.enabled)
        
        self.gui.change_enabled_state(True)
        self.assertTrue(self.gui.enabled)
    
    def test_reveal_element(self):
        self.gui.init()
        element = self.gui.search_in_elements('error_textfield')
        self.gui.reveal_element(element)
        
        self.assertTrue(element.visible)
    
    def test_hide_element(self):
        self.gui.init()
        element = self.gui.search_in_elements('error_textfield')
        self.gui.hide_element(element)
        
        self.assertFalse(element.visible)


class DummyDrawer:
    def __init__(self):
        pass
    
    def reinit(self):
        pass


class DummyScene:
    def __init__(self):
        self.guis = {'main_menu': gi.MainMenuGUI(),
                     'game_select': gi.GameSelectGUI(),
                     'game': gi.GameGUI(),
                     'disconnected': gi.DisconnectedGUI(),
                     'joystick_settings': gi.JoystickSettingsGUI()}
        
        self.active_gui_name = None
        
        self.ung_drawer = DummyDrawer()
        
        for gui in self.guis.values():
            gui.init()
    
    def init(self):
        if self.success:
            return 0
        else:
            return -1
    
    @classmethod
    def update_viewport(cls, width, height):
        pass