from unittest import TestCase

import pathlib
import glfw

import source.gui.ung_elements as ue
import source.data_classes.game_element as ge
import source.gui.gui_instances as gi
import source.data_classes.main_data as md
import source.utilities.unnamed_joystick as uj

md.MAIN_DIR = pathlib.Path.cwd()
md.MAIN_DIR = md.MAIN_DIR.parents[2]


class TestGameSelectGUI(TestCase):
    def setUp(self):
        md.GameD.active_scene = DummyScene()
        md.GameD.active_scene.active_gui_name = 'game_select'
        self.gs_gui = md.GameD.active_scene.guis['game_select']

        if glfw.init() is None:
            return -1

        glfw.window_hint(glfw.RESIZABLE, False)
        md.OpenGLD.window = \
            glfw.create_window(md.OpenGLD.window_width,
                               md.OpenGLD.window_height,
                               'Unnamed Game', None, None)

        # create window
        if md.OpenGLD.window is None:
            glfw.terminate()
            return -2

        # Window settings
        glfw.make_context_current(md.OpenGLD.window)
        
    def test_change_team_no_button(self):
        del self.gs_gui.all_elements['player_team']
        success = self.gs_gui.change_team()
        self.assertEqual(-1, success)
    
    def test_change_team_correct(self):
        success = self.gs_gui.change_team()
        self.assertEqual(0, success)
    
    def test_change_player_model_no_button(self):
        del self.gs_gui.all_elements['player_model']
        success = self.gs_gui.change_player_model()
        self.assertEqual(-1, success)
    
    def test_change_player_model_correct(self):
        success = self.gs_gui.change_player_model()
        self.assertEqual(0, success)
    
    def test_switch_joystick_no_button(self):
        del self.gs_gui.all_elements['joystick_switch']
        success = self.gs_gui.switch_joystick()
        self.assertEqual(-1, success)
    
    def test_switch_joystick_correct(self):
        success = self.gs_gui.switch_joystick()
        self.assertEqual(0, success)
    
    def test_main_menu_no_textfield(self):
        md.GameD.client_settings = gi.GameSelectGUI.load_from_file()
        del self.gs_gui.all_elements['error_textfield']
        success = self.gs_gui.main_menu()
        self.assertEqual(-1, success)
    
    def test_main_menu_correct(self):
        md.GameD.client_settings = gi.GameSelectGUI.load_from_file()
        success = self.gs_gui.main_menu()
        self.assertEqual(0, success)

    def test_load_settings_no_settings(self):
        md.GameD.client_settings = {}
        success = self.gs_gui.load_settings()
        self.assertEqual(-2, success)
    
    def test_load_settings_no_frame(self):
        md.GameD.client_settings = gi.GameSelectGUI.load_from_file()
        del self.gs_gui.all_elements['options_frame']
        success = self.gs_gui.load_settings()
        self.assertEqual(-1, success)
    
    def test_load_settings_correct(self):
        md.GameD.client_settings = gi.GameSelectGUI.load_from_file()
        success = self.gs_gui.load_settings()
        self.assertEqual(0, success)

    def test_check_if_correct_true(self):
        a_d = {'int': 32, 'float': 6.78, 'bool': False, 'str': 'str1',
               'str2': 'str2'}
        a_d2 = {'int': 32, 'float': 6.78, 'bool': False, 'str': 'str0',
                'str2': 'str7'}
        r_d = {'int': 1, 'float': 1.1, 'bool': True, 'str': 'something',
               'str2': 'somethings'}
        e_s = {'str': ['str1', 'str0'], 'str2': ['str2', 'str7']}
        correct = self.gs_gui.check_if_correct(a_d, r_d, e_s)
        self.assertTrue(correct)
        correct = self.gs_gui.check_if_correct(a_d2, r_d, e_s)
        self.assertTrue(correct)

    def test_check_if_correct_false(self):
        a_d = {'int': 32, 'float': 6.78, 'bool': False, 'str': 'str1',
               'str2': 'str0'}
        a_d2 = {'int': 32, 'float': 6, 'bool': False, 'str': 'str0',
                'str2': 'str7'}
        r_d = {'int': 1, 'float': 1.1, 'bool': True, 'str': 'something',
               'str2': 'somethings'}
        e_s = {'str': ['str1', 'str0'], 'str2': ['str2', 'str7']}
        correct = self.gs_gui.check_if_correct(a_d, r_d, e_s)
        self.assertFalse(correct)
        correct = self.gs_gui.check_if_correct(a_d2, r_d, e_s)
        self.assertFalse(correct)
    
    def test_save_to_file(self):
        attr_dict = {
            'server_address': '127.0.0.1',
            'server_port': 5200,
            'player_name': 'correct',
            'player_team': 'team_0',
            'player_model': 'male',
            'joystick_switch': True,
            'fullscreen': False}
        self.gs_gui.save_to_file(attr_dict)

        player_name = \
            gi.GameSelectGUI.load_from_file()['player_name']
        
        self.assertEqual('correct', player_name)
    
    def test_reset_config_file(self):
        attr_dict = {
            'server_address': '127.0.0.1',
            'server_port': 5200,
            'player_name': 'correct',
            'player_team': 'team_0',
            'player_model': 'male',
            'joystick_switch': True,
            'fullscreen': False}
        self.gs_gui.save_to_file(attr_dict)

        new_player_name = \
            gi.GameSelectGUI.load_from_file()['player_name']
        
        self.gs_gui.reset_config_file()

        player_name = gi.GameSelectGUI.load_from_file()['player_name']
        
        self.assertNotEqual(new_player_name, player_name)


class TestJoystickSettingsGUI(TestCase):
    def setUp(self):
        md.GameD.active_scene = DummyScene()
        md.GameD.active_scene.active_gui_name = 'joystick_settings'
        self.js_gui = md.GameD.active_scene.guis['joystick_settings']
    
    def test_switch_inversion_correct(self):
        button = ue.UNGButton(name='bttn', size=[1.0, 1.0])
        button.text = ue.UNGText(value='+', container=button)
        self.js_gui.all_elements['bttn'] = button
        
        self.js_gui.switch_inversion(['bttn'])
        self.assertEqual('-', button.text.value)
        self.js_gui.switch_inversion(['bttn'])
        self.assertEqual('+', button.text.value)
    
    def test_switch_inversion_no_button(self):
        success = self.js_gui.switch_inversion(['bttn'])
        self.assertEqual(-1, success)
    
    def test_switch_inversion_no_arg(self):
        success = self.js_gui.switch_inversion([])
        self.assertEqual(-1, success)

    def test_load_settings_no_settings(self):
        md.UInputD.u_joystick = None
        success = self.js_gui.load_settings()
        self.assertEqual(-2, success)
    
    def test_load_settings_no_frame(self):
        md.UInputD.u_joystick = uj.UnnamedJoystick.load_from_file()
        del self.js_gui.all_elements['joystick_frame']
        success = self.js_gui.load_settings()
        self.assertEqual(-1, success)
    
    def test_load_settings_correct(self):
        md.UInputD.u_joystick = uj.UnnamedJoystick.load_from_file()
        success = self.js_gui.load_settings()
        self.assertEqual(0, success)

    def test_save_settings_no_settings(self):
        md.UInputD.u_joystick = None
        success = self.js_gui.save_settings()
        self.assertEqual(-2, success)
    
    def test_save_settings_no_frame(self):
        md.UInputD.u_joystick = uj.UnnamedJoystick.load_from_file()
        del self.js_gui.all_elements['joystick_frame']
        success = self.js_gui.save_settings()
        self.assertEqual(-1, success)
    
    def test_save_settings_correct(self):
        md.UInputD.u_joystick = uj.UnnamedJoystick.load_from_file()
        success = self.js_gui.save_settings()
        self.assertEqual(0, success)


class TestDisconnectedGUI(TestCase):
    def setUp(self):
        md.GameD.active_scene = DummyScene()
        md.GameD.active_scene.active_gui_name = 'disconnected'
        self.disc_gui = md.GameD.active_scene.guis['disconnected']
    
    def test_change_text_correct(self):
        self.disc_gui.change_text(-1)
        error_text = \
            self.disc_gui.all_elements['error_textfield'].text.value
        self.assertEqual('DISCONNECTED: TIMEOUT', error_text)
        self.disc_gui.change_text(-2)
        error_text = \
            self.disc_gui.all_elements['error_textfield'].text.value
        self.assertEqual('DISCONNECTED: CONNECTION ERROR', error_text)
    
    def test_change_text_no_textfield(self):
        del self.disc_gui.all_elements['error_textfield']
        success = self.disc_gui.change_text(-1)
        self.assertEqual(-1, success)


class TestGameGUI(TestCase):
    def setUp(self):
        md.GameD.active_player = ge.Player('name', 'team_0', 'male')
        md.GameD.active_scene = DummyScene()
        md.GameD.active_scene.active_gui_name = 'game'
        self.game_gui = md.GameD.active_scene.guis['game']
    
    def test_refresh_stats_no_players(self):
        self.game_gui.player_elements = {'this': None}
        del self.game_gui.all_elements['players_frame']
        success = self.game_gui.refresh_stats()
        self.assertEqual(-1, success)
        
    def test_refresh_stats_no_time(self):
        del self.game_gui.all_elements['time_text']
        success = self.game_gui.refresh_stats()
        self.assertEqual(-2, success)
        
    def test_refresh_stats_no_score(self):
        del self.game_gui.all_elements['score_text']
        success = self.game_gui.refresh_stats()
        self.assertEqual(-3, success)
        
    def test_refresh_stats_no_health(self):
        del self.game_gui.all_elements['health_bar']
        success = self.game_gui.refresh_stats()
        self.assertEqual(-4, success)
    
    def test_show_match_end_no_textfield(self):
        del self.game_gui.all_elements['match_end_text']
        success = self.game_gui.show_match_end('msg')
        self.assertEqual(-1, success)
    
    def test_show_match_end_correct(self):
        self.game_gui.show_match_end('msg')
        visible = self.game_gui.all_elements['match_end_text'].visible
        self.assertTrue(visible)
    
    def test_hide_match_end_no_textfield(self):
        del self.game_gui.all_elements['match_end_text']
        success = self.game_gui.hide_match_end()
        self.assertEqual(-1, success)
    
    def test_hide_match_end_correct(self):
        self.game_gui.hide_match_end()
        visible = self.game_gui.all_elements['match_end_text'].visible
        self.assertFalse(visible)


class DummyDrawer:
    def __init__(self):
        pass
    
    def reinit(self):
        pass


class DummyScene:
    def __init__(self):
        self.guis = {'main_menu': gi.MainMenuGUI(),
                     'game_select': gi.GameSelectGUI(),
                     'game': gi.GameGUI(),
                     'disconnected': gi.DisconnectedGUI(),
                     'joystick_settings': gi.JoystickSettingsGUI()}
        
        self.active_gui_name = None
        
        self.ung_drawer = DummyDrawer()
        
        for gui in self.guis.values():
            gui.init()
