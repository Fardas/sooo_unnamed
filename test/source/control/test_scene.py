from unittest import TestCase

import pathlib

import source.network.net_event as ne
import source.control.unnamed_game as ug
import source.data_classes.main_data as md
import source.control.scenes as sc


class TestMainMenuScene(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
        
        ug.UnnamedGame.setup_glfw()
        self.mm_sc = sc.MainMenuScene()
        
    def test_init_good(self):
        success = self.mm_sc.init()
        
        self.assertEqual(0, success)
        
    def test_init_super_init_fail(self):
        self.mm_sc.guis['main_menu'].name = 'main_no_menu'
        
        success = self.mm_sc.init()
        
        self.assertEqual(-1, success)
    
    def test_init_ung_drawer_fail(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[1]
        
        success = self.mm_sc.init()
        
        self.assertEqual(-2, success)


class TestGameScene(TestCase):
    def setUp(self):
        ug.UnnamedGame.setup_glfw()
        self.gs_sc = sc.GameScene('', 1, '', '', '')
        md.GameD.active_scene = self.gs_sc
        
    def test_init_super_init_fail(self):
        self.gs_sc.guis['game'].name = 'gameno'
        
        success = self.gs_sc.init()
        
        self.assertEqual(-1, success)
    
    def test_init_ung_drawer_fail(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[1]
        
        success = self.gs_sc.init()
        
        self.assertEqual(-2, success)
        
    def test_init_static_drawer_fail(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
        md.GameD.map_name = 'm'
        
        success = self.gs_sc.init()
        
        self.assertEqual(-3, success)
    
    def test_shoot(self):
        sc.GameScene.shoot()
        
        self.assertEqual(ne.NetEventType.SHOT,
                         md.GameD.event_list[-1].event_type)
    
    def test_change_control_state(self):
        sc.GameScene.change_control_state(True)
        self.assertTrue(md.UInputD.enable_control)
        
        sc.GameScene.change_control_state(False)
        self.assertFalse(md.UInputD.enable_control)
