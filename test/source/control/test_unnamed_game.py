from unittest import TestCase

import glfw
import pathlib

import source.gui.gui_instances as gi
import source.data_classes.main_data as md
import source.control.unnamed_game as ug


class TestUnnamedGame(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
        self.unnamed_game = ug.UnnamedGame()
        self.unnamed_game.setup_glfw()
        md.GameD.active_scene = DummyScene(False)
    
    def test_initialize_game_fail(self):
        success = self.unnamed_game.initialize_game()
        
        self.assertEqual(-1, success)
    
    def test_change_scene_success(self):
        scene = DummyScene(True)
        
        success = self.unnamed_game.change_scene(scene)
        
        self.assertEqual(0, success)
    
    def test_change_scene_fail(self):
        scene = DummyScene(False)
        
        success = self.unnamed_game.change_scene(scene)
        
        self.assertEqual(-1, success)
    
    def test_exit_game(self):
        self.assertFalse(glfw.window_should_close(md.OpenGLD.window))
        
        self.unnamed_game.exit_game()
        
        self.assertTrue(glfw.window_should_close(md.OpenGLD.window))


class DummyDrawer:
    def __init__(self):
        pass
    
    def reinit(self):
        pass


class DummyScene:
    def __init__(self, success):
        self.guis = {'main_menu': gi.MainMenuGUI(),
                     'game_select': gi.GameSelectGUI(),
                     'game': gi.GameGUI(),
                     'disconnected': gi.DisconnectedGUI(),
                     'joystick_settings': gi.JoystickSettingsGUI()}
        
        self.success = success
        
        self.active_gui_name = None
        
        self.ung_drawer = DummyDrawer()
        
        for gui in self.guis.values():
            gui.init()
    
    def init(self):
        if self.success:
            return 0
        else:
            return -1
    
    @classmethod
    def update_viewport(cls, width, height):
        pass
