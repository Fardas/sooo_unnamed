/*
custom vertex shader
author = Katai Kristof
*/
#version 450
layout(location = 0) in vec3 vertPosition;
layout(location = 1) in vec2 vertTexCoords;
layout(location = 2) in vec3 vertNormal;
layout(location = 3) in uint drawID;

struct transformations
{
    vec4 translate;
    vec4 rotate;
    vec4 scale;
};

layout(std430, binding = 1) buffer ssbo
{
	transformations trans[];
};

layout(location = 0) out vec3 fragPosition;
layout(location = 1) out vec2 fragTexCoords;
layout(location = 2) out vec3 fragNormal;
layout(location = 3) flat out uint textureIndex;

uniform mat4 transform;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	mat4 translate =
		mat4(1.0f, 0.0f, 0.0f, trans[drawID].translate.x,
	         0.0f, 1.0f, 0.0f, trans[drawID].translate.y,
	         0.0f, 0.0f, 1.0f, trans[drawID].translate.z,
	         0.0f, 0.0f, 0.0f, 1.0f);

	float sx = sin(trans[drawID].rotate.x);
	float sy = sin(trans[drawID].rotate.y);
	float sz = sin(trans[drawID].rotate.z);

	float cx = cos(trans[drawID].rotate.x);
	float cy = cos(trans[drawID].rotate.y);
	float cz = cos(trans[drawID].rotate.z);

	mat4 rotate =
		mat4(cy * cz, -sz * cy, sy, 0.0f,
	         sx * sy * cz + sz * cx, -sx * sy * sz + cx * cz, -sx * cy, 0.0f,
	         -sy * cx * cz + sz * sx, sy * sz * cx + sx * cz, cx * cy, 0.0f,
	         0.0f, 0.0f, 0.0f, 1.0f);

	mat4 scale =
		mat4(trans[drawID].scale.x, 0.0f, 0.0f, 0.0f,
	         0.0f, trans[drawID].scale.y, 0.0f, 0.0f,
	         0.0f, 0.0f, trans[drawID].scale.z, 0.0f,
	         0.0f, 0.0f, 0.0f, 1.0f);

	mat4 mvp = projection * view * model;
	mat4 object_trans = rotate * scale * translate;

	fragNormal = (vec4(vertNormal, 0.0f) * object_trans).xyz;
	gl_Position = mvp * (vec4(vertPosition, 1.0f) * object_trans);
	fragPosition = (vec4(vertPosition, 1.0f) * object_trans).xyz;
	fragTexCoords = vertTexCoords;
	textureIndex = uint(trans[drawID].rotate.w);
}
