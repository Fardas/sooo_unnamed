from unittest import TestCase

import pathlib

import source.utilities.server_utilities as s_uty

import source.data_classes.main_data as md


class TestMatch(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[1]
        self.match = s_uty.Match()
        
    def test_init_success(self):
        self.match.map_name = 'correct'
        self.match.game_mode = 'tdm'
        
        success = self.match.init()
        
        self.assertEqual(0, success)
        
    def test_init_dm_fail(self):
        self.match.map_name = 'no_dm'
        self.match.game_mode = 'dm'
        
        success = self.match.init()
        
        self.assertEqual(-1, success)
        
    def test_init_tdm_fail(self):
        self.match.map_name = 'no_tdm'
        self.match.game_mode = 'dm'
        success = self.match.init()
        
        self.assertEqual(0, success)
        
        self.match.game_mode = 'tdm'
        success = self.match.init()
        
        self.assertEqual(-1, success)
        
    def test_init_ctf_fail(self):
        self.match.map_name = 'no_ctf'
        self.match.game_mode = 'dm'
        success = self.match.init()
        
        self.assertEqual(0, success)
        
        self.match.game_mode = 'tdm'
        success = self.match.init()
        
        self.assertEqual(0, success)
        
        self.match.game_mode = 'ctf'
        success = self.match.init()
        
        self.assertEqual(-1, success)
    
    def test_load_static_elements_success(self):
        self.match.map_name = 'correct'
        success = self.match.load_static_elements()
        
        self.assertEqual(0, success)
    
    def test_load_static_elements_no_map(self):
        self.match.map_name = 'no_map'
        success = self.match.load_static_elements()
        
        self.assertEqual(-7, success)
    
    def test_load_static_elements_json_fail(self):
        self.match.map_name = 'corrupted'
        success = self.match.load_static_elements()
        
        self.assertEqual(-2, success)
    
    def test_load_static_elements_wrong_object(self):
        self.match.map_name = 'wrong_object'
        success = self.match.load_static_elements()
        
        self.assertEqual(-3, success)
    
    def test_load_static_elements_game_mode_fail(self):
        self.match.map_name = 'no_dm'
        self.match.game_mode = 'tdm'
        success = self.match.load_static_elements()
        
        self.assertEqual(-4, success)
    
    def test_load_object_success(self):
        object_path = md.MAIN_DIR.joinpath(
            'resources', 'objects', 'static', 'trunk.go')
        o, success = self.match.load_object(object_path, 'static')
        
        self.assertEqual(0, success)
    
    def test_load_object_no_file(self):
        object_path = md.MAIN_DIR.joinpath(
            'resources', 'objects', 'no_file.go')
        o, success = self.match.load_object(object_path, 'static')
        
        self.assertEqual(-1, success)
    
    def test_load_object_wrong_mesh(self):
        object_path = md.MAIN_DIR.joinpath(
            'resources', 'objects', 'wrong_mesh.go')
        o, success = self.match.load_object(object_path, 'static')
        
        self.assertEqual(-2, success)
    
    def test_load_object_corrupted(self):
        object_path = md.MAIN_DIR.joinpath(
            'resources', 'objects', 'corrupted.go')
        o, success = self.match.load_object(object_path, 'static')
        
        self.assertEqual(-4, success)
        
