from unittest import TestCase

import ctypes
import pathlib

import numpy

import source.data_classes.main_data as md

import source.data_classes.game_object as go
import source.data_classes.game_element as ge

import source.utilities.utilities as uty


class TestDynamicLoader(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[1]
    
    def test_generate_arrays(self):
        model = go.Model('test1', 0, 'testpath')
        model.vertices = [1.0, 1.0, 1.0]
        model.tex_coords = [1.0, 0.0]
        model.normals = [1.0, 0.0, 0.0]
        model.indices = [0]
        
        game_object = go.GameObject('test1', 0, model, None,
                                    [0.0, 0.0], None, False)
        
        md.GameD.dynamic_elements['test'] = \
            ge.DynamicElement('test', 0, game_object, {})

        uty.DynamicLoader.generate_arrays()
        
        self.assertListEqual(
            uty.DynamicLoader.mesh_array,
            [1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0])
        
        self.assertListEqual(uty.DynamicLoader.index_array, [0])
    
    def test_get_arrays(self):
        uty.DynamicLoader.mesh_array = [1.0, 0.0, 2.0]
        uty.DynamicLoader.index_array = [1, 2, 3]
        
        m_arr, i_arr = uty.DynamicLoader.get_arrays()
        
        numpy.testing.assert_array_equal(
            numpy.array([1.0, 0.0, 2.0], ctypes.c_float), m_arr)
        numpy.testing.assert_array_equal(
            numpy.array([1, 2, 3], ctypes.c_uint32), i_arr)


class TestStaticLoader(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[1]
        uty.StaticLoader.map_name = 'correct'
    
    def test_load_elements_no_map(self):
        uty.StaticLoader.map_name = 'no_map'
        self.assertEqual(uty.StaticLoader.load_elements(), -1)
    
    def test_load_elements_corrupted_map(self):
        uty.StaticLoader.map_name = 'corrupted'
        self.assertEqual(uty.StaticLoader.load_elements(), -2)
    
    def test_load_elements_empty_map(self):
        uty.StaticLoader.map_name = 'empty'
        self.assertEqual(uty.StaticLoader.load_elements(), -2)
    
    def test_load_elements_wrong_object(self):
        uty.StaticLoader.map_name = 'wrong_object'
        self.assertEqual(uty.StaticLoader.load_elements(), -3)
    
    def test_load_elements_game_mode_mismatch(self):
        md.GameD.game_mode = 'ctf'
        uty.StaticLoader.map_name = 'no_ctf'
        self.assertEqual(uty.StaticLoader.load_elements(), -4)
        md.GameD.game_mode = 'tdm'
        uty.StaticLoader.map_name = 'no_tdm'
        self.assertEqual(uty.StaticLoader.load_elements(), -4)
        md.GameD.game_mode = 'ctf'
        uty.StaticLoader.map_name = 'no_dm'
        self.assertEqual(uty.StaticLoader.load_elements(), -4)
    
    def test_generate_arrays(self):
        model = go.Model('test1', 0, 'testpath')
        model.vertices = [1.0, 1.0, 1.0]
        model.tex_coords = [1.0, 0.0]
        model.normals = [1.0, 0.0, 0.0]
        model.indices = [0]
        
        game_object = go.GameObject('test1', 0, model, None,
                                    [0.0, 0.0], None, False)
        
        md.GameD.static_elements['test'] = \
            ge.StaticElement('test', 0, game_object, [])

        uty.StaticLoader.generate_arrays()
        
        self.assertListEqual(
            uty.StaticLoader.mesh_array, [1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0])
        
        self.assertListEqual(uty.StaticLoader.index_array, [0])
    
    def test_get_arrays(self):
        uty.StaticLoader.mesh_array = [1.0, 0.0, 2.0]
        uty.StaticLoader.index_array = [1, 2, 3]
        
        m_arr, i_arr = uty.StaticLoader.get_arrays()
        
        numpy.testing.assert_array_equal(
            numpy.array([1.0, 0.0, 2.0], ctypes.c_float), m_arr)
        numpy.testing.assert_array_equal(
            numpy.array([1, 2, 3], ctypes.c_uint32), i_arr)


class TestVec3(TestCase):
    def setUp(self):
        self.vec3 = uty.Vec3([1.0, 2.0, 3.0])
    
    def test_init_no_parameters(self):
        self.vec3_no = uty.Vec3()
        self.assertAlmostEqual(self.vec3_no.x, 0.0)
        self.assertAlmostEqual(self.vec3_no.y, 0.0)
        self.assertAlmostEqual(self.vec3_no.z, 0.0)
    
    def test_init_with_parameters(self):
        self.assertAlmostEqual(self.vec3.x, 1.0)
        self.assertAlmostEqual(self.vec3.y, 2.0)
        self.assertAlmostEqual(self.vec3.z, 3.0)
    
    def test_init_wrong_length_parameters(self):
        with self.assertRaises(IndexError) as r:
            uty.Vec3([1.0])
    
    def test_to_list(self):
        vec3_list = self.vec3.to_list()
        self.assertAlmostEqual(vec3_list, [1.0, 2.0, 3.0])


class TestVec4(TestCase):
    def setUp(self):
        self.vec4 = uty.Vec4([1.0, 2.0, 3.0, 4.0])
    
    def test_no_parameters(self):
        self.vec4_no = uty.Vec4()
        self.assertAlmostEqual(self.vec4_no.x, 0.0)
        self.assertAlmostEqual(self.vec4_no.y, 0.0)
        self.assertAlmostEqual(self.vec4_no.z, 0.0)
        self.assertAlmostEqual(self.vec4_no.w, 0.0)
    
    def test_with_parameters(self):
        self.assertAlmostEqual(self.vec4.x, 1.0)
        self.assertAlmostEqual(self.vec4.y, 2.0)
        self.assertAlmostEqual(self.vec4.z, 3.0)
        self.assertAlmostEqual(self.vec4.w, 4.0)
    
    def test_wrong_length_parameters(self):
        with self.assertRaises(IndexError) as r:
            uty.Vec4([1.0])
    
    def test_to_list(self):
        vec4_list = self.vec4.to_list()
        self.assertAlmostEqual(vec4_list, [1.0, 2.0, 3.0, 4.0])


class TestTransformations3(TestCase):
    def setUp(self):
        self.tr3 = uty.Transformations3([1.0, 2.0, 3.0],
                                        [2.0, 3.0, 1.0],
                                        [3.0, 1.0, 2.0])
    
    def test_init_no_parameters(self):
        self.tr3_no = uty.Transformations3()
        self.addTypeEqualityFunc(uty.Vec3, self.vec3_equal)
        self.assertEqual(
            self.tr3_no.translation, uty.Vec3([0.0, 0.0, 0.0]))
        self.assertEqual(
            self.tr3_no.rotation, uty.Vec3([0.0, 0.0, 0.0]))
        self.assertEqual(
            self.tr3_no.scale, uty.Vec3([1.0, 1.0, 1.0]))
    
    def test_init_with_parameters(self):
        self.addTypeEqualityFunc(uty.Vec3, self.vec3_equal)
        self.assertEqual(self.tr3.translation, uty.Vec3([1.0, 2.0, 3.0]))
        self.assertEqual(self.tr3.rotation, uty.Vec3([2.0, 3.0, 1.0]))
        self.assertEqual(self.tr3.scale, uty.Vec3([3.0, 1.0, 2.0]))
    
    @staticmethod
    def vec3_equal(first, second, msg=None):
        return type(first) == uty.Vec3 and type(first) == type(second) and \
               first.x == second.x and first.y == second.y and \
               first.z == second.z
    
    def test_init_wrong_length_parameters(self):
        with self.assertRaises(IndexError) as r:
            uty.Transformations3([1.0])
    
    def test_serialize(self):
        ser_list = self.tr3.serialize()
        self.assertAlmostEqual(ser_list[0], 1.0)
        self.assertAlmostEqual(ser_list[1], 3.0)
        self.assertAlmostEqual(ser_list[2], 3.0)
    
    def test_to_list(self):
        tr3_list = self.tr3.to_list()
        self.assertEqual(tr3_list[0], [1.0, 2.0, 3.0])
        self.assertEqual(tr3_list[1], [2.0, 3.0, 1.0])
        self.assertEqual(tr3_list[2], [3.0, 1.0, 2.0])


class TestTransformations4(TestCase):
    def setUp(self):
        self.tr4 = uty.Transformations4([1.0, 2.0, 3.0, 4.0],
                                        [2.0, 3.0, 4.0, 1.0],
                                        [3.0, 4.0, 1.0, 2.0])
    
    def test_init_no_parameters(self):
        self.tr4_no = uty.Transformations4()
        self.addTypeEqualityFunc(uty.Vec4, self.vec4_equal)
        self.assertEqual(self.tr4_no.translation,
                         uty.Vec4([0.0, 0.0, 0.0, 0.0]))
        self.assertEqual(self.tr4_no.rotation,
                         uty.Vec4([0.0, 0.0, 0.0, 0.0]))
        self.assertEqual(self.tr4_no.scale,
                         uty.Vec4([1.0, 1.0, 1.0, 0.0]))
    
    def test_init_with_parameters(self):
        self.addTypeEqualityFunc(uty.Vec4, self.vec4_equal)
        self.assertEqual(self.tr4.translation,
                         uty.Vec4([1.0, 2.0, 3.0, 4.0]))
        self.assertEqual(self.tr4.rotation,
                         uty.Vec4([2.0, 3.0, 4.0, 1.0]))
        self.assertEqual(self.tr4.scale,
                         uty.Vec4([3.0, 4.0, 1.0, 2.0]))
    
    @staticmethod
    def vec4_equal(first, second, msg=None):
        return type(first) == uty.Vec4 and \
               type(first) == type(second) and \
               first.x == second.x and first.y == second.y and \
               first.z == second.z and first.w == second.w
    
    def test_init_wrong_length_parameters(self):
        with self.assertRaises(IndexError) as r:
            uty.Transformations4([1.0])
    
    def test_to_list(self):
        tr3_list = self.tr4.to_list()
        self.assertEqual(tr3_list[0], [1.0, 2.0, 3.0, 4.0])
        self.assertEqual(tr3_list[1], [2.0, 3.0, 4.0, 1.0])
        self.assertEqual(tr3_list[2], [3.0, 4.0, 1.0, 2.0])
