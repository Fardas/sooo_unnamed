from unittest import TestCase

import math
import pathlib

import source.utilities.unnamed_joystick as uj
import source.data_classes.main_data as md


class TestUnnamedJoystick(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
        self.joystick = uj.UnnamedJoystick()
        
    def test_check_move_x_west(self):
        west, east = self.joystick.check_move_x([-1, 0, 0, 0])
        self.assertEqual(-1, west)
        self.assertEqual(0, east)
        
    def test_check_move_x_east(self):
        west, east = self.joystick.check_move_x([1, 0, 0, 0])
        self.assertEqual(0, west)
        self.assertEqual(1, east)
        
    def test_check_move_y_north(self):
        north, south = self.joystick.check_move_y([0, -1, 0, 0])
        self.assertEqual(-1, north)
        self.assertEqual(0, south)
        
    def test_check_move_y_south(self):
        north, south = self.joystick.check_move_y([0, 1, 0, 0])
        self.assertEqual(0, north)
        self.assertEqual(1, south)
    
    def test_get_look_angle_45(self):
        angle = self.joystick.get_look_angle([0, 0, 1, 1])
        self.assertEqual(math.radians(45), angle)
        
    def test_get_look_angle_minus_45(self):
        angle = self.joystick.get_look_angle([0, 0, 1, -1])
        self.assertEqual(math.radians(-45), angle)
        
    def test_get_look_angle_no_angle(self):
        angle = self.joystick.get_look_angle(
            [0, 0, uj.UnnamedJoystick.L_DZONE*0.9,
             uj.UnnamedJoystick.L_DZONE*0.9])
        self.assertIsNone(angle)
    
    def test_load_from_file_wrong(self):
        load_config('wrong_joystick.config')
        wrong_js = uj.UnnamedJoystick.load_from_file()
        self.assertEqual(0, wrong_js.fire_button)

    def test_load_from_file_correct(self):
        load_config('correct_joystick.config')
        correct_js = uj.UnnamedJoystick.load_from_file()
        self.assertEqual(9, correct_js.fire_button)
    
    def test_check_if_correct_true(self):
        a_d = {'int': 32, 'float': 6.78, 'bool': False, 'str': 'str',
               'bool_list': [False, True], 'int_list': [7, 6, 100]}
        r_d = {'int': 1, 'float': 1.1, 'bool': True, 'str': 'something',
               'bool_list': [True, True], 'int_list': [1, 1, 1]}
        correct = uj.UnnamedJoystick.check_if_correct(a_d, r_d)
        
        self.assertTrue(correct)
    
    def test_check_if_correct_false(self):
        a_d = {'int': 32, 'float': 6.78, 'bool': False, 'str': 'str',
               'bool_list': [False, True], 'int_list': [7, 6, 100]}
        r_d = {'int': 1, 'float': 1, 'bool': 'str', 'str': False,
               'bool_list': [True, True], 'int_list': [1, 1.0, 1]}
        correct = uj.UnnamedJoystick.check_if_correct(a_d, r_d)
        
        self.assertFalse(correct)
        
    def test_save_to_file(self):
        uj.UnnamedJoystick.reset_config_file()
        old = uj.UnnamedJoystick.load_from_file().fire_button
        new_joystick = uj.UnnamedJoystick(fire_button=old + 1)
        new_joystick.save_to_file()
        new = uj.UnnamedJoystick.load_from_file().fire_button
        self.assertEqual(old + 1, new)
    
    def test_reset_config_file(self):
        uj.UnnamedJoystick.reset_config_file()
        old = uj.UnnamedJoystick.load_from_file().fire_button
        new = old + 20
        new_joystick = uj.UnnamedJoystick(fire_button=new)
        new_joystick.save_to_file()
        new = uj.UnnamedJoystick.load_from_file().fire_button
        self.assertEqual(old, new - 20)
    
    def test_button_event_press(self):
        self.joystick.button_state['fire_button'] = 0
        event = self.joystick.button_event('fire_button', 1)
        self.assertEqual(uj.JoystickEvent.PRESSED,
                         uj.JoystickEvent(event))
    
    def test_button_event_release(self):
        self.joystick.button_state['fire_button'] = 1
        event = self.joystick.button_event('fire_button', 0)
        self.assertEqual(uj.JoystickEvent.RELEASED,
                         uj.JoystickEvent(event))
    
    def test_button_event_no_interaction(self):
        self.joystick.button_state['fire_button'] = 1
        event = self.joystick.button_event('fire_button', 1)
        self.assertEqual(uj.JoystickEvent.NO_INTERACTION,
                         uj.JoystickEvent(event))


def load_config(filename):
    filepath = md.MAIN_DIR.joinpath('test', 'configs', filename)
    with filepath.open('r') as test_config:
        config_str = test_config.read()
    
    config_path = md.MAIN_DIR.joinpath('joystick.config')
    with config_path.open('w') as config:
        config.write(config_str)
