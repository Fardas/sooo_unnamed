from unittest import TestCase

import pathlib
import glfw
from OpenGL.GL import *

import source.data_classes.main_data as md
import source.utilities.opengl_utilities as ogl_uty


class TestCompileShader(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
        
        if glfw.init() is None:
            print('Error: Initialization of glfw failed!')
            return -1
        
        glfw.window_hint(glfw.RESIZABLE, False)
        md.OpenGLD.window = \
            glfw.create_window(md.OpenGLD.window_width,
                               md.OpenGLD.window_height,
                               'Unnamed Game', None, None)

        # create window
        if md.OpenGLD.window is None:
            print('Error: Window creation failed!')
            glfw.terminate()
            return -2

        # Window settings
        glfw.make_context_current(md.OpenGLD.window)
        
        self.shader_path = \
            md.MAIN_DIR.joinpath('test', 'source', 'shaders')
    
    def test_compile_shader_wrong_vs(self):
        wrong_vs_path = self.shader_path.joinpath('wrongVS.vert')
        wrong_fs_path = self.shader_path.joinpath('wrongFS.vert')
        result = ogl_uty.compile_shader(wrong_vs_path, wrong_fs_path)
        
        self.assertEqual(-1, result)
        
    def test_compile_shader_wrong_fs(self):
        good_vs_path = self.shader_path.joinpath('goodVS.vert')
        wrong_fs_path = self.shader_path.joinpath('wrongFS.vert')
        result = ogl_uty.compile_shader(good_vs_path, wrong_fs_path)
        
        self.assertEqual(-2, result)
        
    def test_compile_shader_good(self):
        good_vs_path = self.shader_path.joinpath('goodVS.vert')
        good_fs_path = self.shader_path.joinpath('goodFS.frag')
        result = ogl_uty.compile_shader(good_vs_path, good_fs_path)
        
        self.assertLessEqual(0, result)


class TestLoadShader(TestCase):
    def setUp(self):
        md.MAIN_DIR = pathlib.Path.cwd()
        md.MAIN_DIR = md.MAIN_DIR.parents[2]
        
        if glfw.init() is None:
            print('Error: Initialization of glfw failed!')
            return -1
        
        glfw.window_hint(glfw.RESIZABLE, False)
        md.OpenGLD.window = \
            glfw.create_window(md.OpenGLD.window_width,
                               md.OpenGLD.window_height,
                               'Unnamed Game', None, None)

        # create window
        if md.OpenGLD.window is None:
            print('Error: Window creation failed!')
            glfw.terminate()
            return -2

        # Window settings
        glfw.make_context_current(md.OpenGLD.window)
        
        self.shader_path = \
            md.MAIN_DIR.joinpath('test', 'source', 'shaders')
    
    def test_load_shader_good(self):
        good_vs_path = self.shader_path.joinpath('goodVS.vert')
        
        result = ogl_uty.load_shader(good_vs_path, GL_VERTEX_SHADER)

        self.assertIsNotNone(result)

    def test_load_shader_wrong(self):
        wrong_vs_path = self.shader_path.joinpath('wrongVS.vert')
    
        result = ogl_uty.load_shader(wrong_vs_path, GL_VERTEX_SHADER)
        
        self.assertIsNone(result)
